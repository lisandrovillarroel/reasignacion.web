﻿Public Class BANCHDB13

    Implements IDisposable

    Public _IDReasignacion As String
    Public _OrigenSeguro As String
    Public _NroSeguro As String
    Public _RutTitular As String
    Public _RutEjecutivo As String
    Public _MtoUF As String
    Public _TipoProducto As String
    Public _EstadoVenta As String
    Public _CanalVenta As String
    Public _MarcaReferido As String
    Public _CUI As String
    Public _MesContable As String
    Public _AnnoContable As String
    Public _IDMotivoReasignacion As String
    Public _MotivoReasignacion As String
    Public _RutEjecutivoReasignacion As String
    Public _DVEjecutivoReasignacion As String
    Public _NombreEjecutivoReasignacion As String
    Public _CanalVentaReasignacion As String
    Public _CUIReasignacion As String
    Public _Observacion As String
    Public IDUsuario As String
    Public usuario As String
    Public password As String
    Public ClaveAct As String
    Public grupoPermiso As String
    Public _NombreAdjunto As String
    Public _TipoAdjunto As String
    Public _Comentario As String
    Public _Adjunto_Doc As String
    Public _IDCoordinador As String
    Public _FechaDesde As String
    Public _FechaHasta As String
    Public _IDTipoSolicitud As String
    Public _NroFirma As String
    Public _IDUsuario As String
    Public NombrePermiso As String
    Public _NroSeguroOtro As String
    Public _IDEstadoAprobacion As String
    Public _IDEstadoGestion As String
    Public _IDSupervisor As String
    Public _IDSupervisorOriginal As String
    Public _IDGerente As String
    Public _IDGerenteOriginal As String
    Public _EstadoAprobacion As String
    Public _UsuarioCoor As String
    Public _UsuarioSuper As String
    Public _UsuarioSuperInter As String
    Public _UsuarioGerente As String
    Public _Usuario As String
    Public _Rut As String
    Public _Dv As String
    Public _Nombre As String
    Public _ApePaterno As String
    Public _ApeMaterno As String
    Public _Mail As String
    Public _Clave As String
    Public _IDGrupo As String
    Public _Vigente As String
    Public _Pagina As String
    Public _Gerente As String
    Public _Supervisor As String
    Public destinatario As String
    Public clave As String
    Public nombre As String
    Public _enProceso As String

    Public Sub Get_SeguroBVA(ByVal rp As DotNetResponse.SQLPersistence)

        Dim exec As New DotNetDal.SQLPersistence("DB3_DB_BCHILE_ODS")

        Dim szSQL As String
        szSQL = ""
        szSQL = szSQL + " Select convert(varchar,RUT_TITULAR) + '-' + DV_TITULAR  RutTitular "
        szSQL = szSQL + " ,	convert(varchar,RUT_VENDEDOR) + '-' + DV_VENDEDOR RutEjecutivo "
        szSQL = szSQL + " ,	PBRUTAUF "
        szSQL = szSQL + " ,	TIPO_PRODUCTO "
        szSQL = szSQL + " ,	ESTADO_VENTA "
        szSQL = szSQL + " ,	DescCanalDetalle "
        szSQL = szSQL + " ,	(case when MARCA_REFERIDOS = '' or MARCA_REFERIDOS is null then 'No Referido' else MARCA_REFERIDOS end)  MARCA_REFERIDOS                                               "
        szSQL = szSQL + " ,	CUIFUN  "
        szSQL = szSQL + " ,	IdVtaCui "
        szSQL = szSQL + " ,	MES_CONTABLE  "
        szSQL = szSQL + " ,	AÑO_CONTABLE	ANNO_CONTABLE "
        szSQL = szSQL + " , (case when Ramo like '%Desgravamen%' or Ramo like '%Cesant%' or Ramo like '%Inducido%' Then 'N' Else 'S' End) RamoAceptado "
        szSQL = szSQL + " From VW_BASE_VENTA_ACUMULADA "
        If _OrigenSeguro = "ISOL" Then
            szSQL = szSQL + " Where And NRO_SEGURO Like 'I%' And substring(NRO_SEGURO,9,7) = '" & _NroSeguro & "'"
        Else
            szSQL = szSQL + " Where	NRO_SEGURO = '" & _NroSeguro & "'"
        End If


        exec.ExecuteDataset(rp, szSQL)


        exec.Dispose()

    End Sub
    Public Sub Get_buscaReasignacion(ByVal rp As DotNetResponse.SQLPersistence)

        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        Dim szSQL As String
        szSQL = ""
        szSQL = szSQL + " Select convert(varchar,RUT_TITULAR) + '-' + DV_TITULAR  RutTitular "
        szSQL = szSQL + " From Reasignacion "
        szSQL = szSQL + " Where IDReasignacion = " & _IDReasignacion


        exec.ExecuteDataset(rp, szSQL)


        exec.Dispose()

    End Sub

    Public Sub Get_Funcionario(ByVal rp As DotNetResponse.SQLPersistence)

        Dim exec As New DotNetDal.SQLPersistence("DB3_DB2_SGT")

        Dim szSQL As String
        szSQL = ""
        szSQL = szSQL + " Select *,(case When convert(varchar(8),Fec_Egre,112) = '19000101' then 'S' else 'N' End) Activo from vw_funcionarios Where Rut = '" + _RutEjecutivo + "'"

        exec.ExecuteDataset(rp, szSQL)


        exec.Dispose()

    End Sub


    Public Sub Get_Ejecutivos(ByVal rp As DotNetResponse.SQLPersistence)

        Dim exec As New DotNetDal.SQLPersistence("DB3_FDI_CANALES")

        Dim szSQL As String
        szSQL = ""
        szSQL = szSQL + " Select * from VW_EJC_LISTADO_EJECUTIVOS Where RutEjecutivo  = '" + _RutEjecutivo + "'"

        exec.ExecuteDataset(rp, szSQL)


        exec.Dispose()

    End Sub

    Public Sub Get_DatosSeguroSCS(ByVal rp As DotNetResponse.SQLPersistence)

        Dim exec As New DotNetDal.SQLPersistence("DB3_DB2_SEGUROS")

        Dim szSQL As String
        szSQL = ""
        szSQL = szSQL + " Select *,(case When estado_seguro = 0 then 'S' else 'N' end ) SeguroVigente from d_mst_seguros_bco Where Nro_Seguro  = '" + _NroSeguro + "'"

        exec.ExecuteDataset(rp, szSQL)


        exec.Dispose()

    End Sub

    Public Sub Get_EstadoSeguroSCS(ByVal rp As DotNetResponse.SQLPersistence)

        Dim exec As New DotNetDal.SQLPersistence("DB3_DB2_SEGUROS")

        Dim szSQL As String
        szSQL = ""
        szSQL = szSQL + " Select *,(case When estado_seguro = 0 then 'S' else 'N' end ) SeguroVigente from d_mst_seguros_bco Where Nro_Seguro  = '" + _NroSeguro + "'"

        exec.ExecuteDataset(rp, szSQL)


        exec.Dispose()

    End Sub

    Public Sub Get_MotivoReasignacion(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.StoredProcedure = "sp_Get_MotivoReasignacion"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    'Public Sub Get_Reasignacion_x_Estado(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)
    '    Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
    '    rp.StoredProcedure = "sp_Get_Reasignacion_x_Estado"

    '    exec.ExecuteDataset(rp)

    '    exec.Dispose()

    'End Sub
    Public Sub Get_EstadoGestion(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.StoredProcedure = "sp_Get_EstadoGestion"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub Get_EstadoAprobacion(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.StoredProcedure = "sp_Get_EstadoAprobacion"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub Get_EstadoGestionTodos(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.StoredProcedure = "sp_Get_EstadoGestionTodos"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub Get_EstadoAprobacionTodos(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.StoredProcedure = "sp_Get_EstadoAprobacionTodos"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub Get_SupervidorTodos(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.StoredProcedure = "sp_Get_SupervisoresTodos"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub GetUsuario(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.StoredProcedure = "usp_GetUsuario"

        rp.AddParametro(usuario)
        rp.AddParametro(password)

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub


    Public Sub GetEnviaClave(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.StoredProcedure = "usp_GetEnviaClave"

        rp.AddParametro(usuario)

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub
    Public Sub GetEnviaCorreo(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.StoredProcedure = "SP_ENVIO_CLAVE"

        rp.AddParametro(destinatario)
        rp.AddParametro(Clave)
        rp.AddParametro(Nombre)

        rp.AddParametro(usuario)

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub
    Public Sub UpdatePass(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.StoredProcedure = "usp_UpdClave"

        rp.AddParametro(usuario)
        rp.AddParametro(password)
        rp.AddParametro(ClaveAct)

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub
    Public Sub GetPermiso(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.StoredProcedure = "usp_GetPermiso"

        rp.AddParametro(IDUsuario)
        rp.AddParametro(grupoPermiso)

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub GetSuperPermiso(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.StoredProcedure = "usp_GetSuperPermisos"

        rp.AddParametro(IDUsuario)
        rp.AddParametro(NombrePermiso)

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub


    Public Sub Put_Reasignacion(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)

        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.StoredProcedure = "sp_Put_Reasignacion"

        rp.AddParametro(_IDReasignacion)
        rp.AddParametro(_OrigenSeguro)
        rp.AddParametro(_NroSeguro)
        rp.AddParametro(_RutTitular)
        rp.AddParametro(_RutEjecutivo)
        rp.AddParametro(_MtoUF)
        rp.AddParametro(_TipoProducto)
        rp.AddParametro(_EstadoVenta)
        rp.AddParametro(_CanalVenta)
        rp.AddParametro(_MarcaReferido)
        rp.AddParametro(_CUI)
        rp.AddParametro(_MesContable)
        rp.AddParametro(_AnnoContable)
        rp.AddParametro(_IDMotivoReasignacion)
        rp.AddParametro(_MotivoReasignacion)
        rp.AddParametro(_RutEjecutivoReasignacion)
        rp.AddParametro(_DVEjecutivoReasignacion)
        rp.AddParametro(_NombreEjecutivoReasignacion)
        rp.AddParametro(_CanalVentaReasignacion)
        rp.AddParametro(_CUIReasignacion)
        rp.AddParametro(_Observacion)
        rp.AddParametro(IDUsuario)

        exec.ExecuteDataset(rp)
        exec.Commit()
        exec.Dispose()

    End Sub

    Public Sub Get_Reasignacion_x_Estado(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.StoredProcedure = "sp_Get_Reasignacion_x_Estado"

        rp.AddParametro(_IDReasignacion)
        rp.AddParametro(_IDEstadoAprobacion)
        rp.AddParametro(_IDTipoSolicitud)
        rp.AddParametro(_IDEstadoGestion)
        rp.AddParametro(_FechaDesde)
        rp.AddParametro(_FechaHasta)
        rp.AddParametro(_IDSupervisor)
        rp.AddParametro(_RutEjecutivo)
        rp.AddParametro(_NroSeguro)
        rp.AddParametro(_IDUsuario)
        rp.AddParametro(_IDCoordinador)
        rp.AddParametro(_IDGerente)
        rp.AddParametro(_enProceso)
        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub
    Public Sub Put_Adjunto(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.AddParametro(_IDReasignacion)
        rp.AddParametro(_NombreAdjunto)
        rp.AddParametro(_TipoAdjunto)
        rp.AddParametro(_Comentario)
        rp.AddParametro(_Adjunto_Doc)
        rp.AddParametro(IDUsuario)
        rp.AddParametro(_NroSeguro)

        rp.StoredProcedure = "sp_Put_DocumentoAdjunto"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub Get_ListadoAdjuntoDevolucion(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.AddParametro(_NroSeguro)

        rp.StoredProcedure = "sp_Get_DocumentoAdjunto"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub Get_TipoSolicitud(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.StoredProcedure = "sp_Get_TipoSolicitud"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub
    Public Sub Get_TipoSolicitudTodas(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.StoredProcedure = "sp_Get_TipoSolicitudTodas"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub Get_CoordinadoresTodos(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        ' rp.AddParametro(_IDReasignacion)

        rp.StoredProcedure = "sp_Get_CoordinadoresTodos"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub Get_SupervisorMasBlanco(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.StoredProcedure = "sp_Get_SupervisorMasBlanco"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub


    Public Sub Get_GerenteMasBlanco(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.StoredProcedure = "sp_Get_GerenteMasBlanco"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub


    'Public Sub GetUsuariosDerivacion(ByVal rp As DotNetResponse.SQLPersistence)
    '    Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

    '    rp.StoredProcedure = "usp_GetListaUsuario"

    '    exec.ExecuteDataset(rp)

    '    exec.Dispose()

    'End Sub

    Public Sub GetDerivacionFirma(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.StoredProcedure = "usp_GetDerivacionFirmas"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub InsertDerivacionFirma(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.StoredProcedure = "usp_InsertDerivacionFirmas"

        rp.AddParametro(_UsuarioCoor)
        rp.AddParametro(_UsuarioSuper)
        rp.AddParametro(_UsuarioSuperInter)
        rp.AddParametro(_UsuarioGerente)

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub
    Public Sub UpdateDerivacionFirma(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.StoredProcedure = "usp_UpdateDerivacionFirmas"

        rp.AddParametro(_UsuarioCoor)
        rp.AddParametro(_UsuarioSuper)
        rp.AddParametro(_UsuarioSuperInter)
        rp.AddParametro(_UsuarioGerente)

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub GetMotivoReasignacion(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.StoredProcedure = "usp_GetMotivo"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub
    Public Sub GetListaUsuario(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.StoredProcedure = "usp_GetListaUsuario"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub InsertUsuario(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.StoredProcedure = "usp_InsertUsuario"

        rp.AddParametro(_Usuario)
        '   rp.AddParametro(_Rut)
        '  rp.AddParametro(_Dv)
        rp.AddParametro(_Nombre)
        rp.AddParametro(_ApePaterno)
        rp.AddParametro(_ApeMaterno)
        rp.AddParametro(_Mail)
        rp.AddParametro(_Clave)
        rp.AddParametro(_IDGrupo)
        rp.AddParametro(_Vigente)

        exec.ExecuteDataset(rp)
        exec.Commit()
        exec.Dispose()

    End Sub
    Public Sub UpdateUsuario(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")


        rp.AddParametro(_IDUsuario)
        rp.AddParametro(_Usuario)
        'rp.AddParametro(_Rut)
        'rp.AddParametro(_Dv)
        rp.AddParametro(_Nombre)
        rp.AddParametro(_ApePaterno)
        rp.AddParametro(_ApeMaterno)
        rp.AddParametro(_Mail)
        rp.AddParametro(_Clave)
        rp.AddParametro(_IDGrupo)
        rp.AddParametro(_Vigente)

        rp.StoredProcedure = "usp_UpdateUsuario"

        exec.ExecuteDataset(rp)
        exec.Commit()
        exec.Dispose()

    End Sub



    Public Sub GetCoordinador(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.StoredProcedure = "usp_GetCoordinador"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub GetSupervisor(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.StoredProcedure = "usp_GetSupervisor"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub GetSupervisorInternet(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.StoredProcedure = "usp_GetSuperInternet"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub GetGerente(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.StoredProcedure = "usp_GetGerente"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub DelMotivo(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.StoredProcedure = "usp_DeleteMotivo"
        rp.AddParametro(_IDMotivoReasignacion)

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub InsertMotivosReasig(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.StoredProcedure = "usp_InsertMotivo"
        rp.AddParametro(_MotivoReasignacion)

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub UpdateMotivosReasig(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.StoredProcedure = "usp_UpdateMotivo"

        rp.AddParametro(_IDMotivoReasignacion)
        rp.AddParametro(_MotivoReasignacion)

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub




    'Public Sub Get_ReasignacionesGestionar(ByVal rp As DotNetResponse.SQLPersistence)
    '    Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

    '    rp.AddParametro(IDUsuario)
    '    rp.AddParametro(_IDReasignacion)
    '    rp.AddParametro(_IDCoordinador)
    '    rp.AddParametro(_FechaDesde)
    '    rp.AddParametro(_FechaHasta)
    '    rp.AddParametro(_IDTipoSolicitud)
    '    rp.AddParametro(_RutEjecutivoReasignacion)
    '    rp.AddParametro(_NroSeguro)

    '    rp.StoredProcedure = "sp_Get_ReasignacionesGestionar"

    '    exec.ExecuteDataset(rp)

    '    exec.Dispose()

    'End Sub

    'Public Sub Upd_AutorizacionRechazo(ByVal rp As DotNetResponse.SQLPersistence)
    '    Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

    '    rp.AddParametro(_IDReasignacion)
    '    rp.AddParametro(_NroFirma)
    '    rp.AddParametro(_Observacion)
    '    rp.AddParametro(_IDUsuario)

    '    rp.StoredProcedure = "sp_Upd_AutorizacionRechazo"

    '    exec.ExecuteDataset(rp)

    '    exec.Dispose()

    'End Sub
    Public Sub Get_ReasignacionesGestionar(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.AddParametro(IDUsuario)
        rp.AddParametro(_IDReasignacion)
        rp.AddParametro(_IDCoordinador)
        rp.AddParametro(_FechaDesde)
        rp.AddParametro(_FechaHasta)
        rp.AddParametro(_IDTipoSolicitud)
        rp.AddParametro(_RutEjecutivoReasignacion)
        rp.AddParametro(_NroSeguro)
        rp.AddParametro(_enProceso)

        rp.StoredProcedure = "sp_Get_ReasignacionesGestionar"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub
    Public Sub Get_ReasignacionesGestionarActualizar(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.AddParametro(IDUsuario)
        rp.AddParametro(_IDReasignacion)
        rp.AddParametro(_IDCoordinador)
        rp.AddParametro(_FechaDesde)
        rp.AddParametro(_FechaHasta)
        rp.AddParametro(_IDTipoSolicitud)
        rp.AddParametro(_RutEjecutivoReasignacion)
        rp.AddParametro(_NroSeguro)

        rp.StoredProcedure = "sp_Get_ReasignacionesGestionarAplicar"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub
    Public Sub Upd_AutorizacionRechazo(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.AddParametro(_IDReasignacion)
        rp.AddParametro(_NroFirma)
        rp.AddParametro(_Observacion)
        rp.AddParametro(_IDUsuario)

        rp.StoredProcedure = "sp_Upd_AutorizacionRechazo"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub
    Public Sub Upd_AutorizacionAprobar(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.AddParametro(_IDReasignacion)
        rp.AddParametro(_NroFirma)
        rp.AddParametro(_NroSeguroOtro)
        rp.AddParametro(_IDUsuario)

        rp.StoredProcedure = "sp_Upd_AutorizacionAprobar"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub Get_ReasignacionResumen(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.AddParametro(_IDReasignacion)
        rp.AddParametro(_IDUsuario)

        rp.StoredProcedure = "sp_Get_ReasignacionResumen"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub Get_ReasignacionResumenMisSolicitudes(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.AddParametro(_IDReasignacion)
        rp.AddParametro(_IDUsuario)

        rp.StoredProcedure = "sp_Get_ReasignacionResumenMisSolicitudes"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub Upd_ReasignacionEstadoTipo(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.AddParametro(_IDReasignacion)
        rp.AddParametro(_IDTipoSolicitud)
        rp.AddParametro(_EstadoAprobacion)
        rp.AddParametro(_IDSupervisor)
        rp.AddParametro(_IDSupervisorOriginal)
        rp.AddParametro(_IDGerente)
        rp.AddParametro(_IDGerenteOriginal)
        rp.AddParametro(_IDUsuario)

        rp.StoredProcedure = "sp_Upd_ReasignacionEstadoTipo"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub Get_Adjunto(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.AddParametro(_IDReasignacion)
        rp.AddParametro(_NroSeguro)

        rp.StoredProcedure = "sp_Get_Adjunto"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub Get_PermiteIngresarSeguro(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)

        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.StoredProcedure = "sp_PermiteIngresarFolio"

        rp.AddParametro(_OrigenSeguro)
        rp.AddParametro(_NroSeguro)

        exec.ExecuteDataset(rp)
        exec.Dispose()

    End Sub

    Public Sub GetGrupo(ByVal rp As DotNetResponse.SQLPersistence)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.StoredProcedure = "usp_GetGrupo"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub Get_ReasignacionHistorico(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.StoredProcedure = "sp_Get_ReasignacionHistorica"

        rp.AddParametro(_IDReasignacion)
        rp.AddParametro(_IDEstadoAprobacion)
        rp.AddParametro(_IDTipoSolicitud)
        rp.AddParametro(_IDEstadoGestion)
        rp.AddParametro(_FechaDesde)
        rp.AddParametro(_FechaHasta)
        rp.AddParametro(_IDSupervisor)
        rp.AddParametro(_RutEjecutivo)
        rp.AddParametro(_NroSeguro)
        rp.AddParametro(_IDUsuario)
        rp.AddParametro(_IDCoordinador)
        rp.AddParametro(_IDGerente)
        rp.AddParametro(_enProceso)
        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub Put_ProcesIntegracionHaciaBVA(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.StoredProcedure = "SP_RF049"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub Put_ProcesIntegracionDesdeBVA(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.StoredProcedure = "SP_RF050"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub Get_DashBoard(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.StoredProcedure = "sp_Get_DashBoard"
        rp.AddParametro(_IDUsuario)

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub Get_ValidarAccesoPagina(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.StoredProcedure = "sp_Get_AccesoPagina"
        rp.AddParametro(_IDGrupo)
        rp.AddParametro(_Pagina)

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub Get_MisSolicitudesXLS(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.StoredProcedure = "sp_Get_MisSolicitudesXLS"

        rp.AddParametro(_IDReasignacion)
        rp.AddParametro(_IDEstadoAprobacion)
        rp.AddParametro(_IDTipoSolicitud)
        rp.AddParametro(_IDEstadoGestion)
        rp.AddParametro(_FechaDesde)
        rp.AddParametro(_FechaHasta)
        rp.AddParametro(_IDSupervisor)
        rp.AddParametro(_RutEjecutivo)
        rp.AddParametro(_NroSeguro)
        rp.AddParametro(_IDUsuario)
        rp.AddParametro(_IDCoordinador)
        rp.AddParametro(_IDGerente)

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub Get_ReasignacionHistoricaXLS(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.StoredProcedure = "sp_Get_ReasignacionHistoricaXLS"

        rp.AddParametro(_IDReasignacion)
        rp.AddParametro(_IDEstadoAprobacion)
        rp.AddParametro(_IDTipoSolicitud)
        rp.AddParametro(_IDEstadoGestion)
        rp.AddParametro(_FechaDesde)
        rp.AddParametro(_FechaHasta)
        rp.AddParametro(_IDSupervisor)
        rp.AddParametro(_RutEjecutivo)
        rp.AddParametro(_NroSeguro)
        rp.AddParametro(_IDUsuario)
        rp.AddParametro(_IDCoordinador)
        rp.AddParametro(_IDGerente)


        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub


    Public Sub Get_SupervisorMasTodos_x_Gerente(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.StoredProcedure = "sp_Get_SupervisorMasTodos_x_Gerente"

        rp.AddParametro(_Gerente)

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub Get_CoordinadorMasTodos_x_Supervisor(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.StoredProcedure = "sp_Get_CoordinadorMasTodos_x_Supervisor"

        rp.AddParametro(_Supervisor)

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub Put_grabaEnProceso(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)

        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.StoredProcedure = "sp_Put_grabaEnProceso"

        rp.AddParametro(_IDReasignacion)
        rp.AddParametro(_IDUsuario)
        rp.AddParametro(_enProceso)

        exec.ExecuteDataset(rp)
        exec.Commit()
        exec.Dispose()

    End Sub
#Region "IDisposable Support"
    Private disposedValue As Boolean ' Para detectar llamadas redundantes

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not disposedValue Then
            If disposing Then
                ' TODO: elimine el estado administrado (objetos administrados).
            End If

            ' TODO: libere los recursos no administrados (objetos no administrados) y reemplace Finalize() a continuación.
            ' TODO: configure los campos grandes en nulos.
        End If
        disposedValue = True
    End Sub

    ' TODO: reemplace Finalize() solo si el anterior Dispose(disposing As Boolean) tiene código para liberar recursos no administrados.
    'Protected Overrides Sub Finalize()
    '    ' No cambie este código. Coloque el código de limpieza en el anterior Dispose(disposing As Boolean).
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' Visual Basic agrega este código para implementar correctamente el patrón descartable.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' No cambie este código. Coloque el código de limpieza en el anterior Dispose(disposing As Boolean).
        Dispose(True)
        ' TODO: quite la marca de comentario de la siguiente línea si Finalize() se ha reemplazado antes.
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
