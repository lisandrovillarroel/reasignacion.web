﻿Public Class BANCHDB3
    Implements IDisposable

    Public _IDReasignacion As String
    Public _OrigenSeguro As String
    Public _NroSeguro As String
    Public _RutTitular As String
    Public _RutEjecutivo As String
    Public _MtoUF As String
    Public _TipoProducto As String
    Public _EstadoVenta As String
    Public _CanalVenta As String
    Public _MarcaReferido As String
    Public _CUI As String
    Public _MesContable As String
    Public _AnnoContable As String
    Public _IDMotivoReasignacion As String
    Public _MotivoReasignacion As String
    Public _RutEjecutivoReasignacion As String
    Public _DVEjecutivoReasignacion As String
    Public _NombreEjecutivoReasignacion As String
    Public _CanalVentaReasignacion As String
    Public _CUIReasignacion As String
    Public _Observacion As String
    Public _IDUsuario As String
    Public _IDTipoSolicitud As String
    Public _FirmaSupervisor As String
    Public _FirmaGerente As String
    Public _EsInternet As String
    Public _Zona As String
    Public _Sucursal As String
    Public _ExisteEnBVA As String
    Public _ZonaEje As String
    Public _SucursalEje As String
    Public _AnoVenta As String
    Public _MesVenta As String
    Public _dia_cierre As String
    Public _txtEsSupervisorInternet As String
    Public _txtDiferenteFolioSN As String
    Public _txtRutTitular As String
    Public _txtNombreTitular As String



    Public Sub Get_SeguroBVA(ByVal rp As DotNetResponse.SQLPersistence)

        Dim exec As New DotNetDal.SQLPersistence("DB3_DB_BCHILE_ODS")

        Dim szSQL As String
        szSQL = ""
        szSQL = szSQL + " Select convert(varchar,RUT_TITULAR) + '-' + DV_TITULAR  RutTitular "
        szSQL = szSQL + " ,	RUT_TITULAR NroRutTitular "
        szSQL = szSQL + " ,	convert(varchar,RUT_VENDEDOR) + '-' + (case when DV_VENDEDOR is null then '' else DV_VENDEDOR end) RutEjecutivo "
        szSQL = szSQL + " ,	Nombre + ' ' + ApePat + ' ' + ApeMat NombreEjecutivo "
        szSQL = szSQL + " ,	convert(numeric(18,4),PBRUTAUF) PBRUTAUF "
        szSQL = szSQL + " ,	TIPO_PRODUCTO "
        szSQL = szSQL + " ,	ESTADO_VENTA "
        'szSQL = szSQL + " ,	DescCanalDetalle "
        szSQL = szSQL + " ,	(case when MARCA_REFERIDOS = '' or MARCA_REFERIDOS is null then '' else MARCA_REFERIDOS end)  MARCA_REFERIDOS                                               "
        szSQL = szSQL + " ,	CUIFUN  "
        szSQL = szSQL + " ,	IdVtaCui "
        szSQL = szSQL + " ,	MES_CONTABLE  "
        szSQL = szSQL + " ,	AÑO_CONTABLE	ANNO_CONTABLE "
        szSQL = szSQL + " ,	IdVtaSuc,DescVtaSuc,IdZonMod,DescVtaZonMod,DescVtaGerencia,IdCanalDetalle,DescCanalDetalle,DesSubCanal "
        szSQL = szSQL + " , case when DesSubCanal = 'Internet' or DesSubCanal = 'App' or DesSubCanal = 'Mobile' or IdSubCanal = 2014 then 'S' else 'N' End Internet "
        szSQL = szSQL + " , (case when Ramo like '%Desgravamen%' or Ramo like '%Cesant%' or Ramo like '%Inducido%' Then 'N' Else 'S' End) RamoAceptado "
        szSQL = szSQL + " ,	NRO_SEGURO "

        szSQL = szSQL + " From VW_BASE_VENTA_ACUMULADA "
        ' szSQL = " select * From VW_BASE_VENTA_ACUMULADA "
        If _OrigenSeguro = "ISOL" Then
            If _NroSeguro.Length = 7 Then
                szSQL = szSQL + " Where NRO_SEGURO Like 'I%' And substring(NRO_SEGURO,9,7) = '" & _NroSeguro & "'"
            Else
                szSQL = szSQL + " Where NRO_SEGURO Like 'I%' And substring(NRO_SEGURO,9,7) = substring('" & _NroSeguro & "',9,7)"
            End If
        Else
            szSQL = szSQL + " Where	NRO_SEGURO = '" & _NroSeguro & "'"
        End If


        exec.ExecuteDataset(rp, szSQL)


        exec.Dispose()

    End Sub


    Public Sub Get_Funcionario(ByVal rp As DotNetResponse.SQLPersistence)

        Dim exec As New DotNetDal.SQLPersistence("DB3_DB2_SGT")

        Dim szSQL As String
        szSQL = ""
        szSQL = szSQL + " Select *,(case When (convert(varchar(8),Fec_Egre,112) = '19000101' or Fec_Egre = '') then 'S' else 'N' End) Activo from vw_funcionarios Where Rut = '" + _RutEjecutivo + "'"

        exec.ExecuteDataset(rp, szSQL)


        exec.Dispose()

    End Sub


    Public Sub Get_Ejecutivos(ByVal rp As DotNetResponse.SQLPersistence)

        Dim exec As New DotNetDal.SQLPersistence("DB3_FDI_CANALES")

        Dim szSQL As String
        szSQL = ""
        szSQL = szSQL + " Select * from VW_EJC_LISTADO_EJECUTIVOS Where idesteje=1 and RutEjecutivo  = '" + _RutEjecutivo + "'"

        exec.ExecuteDataset(rp, szSQL)


        exec.Dispose()

    End Sub

    Public Sub Get_EjecutivoBancaPropia(ByVal rp As DotNetResponse.SQLPersistence)

        Dim exec As New DotNetDal.SQLPersistence("DB3_DB2_SGT")

        Dim szSQL As String
        szSQL = ""
        szSQL = szSQL + " Select *,(case When (convert(varchar(8),Fec_Egre,112) = '19000101' or Fec_Egre='') then 'S' else 'N' End) Activo "
        szSQL = szSQL + " From vw_funcionarios "
        szSQL = szSQL + " 	Inner Join FDI_CANALES.DBO.VW_EJC_LISTADO_EJECUTIVOS LE On LE.RutEjecutivo = convert(int,vw_funcionarios.Rut) "
        szSQL = szSQL + " Where LE.idesteje = 1 "
        '   szSQL = szSQL + " And	LE.dessubcanal = 'Banca Propia' "   'A solicitud de Tomas ya no se considera que el canal sea banca propia, sino que se cambia por los CUI 4488, 4785, 6763, 4409, 1034
        szSQL = szSQL + " And vw_funcionarios.CUI in (4488, 4785, 6763, 4409, 1034) "
        szSQL = szSQL + " And	Rut = '" + _RutEjecutivo + "'"

        exec.ExecuteDataset(rp, szSQL)
        exec.Dispose()

    End Sub

    Public Sub Get_buscaBvaBigsaMaestra(ByVal rp As DotNetResponse.SQLPersistence)

        Dim exec As New DotNetDal.SQLPersistence("DB3_DB_BCHILE_ODS")

        Dim szSQL As String
        szSQL = ""
        szSQL = szSQL + " Select base, nombreDelSeguro, oficina, cui, nombreEjecutivo, rutEjecutivo, folio, fechaOrden,rut_titular"
        szSQL = szSQL + " From"
        szSQL = szSQL + " ("
        szSQL = szSQL + " (Select '1' base, TIPO_PRODUCTO COLLATE Modern_Spanish_CI_AS As nombreDelSeguro "
        szSQL = szSQL + " , DescVtaSuc oficina"
        szSQL = szSQL + " , IdVtaCui cui"
        szSQL = szSQL + " , Nombre + ' ' + ApePat + ' ' + ApeMat nombreEjecutivo"
        szSQL = szSQL + " , convert(varchar,RUT_VENDEDOR) + '-' + (case when DV_VENDEDOR is null then '' else DV_VENDEDOR end) rutEjecutivo "
        szSQL = szSQL + " , cast(NRO_SEGURO As varchar(15)) folio"
        szSQL = szSQL + " ,FECHA_GRABACION fechaOrden"
        szSQL = szSQL + " ,rut_titular"
        szSQL = szSQL + " From VW_BASE_VENTA_ACUMULADA"
        szSQL = szSQL + " where Convert(varchar, FECHA_GRABACION, 112) > Convert(varchar, DateAdd(Month, -6, getdate()), 112)"
        szSQL = szSQL + "  And Estado_Venta<>'Cancelado'"
        szSQL = szSQL + " )"
        szSQL = szSQL + " union"
        szSQL = szSQL + " ("
        szSQL = szSQL + " Select '2' base,"
        szSQL = szSQL + " RAMO_DESCRIP As nombreDelSeguro "
        szSQL = szSQL + " , '' oficina"
        szSQL = szSQL + " , '' cui"
        szSQL = szSQL + " , '' nombreEjecutivo"
        szSQL = szSQL + " , '' rutEjecutivo"
        szSQL = szSQL + " , cast(DOCU_CODIGO As varchar(15)) folio"
        szSQL = szSQL + " , [docu.DOCU_FECHA_PROPUESTA] fechaOrden"
        szSQL = szSQL + " , PERS_RUT Rut_Titular"
        szSQL = szSQL + " from BIGSA12.dbo.VW_VENTAS_BIGSA"
        szSQL = szSQL + " where DOCU_ESTADO<>-1 and DOCU_TIPO_DOCUMENTO <> 'POLI'"
        szSQL = szSQL + " And Convert(varchar, [docu.DOCU_FECHA_PROPUESTA], 112) > Convert(varchar, DateAdd(Month, -6, getdate()), 112)"
        'szSQL = szSQL + " --Si esta en BVA no lo muestra"
        szSQL = szSQL + " And cast(DOCU_CODIGO As varchar(15)) not in "
        szSQL = szSQL + " ("
        szSQL = szSQL + " Select cast(NRO_SEGURO As varchar(15))"
        szSQL = szSQL + " From VW_BASE_VENTA_ACUMULADA"
        szSQL = szSQL + " where Convert(varchar, FECHA_GRABACION, 112) > Convert(varchar, DateAdd(Month, -6, getdate()), 112)"
        szSQL = szSQL + "  And Estado_Venta<>'Cancelado'"
        szSQL = szSQL + " )"
        'szSQL = szSQL + " -- Fin BVA"
        szSQL = szSQL + " )"
        szSQL = szSQL + " union"
        szSQL = szSQL + " ("
        szSQL = szSQL + " select "
        szSQL = szSQL + " '3' base"
        szSQL = szSQL + " ,VIEW_PRODUCTOS.TIPO_PRODUCTO nombreDelSeguro"
        szSQL = szSQL + " , '' oficina"
        szSQL = szSQL + " , '' cui"
        szSQL = szSQL + " , '' nombreEjecutivo"
        szSQL = szSQL + " , '' rutEjecutivo"
        szSQL = szSQL + " , cast(NRO_SEGURO As varchar(15)) folio"
        szSQL = szSQL + " ,FECHA_GRABACION fechaOrden"
        szSQL = szSQL + " ,RUT_TITULAR"
        szSQL = szSQL + " From DB2_SEGUROS.dbo.d_mst_seguros_bco"
        szSQL = szSQL + " Inner Join FDI_PRODUCTOS.[dbo].[VIEW_PRODUCTOS] VIEW_PRODUCTOS on VIEW_PRODUCTOS.poliza = DB2_SEGUROS.dbo.d_mst_seguros_bco.poliza COLLATE Modern_Spanish_CI_AS "
        szSQL = szSQL + " And VIEW_PRODUCTOS.ciaseg = DB2_SEGUROS.dbo.d_mst_seguros_bco.CODIGO_CIA  COLLATE Modern_Spanish_CI_AS"
        szSQL = szSQL + " Inner Join BD_PRODUCCION.dbo.VW_PRODUCTOS_REGLA_RECLAMO ReglaReclamo on DB2_SEGUROS.dbo.d_mst_seguros_bco.POLIZA = ReglaReclamo.POLIZA_MST_SEG  COLLATE Modern_Spanish_CI_AS "
        szSQL = szSQL + " Where Convert(varchar, FECHA_GRABACION, 112) > Convert(varchar, DateAdd(Month, -6, getdate()), 112)"
        szSQL = szSQL + " And ISNUMERIC(NRO_SEGURO)=1 And Estado_Seguro<>50"
        'szSQL = szSQL + " --Si esta en BVA no lo muestra"
        szSQL = szSQL + " And cast(NRO_SEGURO As varchar(15)) not in "
        szSQL = szSQL + " ("
        szSQL = szSQL + " Select cast(NRO_SEGURO As varchar(15))"
        szSQL = szSQL + " From VW_BASE_VENTA_ACUMULADA"
        szSQL = szSQL + " where Convert(varchar, FECHA_GRABACION, 112) > Convert(varchar, DateAdd(Month, -6, getdate()), 112)"
        szSQL = szSQL + " And Estado_Venta<>'Cancelado'"
        szSQL = szSQL + " )"
        'szSQL = szSQL + " -- Fin BVA"
        'szSQL = szSQL + " --Si esta en BIGSA no lo muestra"
        szSQL = szSQL + " And cast(NRO_SEGURO As varchar(15)) not in "
        szSQL = szSQL + " ("
        szSQL = szSQL + " Select cast(NRO_SEGURO As varchar(15))"
        szSQL = szSQL + " from BIGSA12.dbo.VW_VENTAS_BIGSA"
        szSQL = szSQL + " where DOCU_ESTADO<>-1 and DOCU_TIPO_DOCUMENTO <> 'POLI'"
        szSQL = szSQL + " And Convert(varchar, [docu.DOCU_FECHA_PROPUESTA], 112) > Convert(varchar, DateAdd(Month, -6, getdate()), 112)"
        szSQL = szSQL + " )"
        ' szSQL = szSQL + " -- Fin BIGSA"
        szSQL = szSQL + " )"
        szSQL = szSQL + " ) Final"
        szSQL = szSQL + " where rut_titular='" + _txtRutTitular + "'"
        szSQL = szSQL + " Order by fechaOrden"

        exec.ExecuteDataset(rp, szSQL)
        exec.Dispose()

    End Sub
    Public Sub Get_DatosSeguroSCS(ByVal rp As DotNetResponse.SQLPersistence)

        Dim exec As New DotNetDal.SQLPersistence("DB3_DB2_SEGUROS")

        Dim szSQL As String
        szSQL = ""
        szSQL = szSQL + " Select *,(case When estado_seguro = 0 then 'S' else 'N' end ) SeguroVigente from d_mst_seguros_bco Where Nro_Seguro  = '" + _NroSeguro + "'"

        exec.ExecuteDataset(rp, szSQL)


        exec.Dispose()

    End Sub
    Public Sub Get_diaHabil(ByVal rp As DotNetResponse.SQLPersistence)

        Dim exec As New DotNetDal.SQLPersistence("DB3_DB_BCHILE_ODS")

        Dim szSQL As String
        szSQL = ""
        szSQL = szSQL + " SELECT COD_DIA FROM [DB_BCHILE_ODS].[dbo].[TBL_DEF_CAMPAÑA_DIA] where año_ref='" + _AnoVenta + "' and nro_dia_habil_mes='" + _MesVenta + "' and mes_ref=" + _dia_cierre
        exec.ExecuteDataset(rp, szSQL)


        exec.Dispose()

    End Sub

    Public Sub Get_EstadoSeguroSCS(ByVal rp As DotNetResponse.SQLPersistence)

        Dim exec As New DotNetDal.SQLPersistence("DB3_DB2_SEGUROS")

        Dim szSQL As String
        szSQL = ""
        szSQL = szSQL + " Select *,(case When estado_seguro = 0 then 'S' else 'N' end ) SeguroVigente from d_mst_seguros_bco Where Nro_Seguro  = '" + _NroSeguro + "'"

        exec.ExecuteDataset(rp, szSQL)


        exec.Dispose()

    End Sub

    Public Sub Get_FechaCierreVenta(ByVal rp As DotNetResponse.SQLPersistence)

        Dim exec As New DotNetDal.SQLPersistence("DB3_GC_PROCESO_DIARIO")

        Dim szSQL As String
        szSQL = ""
        szSQL = szSQL + " Select ID,MES_VENTA,ANIO_VENTA,convert(varchar,FECHA_CIERRE,112) FECHA_CIERRE, convert(varchar,FECHA_INICIO_MES,112) FECHA_INICIO_MES,convert(varchar,FECHA_TERMINO_MES,112) FECHA_TERMINO_MES,STATUS_REG,DESC_MES,day(getdate()) DiaActual, substring(convert(varchar,dateadd(month,-1,FECHA_INICIO_MES),112),1,6) MesAnterior, substring(convert(varchar,getdate(),112),1,6) mesCalendario  From VW_FEC_CIERRE_VENTA Where status_reg =  1"
        'szSQL = szSQL + " Select ID,MES_VENTA,ANIO_VENTA,convert(varchar,FECHA_CIERRE,112) FECHA_CIERRE, convert(varchar,FECHA_INICIO_MES,112) FECHA_INICIO_MES,convert(varchar,FECHA_TERMINO_MES,112) FECHA_TERMINO_MES,STATUS_REG,DESC_MES,day(getdate()) DiaActual, substring(convert(varchar,dateadd(month,-1,FECHA_INICIO_MES),112),1,6) MesAnterior  From VW_FEC_CIERRE_VENTA Where status_reg =  1"
        'szSQL = szSQL + " Select ID,MES_VENTA,ANIO_VENTA,convert(varchar,FECHA_CIERRE,112) FECHA_CIERRE, convert(varchar,FECHA_INICIO_MES,112) FECHA_INICIO_MES,convert(varchar,FECHA_TERMINO_MES,112) FECHA_TERMINO_MES,STATUS_REG,DESC_MES,day(getdate()) DiaActual, substring(convert(varchar,dateadd(month,-1,FECHA_INICIO_MES),112),1,6) MesAnterior  From VW_FEC_CIERRE_VENTA Where status_reg =  1"

        exec.ExecuteDataset(rp, szSQL)


        exec.Dispose()

    End Sub

    Public Sub Get_MotivoReasignacion(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.StoredProcedure = "sp_Get_MotivoReasignacion"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub

    Public Sub Get_Reasignacion_x_Estado(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)
        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")
        rp.StoredProcedure = "sp_Get_Reasignacion_x_Estado"

        exec.ExecuteDataset(rp)

        exec.Dispose()

    End Sub



    Public Sub Put_Reasignacion(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)

        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.StoredProcedure = "sp_Put_Reasignacion"

        rp.AddParametro(_IDReasignacion)
        rp.AddParametro(_OrigenSeguro)
        rp.AddParametro(_NroSeguro)
        rp.AddParametro(_RutTitular)
        rp.AddParametro(_RutEjecutivo)
        rp.AddParametro(_MtoUF)
        rp.AddParametro(_TipoProducto)
        rp.AddParametro(_EstadoVenta)
        rp.AddParametro(_CanalVenta)
        rp.AddParametro(_MarcaReferido)
        rp.AddParametro(_CUI)
        rp.AddParametro(_MesContable)
        rp.AddParametro(_AnnoContable)
        rp.AddParametro(_IDMotivoReasignacion)
        rp.AddParametro(_MotivoReasignacion)
        rp.AddParametro(_RutEjecutivoReasignacion)
        rp.AddParametro(_DVEjecutivoReasignacion)
        rp.AddParametro(_NombreEjecutivoReasignacion)
        rp.AddParametro(_CanalVentaReasignacion)
        rp.AddParametro(_CUIReasignacion)
        rp.AddParametro(_Observacion)
        rp.AddParametro(_IDUsuario)
        rp.AddParametro(_IDTipoSolicitud)
        rp.AddParametro(_Sucursal)
        rp.AddParametro(_Zona)
        rp.AddParametro(_EsInternet)
        rp.AddParametro(_ExisteEnBVA)
        rp.AddParametro(_SucursalEje)
        rp.AddParametro(_ZonaEje)
        rp.AddParametro(_txtEsSupervisorInternet)
        rp.AddParametro(_txtDiferenteFolioSN)

        exec.ExecuteDataset(rp)
        exec.Commit()
        exec.Dispose()

    End Sub


    Public Sub Get_DerivacionFirma(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)

        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.StoredProcedure = "sp_Get_DerivacionFirma"

        rp.AddParametro(_IDUsuario)

        exec.ExecuteDataset(rp)
        exec.Dispose()

    End Sub

    Public Sub Get_EsDerivacionInternet(ByVal rp As DotNetResponse.SQLPersistence) 'ByVal rp As NetResponse.MsSqlServer)

        Dim exec As New DotNetDal.SQLPersistence("BD_REASIGNACION")

        rp.StoredProcedure = "sp_Get_DerivacionFirmaSInternet"

        rp.AddParametro(_IDUsuario)

        exec.ExecuteDataset(rp)
        exec.Dispose()

    End Sub


    Public Sub Get_SeguroBIGSA(ByVal rp As DotNetResponse.SQLPersistence)

        Dim exec As New DotNetDal.SQLPersistence("DB3_BIGSA12")

        Dim szSQL As String
        szSQL = ""

        szSQL = szSQL + " Select DOCU_CODIGO	NRO_SEGURO"
        szSQL = szSQL + " ,	(convert(varchar,PERS_RUT) + '-' + PERS_RUT_DIGITO)	RutTitular "
        szSQL = szSQL + " ,	PERS_RUT	NroRutTitular "
        szSQL = szSQL + " ,	IASE_CONTRATO	RutEjecutivo "
        szSQL = szSQL + " ,	convert(numeric(18,4),DOCU_PRIMA_BRUTA_TOTAL)	PBRUTAUF "
        szSQL = szSQL + " ,	RAMO_DESCRIP	TIPO_PRODUCTO "
        szSQL = szSQL + " ,	case when DOCU_ESTADO = -1 then 'ELIMINADO' "
        szSQL = szSQL + " 		when DOCU_ESTADO = 0 then 'VIGENTE' "
        szSQL = szSQL + " 		else '' "
        szSQL = szSQL + " 	END	ESTADO_VENTA "
        szSQL = szSQL + " ,	NEGO_DESCRIP	DescCanalDetalle "
        szSQL = szSQL + " ,	''	MARCA_REFERIDOS "
        szSQL = szSQL + " ,	'' CUIFUN "
        szSQL = szSQL + " ,	'' IdVtaCui "
        szSQL = szSQL + " ,	[docu.DOCU_FECHA_PROPUESTA] FechaInicioVigencia "
        szSQL = szSQL + " ,	right('00' + convert(varchar,MONTH([docu.DOCU_FECHA_PROPUESTA])),2) MES_CONTABLE "
        szSQL = szSQL + " ,	year([docu.DOCU_FECHA_PROPUESTA]) ANNO_CONTABLE "
        szSQL = szSQL + " ,	case when NEGO_DESCRIP like '%Internet%' or NEGO_DESCRIP like '%App%' or NEGO_DESCRIP like '%Mobile%' then 'S' else 'N' End Internet  "
        szSQL = szSQL + " ,	(case when RAMO_DESCRIP like '%Desgravamen%' or RAMO_DESCRIP like '%Cesant%' or RAMO_DESCRIP like '%Inducido%' Then 'N' Else 'S' End) RamoAceptado  "
        szSQL = szSQL + " from VW_VENTAS_BIGSA "
        szSQL = szSQL + " Where	DOCU_CODIGO =  " & _NroSeguro

        exec.ExecuteDataset(rp, szSQL)


        exec.Dispose()

    End Sub


    Public Sub Get_SeguroSCS(ByVal rp As DotNetResponse.SQLPersistence)

        Dim exec As New DotNetDal.SQLPersistence("DB3_DB2_SEGUROS")

        Dim szSQL As String
        szSQL = ""

        szSQL = szSQL + " Select "
        szSQL = szSQL + " 	NRO_SEGURO"
        szSQL = szSQL + " ,	(convert(varchar,RUT_TITULAR) + '-' + DV_TITULAR)	RutTitular"
        szSQL = szSQL + " ,	RUT_TITULAR	NroRutTitular"
        szSQL = szSQL + " ,	''	RutEjecutivo "
        szSQL = szSQL + " ,	convert(numeric(18,4),(MONTO_CUOTAMO*TOTAL_CUOTAS))	PBRUTAUF "
        szSQL = szSQL + " ,	VIEW_PRODUCTOS.TIPO_PRODUCTO	TIPO_PRODUCTO "
        szSQL = szSQL + " ,	case when ESTADO_SEGURO = 50 then 'ELIMINADO' "
        szSQL = szSQL + " 		when ESTADO_SEGURO = 0 or ESTADO_SEGURO = 1 then 'VIGENTE' "
        szSQL = szSQL + " 		else '' "
        szSQL = szSQL + " 	END	ESTADO_VENTA "
        szSQL = szSQL + " ,	CANAL_VENTA	DescCanalDetalle "
        szSQL = szSQL + " ,	''	MARCA_REFERIDOS "
        szSQL = szSQL + " ,	'' CUIFUN "
        szSQL = szSQL + " ,	'' IdVtaCui "
        szSQL = szSQL + " ,	[FECHA_INICIO_SEGURO] FechaInicioVigencia "
        szSQL = szSQL + " ,	right('00' + convert(varchar,MONTH(fecha_grabacion)),2) MES_CONTABLE "
        szSQL = szSQL + " ,	year(fecha_grabacion) ANNO_CONTABLE "
        szSQL = szSQL + " ,	case when (CANAL_VENTA like '%Internet%' or CANAL_VENTA like '%Mobile%' or CANAL_VENTA like '%App%') then 'S' else 'N' End Internet  "
        szSQL = szSQL + " ,	(case when RAMO_APP like '%Desgravamen%' or RAMO_APP like '%Cesant%' or RAMO_APP like '%Inducido%' Then 'N' Else 'S' End) RamoAceptado  "
        szSQL = szSQL + " From d_mst_seguros_bco"
        szSQL = szSQL + " 	Inner Join FDI_PRODUCTOS.[dbo].[VIEW_PRODUCTOS] VIEW_PRODUCTOS on VIEW_PRODUCTOS.poliza = d_mst_seguros_bco.poliza COLLATE Modern_Spanish_CI_AS "
        szSQL = szSQL + " And VIEW_PRODUCTOS.ciaseg = d_mst_seguros_bco.CODIGO_CIA  COLLATE Modern_Spanish_CI_AS"
        szSQL = szSQL + " 	Inner Join BD_PRODUCCION.dbo.VW_PRODUCTOS_REGLA_RECLAMO ReglaReclamo on d_mst_seguros_bco.POLIZA = ReglaReclamo.POLIZA_MST_SEG  COLLATE Modern_Spanish_CI_AS "
        If _OrigenSeguro = "ISOL" Then
            If _NroSeguro.Length = 7 Then
                szSQL = szSQL + " Where NRO_SEGURO Like 'I%' And substring(NRO_SEGURO,9,7) = '" & _NroSeguro & "'"
            Else
                szSQL = szSQL + " Where NRO_SEGURO Like 'I%' And substring(NRO_SEGURO,8,8) = substring('" & _NroSeguro & "',8,8)"
                ' szSQL = szSQL + " Where	NRO_SEGURO = '" & _NroSeguro & "'"
            End If
        Else
            szSQL = szSQL + " Where	NRO_SEGURO = '" & _NroSeguro & "'"
        End If

        exec.ExecuteDataset(rp, szSQL)


        exec.Dispose()

    End Sub



    Public Sub Get_ValidarCUI(ByVal rp As DotNetResponse.SQLPersistence)

        Dim exec As New DotNetDal.SQLPersistence("DB3_DB2_SGT")

        Dim szSQL As String
        szSQL = ""
        szSQL = szSQL + " Select Case When count(1) = 0 then 'N' else 'S' End ExisteCUI "
        szSQL = szSQL + " From SGT_CUI Where Cuicod = " & _CUI

        exec.ExecuteDataset(rp, szSQL)
        exec.Dispose()

    End Sub


#Region "IDisposable Support"
    Private disposedValue As Boolean ' Para detectar llamadas redundantes

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not disposedValue Then
            If disposing Then
                ' TODO: elimine el estado administrado (objetos administrados).
            End If

            ' TODO: libere los recursos no administrados (objetos no administrados) y reemplace Finalize() a continuación.
            ' TODO: configure los campos grandes en nulos.
        End If
        disposedValue = True
    End Sub

    ' TODO: reemplace Finalize() solo si el anterior Dispose(disposing As Boolean) tiene código para liberar recursos no administrados.
    'Protected Overrides Sub Finalize()
    '    ' No cambie este código. Coloque el código de limpieza en el anterior Dispose(disposing As Boolean).
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' Visual Basic agrega este código para implementar correctamente el patrón descartable.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' No cambie este código. Coloque el código de limpieza en el anterior Dispose(disposing As Boolean).
        Dispose(True)
        ' TODO: quite la marca de comentario de la siguiente línea si Finalize() se ha reemplazado antes.
        GC.SuppressFinalize(Me)
    End Sub
#End Region


End Class
