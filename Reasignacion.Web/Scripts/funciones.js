﻿(function ($) {
    $.webMethod = function (options) {
        var settings = $.extend({
            'methodName': '',
            'async': false,
            'cache': false,
            timeout: 30000,
            debug: false,
            'parameters': {},
            success: function (response) { },
            error: function (response) { }
        }, options); var parameterjson = "{}";
        var result = null;
        //if (settings.parameters != null) { parameterjson = $.toJSON(settings.parameters); }
        if (settings.parameters != null) { parameterjson = JSON.stringify(settings.parameters); }
        $.ajax({
            type: "POST",
            url: location.pathname + "/" + settings.methodName,
            data: parameterjson,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: settings.async,
            cache: settings.cache,
            timeout: settings.timeout,
            success: function (value) {
                result = value.d;
                settings.success(result);
            },
            error: function (response) {
                settings.error(response);
                if (settings.debug) { alert("Error Calling Method \"" + settings.methodName + "\"\n\n+" + response.responseText); }
            }
        });
        return result;
    };
})(jQuery);

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function showSuccess(mensaje) {
    $('#showSuccessTitle').text('[Reasignación] - Operación exitosa');
    $('#showSuccessBody').empty();
    $('#showSuccessBody').html(mensaje);

    $('#showSuccess').modal('show');
}

function showNewData(mensaje) {
    alert('showNewData');
    $('#showNewDataTitle').text('[GRT]');
    $('#showNewDataBody').empty();
    //$('#showSuccessBody').html(mensaje);

    $('#showNewData').modal('show');
}

function showError(mensaje) {
    $('#showErrorTitle').text('[Reasignación] -Operación con error');
    $('#showErrorBody').empty();
    $('#showErrorBody').html(mensaje);

    $('#showError').modal('show');
}

function showInfo(mensaje) {
    $('#showInfoTitle').text('[Reasignación] -Operación Informativa');
    $('#showInfoBody').empty();
    $('#showInfoBody').html(mensaje);

    $('#showInfo').modal('show');
}

function showLoader(mensaje) {
    $.blockUI({ message: "<h4>" + mensaje + "</h4>" });
    //$.blockUI({ message: "<img src='img/loader.gif' /><h4>" + mensaje + "</h4>" });
}

function hide() {
    $.unblockUI();
}

function hideModal() {
    $('#managerModal').modal('hide');
}

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }

    return vars;
}

function getParam() {
    return getUrlVars()["param"];
}

//function getParameterByName(name) {
//    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
//    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
//                results = regex.exec(location.search);
//    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
//}

function getHeight() {
    return $(window).height() - $('h1').outerHeight(true);
}


$.date = function (dateObject) {
    var d = new Date(dateObject);

    var date = d.toLocaleString().split(' ')[0];
    return date;
};

function parseBool(val) {
    if ((typeof val === 'string' && (val.toLowerCase() === 'true' || val.toLowerCase() === 'yes')) || val === 1)
        return true;
    else if ((typeof val === 'string' && (val.toLowerCase() === 'false' || val.toLowerCase() === 'no')) || val === 0)
        return false;

    return null;
}

function StringIsEmpty(str) {
    return (str == null || str === "");
}

// ******************************************************************
// This function accepts a string variable and verifies if it is a
// proper date or not. It validates format matching either
// mm-dd-yyyy or mm/dd/yyyy. Then it checks to make sure the month
// has the proper number of days, based on which month it is.

// The function returns true if a valid date, false if not.
// ******************************************************************



function BlockUI() {
    var ajaxBlock = function () {
        $.blockUI({
            message: "<img src='Img/Spin-1s-124px.gif' /><h4>" + "Cargando..." + "</h4>",
            css: { width: '30%', border: '0px solid #FFFFFF', cursor: 'wait', backgroundColor: '#FFFFFF' },
            overlayCSS: { backgroundColor: '#FFFFFF', opacity: .4, cursor: 'wait' }
        })
    }
    $(document).ajaxStart(ajaxBlock).ajaxStop($.unblockUI);
}

function LockUI() {
        $.blockUI({
            message: "<img src='Img/Spin-1s-124px.gif' /><h4>" + "Cargando..." + "</h4>",
            css: { width: '30%', border: '0px solid #FFFFFF', cursor: 'wait', backgroundColor: '#FFFFFF' },
            overlayCSS: { backgroundColor: '#FFFFFF', opacity: .4, cursor: 'wait' }
        });
}

function UnLockUI() {
    $.unblockUI();
}



function isDate(dateStr) {

    //var datePat = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
    var datePat = /^(\d{1,4})(\/|-)(\d{1,2})(\/|-)(\d{2})$/;
    var matchArray = dateStr.match(datePat); // is the format ok?

    if (matchArray == null) {
        alert("Please enter date as either mm/dd/yyyy or mm-dd-yyyy.");
        return false;
    }

    year = matchArray[1]; // p@rse date into variables
    month = matchArray[3];
    day = matchArray[5];
 

    if (month < 1 || month > 12) { // check month range
        //alert("Month must be between 1 and 12.");
        return false;
    }

    if (day < 1 || day > 31) {
        //alert("Day must be between 1 and 31.");
        return false;
    }

    if ((month == 4 || month == 6 || month == 9 || month == 11) && day == 31) {
        //alert("Month " + month + " doesn`t have 31 days!")
        return false;
    }

    if (month == 2) { // check for february 29th
        var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
        if (day > 29 || (day == 29 && !isleap)) {
            //alert("February " + year + " doesn`t have " + day + " days!");
            return false;
        }
    }
    return true; // date is valid
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};


function DigitoRut(rut) {
    
        var caracteres = new Array();
        var serie = new Array(2, 3, 4, 5, 6, 7);

        for (var i = 0; i < rut.length; i++) {
            caracteres[i] = parseInt(rut.charAt((rut.length - (i + 1))));
        }

        var sumatoria = 0;
        var k = 0;
        var resto = 0;

        for (var j = 0; j < caracteres.length; j++) {
            if (k == 6) {k = 0;}
            sumatoria += parseInt(caracteres[j]) * parseInt(serie[k]);
            k++;
        }

        resto = sumatoria % 11;
        dv = 11 - resto;

        if (dv == 10) {dv = "K";}
        else if (dv == 11) {dv = 0;}

        return dv;
}

function soloNumeros(e) {
    var key = window.Event ? e.which : e.keyCode
    return ((key >= 48 && key <= 57) || (key == 8))
}


function RestrictInvalidChar(_Campo) {
    //alert('aaaaaa');

    var _Campo = $('#_Campo');

    _Campo.val(_Campo.val().replace(/['"°|;]/g, ''));

    _Campo.keypress(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
        }
    });

}