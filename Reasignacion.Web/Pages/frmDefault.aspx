﻿<%@ Page Language="vb" AutoEventWireup="false"  MasterPageFile="~/Pages/Sitio.Master"  CodeBehind="frmDefault.aspx.vb" Inherits="Reasignacion.Web.frmDefault" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

   <script type="text/javascript">

       $(document).ready(function () {

          //$(document).ajaxStart($.blockUI({ message: '<h1><img src="../Img/loading.gif" /> Procesando...</h1>' })).ajaxStop($.unblockUI);

          //Get_Coordinadores();


           //$.unblockUI();
          

      });


    </script>


</asp:Content>

 
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div class="content content--full" id="DashBoard">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">DashBoard con Total Historico de Reasignaciones</h3>
        </div>
        <div class="panel-body">
             <div class="row">
                 <div class="col-md-12">
                     <div class="panel panel-primary">
                         <div class="panel-body">
                             <asp:Literal ID="l_Data" runat="server" Mode="PassThrough"></asp:Literal>

                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>
        

<div class="modal modal-header-success" role="dialog" tabindex="-1" id="CamposOcualtos">
    <asp:TextBox ID="txtUsuario_Session" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtIDPerfil" runat="server"></asp:TextBox>

     <asp:TextBox ID="txtIDUsuario" runat="server" ></asp:TextBox>
</div>
<script>
    if (document.getElementById("<%=txtIDPerfil.ClientID%>").value !=1) {
        document.getElementById('DashBoard').style.display = "none";
    }
</script>
</asp:Content>

