﻿Imports System.Web.Services
Imports Newtonsoft.Json
Imports System.IO
Imports System.Security.Cryptography

Public Class frmMantUsuario
    Inherits Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            txtIDUsuario.Text = Session("IDUsuario")
            txtIDPerfil.Text = Session("IDGrupo")
            If txtIDUsuario.Text = "" Then
                Response.Redirect("frmLogin.aspx")
            End If
        End If

        Dim _Pagina As String

        _Pagina = (New System.IO.FileInfo(Page.Request.Url.AbsolutePath).Name)

    End Sub

    Public Shared Function ToJSON(pTable As DataTable) As String
        Dim vCadenaJSON As String = ""
        vCadenaJSON = JsonConvert.SerializeObject(pTable)
        If vCadenaJSON.Length <= 5 Then
            vCadenaJSON = "[{""Nro_Seguro"":""0""}]"
        End If
        Return vCadenaJSON
    End Function

    <WebMethod()>
    Public Shared Function GetListUsers() As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable
            Dim claveDesc As String

            Exec.GetListaUsuario(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then



                dtJSON = rp.Ds.Tables(0)

                For Each Fila As DataRow In dtJSON.Rows
                    claveDesc = Decrypt(Fila.Field(Of String)("Clave"))
                    Fila.SetField("Clave", claveDesc) 'Desencripta clave
                Next
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <WebMethod()>
    Public Shared Function InsertUsuario(Usuario As String, Nombre As String, ApePaterno As String, ApeMaterno As String, Mail As String, Clave As String, IDGrupo As String, Vigente As String) As String
        'Se saco el rut
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._Usuario = Usuario
            ' Exec._Rut = Rut
            ' Exec._Dv = Dv
            Exec._Nombre = Nombre
            Exec._ApePaterno = ApePaterno
            Exec._ApeMaterno = ApeMaterno
            Exec._Mail = Mail
            Exec._Clave = Encrypt(Clave.Trim())
            Exec._IDGrupo = IDGrupo
            Exec._Vigente = Vigente

            Exec.InsertUsuario(rp)

            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <WebMethod()>
    Public Shared Function UpdateUsuario(IDUsuario As String, Usuario As String, Nombre As String, ApePaterno As String, ApeMaterno As String, Mail As String, Clave As String, IDGrupo As String, Vigente As String) As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._IDUsuario = IDUsuario
            Exec._Usuario = Usuario
            ' Exec._Rut = Rut
            'Exec._Dv = Dv
            Exec._Nombre = Nombre
            Exec._ApePaterno = ApePaterno
            Exec._ApeMaterno = ApeMaterno
            Exec._Mail = Mail
            Exec._Clave = Encrypt(Clave.Trim())
            Exec._IDGrupo = IDGrupo
            Exec._Vigente = Vigente

            Exec.UpdateUsuario(rp)


            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <WebMethod()>
    Public Shared Function GetGrupo() As String
        Try
            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec.GetGrupo(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Private Shared Function Encrypt(clearText As String) As String
        Dim EncryptionKey As String = "MAKV2SPBNI99212"
        Dim clearBytes As Byte() = Encoding.Unicode.GetBytes(clearText)
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, _
             &H65, &H64, &H76, &H65, &H64, &H65, _
             &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using ms As New MemoryStream()
                Using cs As New CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write)
                    cs.Write(clearBytes, 0, clearBytes.Length)
                    cs.Close()
                End Using
                clearText = Convert.ToBase64String(ms.ToArray())
            End Using
        End Using
        Return clearText
    End Function

    Private Shared Function Decrypt(cipherText As String) As String
        Dim EncryptionKey As String = "MAKV2SPBNI99212"
        Dim cipherBytes As Byte() = Convert.FromBase64String(cipherText)
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, _
             &H65, &H64, &H76, &H65, &H64, &H65, _
             &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using ms As New MemoryStream()
                Using cs As New CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write)
                    cs.Write(cipherBytes, 0, cipherBytes.Length)
                    cs.Close()
                End Using
                cipherText = Encoding.Unicode.GetString(ms.ToArray())
            End Using
        End Using
        Return cipherText
    End Function

End Class