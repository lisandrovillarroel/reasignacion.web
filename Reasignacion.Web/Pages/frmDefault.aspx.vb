﻿Public Class frmDefault
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            txtIDUsuario.Text = Session("IDUsuario")
            txtIDPerfil.Text = Session("IDGrupo")
            If txtIDUsuario.Text = "" Then
                Response.Redirect("frmLogin.aspx")
            End If
        End If

        If txtIDPerfil.Text = 2 Then  'Coordinador
            Response.Redirect("frmIngresoSolicitud.aspx")
        Else
            If txtIDPerfil.Text = 3 Or txtIDPerfil.Text = 4 Or txtIDPerfil.Text = 5 Then
                Response.Redirect("frmAprobacionSolicitud.aspx")
            Else
                Get_DashBoard(txtIDUsuario.Text)
            End If
        End If
    End Sub




    Private Sub Get_DashBoard(ByVal _IDUsuario As Integer)
        Try
            Dim exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence
            Dim _Data As String
            _Data = ""

            exec._IDUsuario = _IDUsuario
            exec.Get_DashBoard(rp)


            If Not rp Is Nothing Then
                If Not rp.Errores Then
                    If rp.TieneDatos Then
                        'f_Data(rp.Ds.Tables(0))
                        _Data = _Data + " <div class='jumbotron'>"
                        _Data = _Data + " <div class='row w-100'>"
                        _Data = _Data + "         <div class='col-md-4'>"
                        _Data = _Data + "             <div class='card border-primary mx-sm-1 p-3'>"
                        _Data = _Data + "                 <div class='card border-primary shadow text-primary p-3 my-card' ><span class='fa fa-calendar-alt fa-lg' aria-hidden='true'> Reasignaciones</span></div>"
                        _Data = _Data + "                 <div class='text-primary text-center mt-3'><h4>Total Reasignaciones</h4></div>"
                        _Data = _Data + "                 <div class='text-primary text-center mt-2'><h1>" & rp.Ds.Tables(0).Rows(0).Item("Total") & "</h1></div>"
                        _Data = _Data + "             </div>"
                        _Data = _Data + "         </div>"
                        _Data = _Data + "         <div class='col-md-4'>"
                        _Data = _Data + "             <div class='card border-success mx-sm-1 p-3'>"
                        _Data = _Data + "                 <div class='card border-success shadow text-success p-3 my-card'><span class='fa fa-check-square fa-lg' aria-hidden='true'> Reasignaciones Aprobadas</span></div>"
                        _Data = _Data + "                 <div class='text-success text-center mt-3'><h4>Total Aprobadas</h4></div>"
                        _Data = _Data + "                 <div class='text-success text-center mt-2'><h1>" & rp.Ds.Tables(0).Rows(0).Item("Aprobadas") & "</h1></div>"
                        _Data = _Data + "             </div>"
                        _Data = _Data + "         </div>"
                        _Data = _Data + "         <div class='col-md-4'>"
                        _Data = _Data + "             <div class='card border-danger mx-sm-1 p-3'>"
                        _Data = _Data + "                 <div class='card border-danger shadow text-danger p-3 my-card' ><span class='fa fa-times-square fa-lg' aria-hidden='true'> Reasignaciones Rechazadas</span></div>"
                        _Data = _Data + "                 <div class='text-danger text-center mt-3'><h4>Total Rechazadas</h4></div>"
                        _Data = _Data + "                 <div class='text-danger text-center mt-2'><h1>" & rp.Ds.Tables(0).Rows(0).Item("Rechazada") & "</h1></div>"
                        _Data = _Data + "             </div>"
                        _Data = _Data + "         </div>"
                        _Data = _Data + "      </div>"
                        _Data = _Data + " <div class='row w-100'>"
                        _Data = _Data + "     <div class='col-md-4'>"
                        _Data = _Data + "             <div class='card border-secondary mx-sm-1 p-3 '>"
                        _Data = _Data + "                 <div class='card border-secondary shadow text-secondary p-3 my-card'><span class='fa fa-pause-circle fa-lg' aria-hidden='true'> Reasignaciones Pendiente</span></div>"
                        _Data = _Data + "                 <div class='text-secondary text-center mt-3'><h4>Total Pendiente</h4></div>"
                        _Data = _Data + "                 <div class='text-secondary text-center mt-2'><h1>" & rp.Ds.Tables(0).Rows(0).Item("Pendiente") & "</h1></div>"
                        _Data = _Data + "             </div>"
                        _Data = _Data + "         </div>"
                        _Data = _Data + "             "
                        _Data = _Data + "     <div class='col-md-4'>"
                        _Data = _Data + "             <div class='card border-info mx-sm-1 p-3'>"
                        _Data = _Data + "                 <div class='card border-info shadow text-info p-3 my-card' ><span class='fa fa-not-equal fa-lg' aria-hidden='true'> Reasignaciones Diferente Folio</span></div>"
                        _Data = _Data + "                 <div class='text-info text-center mt-3'><h4>Total Diferente Folio</h4></div>"
                        _Data = _Data + "                 <div class='text-info text-center mt-2'><h1>" & rp.Ds.Tables(0).Rows(0).Item("AprobadaDifFolio") & "</h1></div>"
                        _Data = _Data + "             </div>"
                        _Data = _Data + "         </div>"
                        _Data = _Data + "         <div class='col-md-4'>"
                        _Data = _Data + "             <div class='card border-warning mx-sm-1 p-3'>"
                        _Data = _Data + "                 <div class='card border-warning shadow text-warning p-3 my-card' ><span class='fa fa-times-circle fa-lg' aria-hidden='true'> Reasignaciones Caducadas</span></div>"
                        _Data = _Data + "                 <div class='text-warning text-center mt-3'><h4>Total Caducadas</h4></div>"
                        _Data = _Data + "                 <div class='text-warning text-center mt-2'><h1>" & rp.Ds.Tables(0).Rows(0).Item("Caducada") & "</h1></div>"
                        _Data = _Data + "             </div>"
                        _Data = _Data + "         </div>"
                        _Data = _Data + "      </div>"
                        _Data = _Data + " </div>"


                    End If
                Else
                    _Data = ""
                    'MsgBox1.ShowMessage(rp.MensajeError)
                End If
            End If
            l_Data.Text = _Data
            rp.Dispose()
            exec.Dispose()
        Catch ex As Exception
            'MsgBox1.ShowMessage(ex.Message)
        End Try
    End Sub


    'Private Sub CrearMenu(ByRef Tabla As System.Data.DataTable)
    '    Dim szMenu As String
    '    szMenu = "<ul class='nav navbar-nav'> "

    '    CrearMenu(Tabla, 0, Nothing, 0, 1)

    '    szMenu = szMenu + _Menu + " </ul>"

    '    l_Menu.Text = szMenu

    'End Sub

    'Private Sub CrearMenu(ByRef Tabla As System.Data.DataTable, ByVal indicePadre As Integer, ByVal nodePadre As TreeNode, ByVal CantHijos As Integer, ByVal HijosCargados As Integer)
    '    Try
    '        Dim dataViewHijos As New System.Data.DataView(Tabla)
    '        dataViewHijos.RowFilter = Tabla.Columns("IDMenuPadre").ColumnName + " = " + indicePadre.ToString

    '        Dim dataRowCurrent As System.Data.DataRowView

    '        For Each dataRowCurrent In dataViewHijos
    '            If dataRowCurrent("Pagina").ToString().Trim() = "#" Then
    '                CantHijos = dataRowCurrent("CantidadHijos").ToString().Trim()
    '                HijosCargados = 0
    '                Dim _Class = dataRowCurrent("Nombre").ToString().Trim().Replace(" ", "")
    '                _Menu = _Menu + " <li class='dropdown " & _Class & "'><a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='True' aria-expanded='False'>" & dataRowCurrent("Nombre").ToString().Trim() & "<span class='caret'></span></a>"
    '                _Menu = _Menu + " <ul class='dropdown-menu'>"
    '            Else
    '                Dim _Class = Mid(dataRowCurrent("Pagina").ToString().Trim(), 1, InStr(dataRowCurrent("Pagina").ToString().Trim(), ".") - 1)
    '                _Menu = _Menu + " <li class='" & _Class & "'><a href='" & dataRowCurrent("Pagina").ToString().Trim() & "?ID=" & _ID & "'>" & dataRowCurrent("Nombre").ToString().Trim() & "</a></li> "
    '                HijosCargados = HijosCargados + 1
    '                If CantHijos = HijosCargados Then
    '                    _Menu = _Menu + " </ul> </li>"
    '                End If
    '            End If
    '            CrearMenu(Tabla, CInt(dataRowCurrent("IdMenu")), Nothing, CantHijos, HijosCargados)
    '        Next
    '    Catch ex As Exception

    '    End Try

    'End Sub

End Class