﻿Public Class Sitio
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        Dim _Pagina As String

        _Pagina = (New System.IO.FileInfo(Page.Request.Url.AbsolutePath).Name)

        Dim IDUsuario As String = Session("IDUsuario")
        Dim _IDGrupo As String = Session("IDGrupo")
        'If _Pagina <> "frmDefault.aspx" Then
        '    Get_ValidarAcceso(_IDGrupo, _Pagina)
        'End If

        If IDUsuario <> "" Then
            If _Pagina <> "frmDefault.aspx" Then
                If Not Get_ValidarAcceso(_IDGrupo, _Pagina) Then
                    Response.Redirect("frmLogin.aspx")
                    Exit Sub
                End If
            End If
            navbarDropdown.Visible = False
            GetLinks()
        Else
            Response.Redirect("frmLogin.aspx")
        End If

        'Dim a As String
        'a = Request.ServerVariables("AUTH_USER").ToString()


    End Sub

    Protected Function Get_ValidarAcceso(_IDGrupo As String, _Pagina As String) As Boolean

        Try
            Dim _Retorno As Boolean
            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Exec._IDGrupo = _IDGrupo
            Exec._Pagina = _Pagina

            Exec.Get_ValidarAccesoPagina(rp)

            If rp.TieneDatos = True Then
                If rp.Ds.Tables(0).Rows(0).Item("TieneAcceso") = "S" Then
                    _Retorno = True
                Else
                    _Retorno = False
                End If

            Else
                'Response.Redirect("frmLogin.aspx")
                _Retorno = False
            End If

            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()
            Return _Retorno
        Catch ex As Exception
        End Try


    End Function

    Protected Sub GetLinks()

        Try
            Dim grupoPermiso = Session("IDGrupo")
            Dim NombreEjecutivo = Session("NombreEjecutivo")
            Dim IDUsuario = Session("IDUsuario")

            Dim dtPermiso As DataTable

            navbardrop.InnerText = NombreEjecutivo

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Exec.grupoPermiso = grupoPermiso
            Exec.IDUsuario = IDUsuario

            Exec.GetPermiso(rp)


            dtPermiso = rp.Ds.Tables(0)

            If rp.TieneDatos = True Then

                For Each row As DataRow In dtPermiso.Rows

                    If (row("NombrePermiso") = "Administración") Then
                        navbarDropdown.Visible = True
                        row.Delete()
                    End If
                Next


                rptBotonera.DataSource = rp.Ds
                rptBotonera.DataBind()


            Else
                Response.Redirect("frmLogin.aspx")
            End If

            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()
        Catch ex As Exception
        End Try


    End Sub

    Protected Sub rptBotonera_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then

            If (navbarDropdown.Visible = True) Then

                Dim IDUsuario = Session("IDUsuario")
                Dim NombrePermiso As String = "Administración"

                Dim Exec As New Business.BANCHDB13
                Dim rp As New DotNetResponse.SQLPersistence

                Exec.NombrePermiso = NombrePermiso
                Exec.IDUsuario = IDUsuario

                Exec.GetSuperPermiso(rp)

                rptAdmin.DataSource = rp.Ds
                rptAdmin.DataBind()

            End If

        End If
    End Sub

End Class