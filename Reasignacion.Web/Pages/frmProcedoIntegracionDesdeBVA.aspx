﻿<%@ Page Language="vb" AutoEventWireup="false"  MasterPageFile="~/Pages/Sitio.Master" CodeBehind="frmProcedoIntegracionDesdeBVA.aspx.vb" Inherits="Reasignacion.Web.frmProcedoIntegracionDesdeBVA" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

   <script type="text/javascript">
       $(document).ready(function () {
           f_ProcesoIntegracionDesdeBVA();
       });




       function f_ProcesoIntegracionDesdeBVA() {


           $.ajax({
               type: "POST",
               url: "frmProcedoIntegracionDesdeBVA.aspx/Proceso",
               data: '{}',
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               success: onSuccess_Proceso,
               failure: function (response) {
                   alert(response.d);
               }
           });
       }

       function onSuccess_Proceso(response) {

           datos = JSON.parse(response.d);
           $(datos).each(function () {
               if (this.Respuesta == 'OK') {
                   showSuccess('Proceso Ejecutado de forma Satisfactoria');
               }
               else {
                   showError('Proceso terminado con Error, favor reportar');
               }
           });
       }





    </script>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<%--    <div class="content content--full">
    <div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Proceso Integracion Hacia BVA</h3></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-primary">
                    <div class="panel-body">

                         <div class="row">
                            <div class="col-md-12">

                <div class="container">
                    <table id="tbl_Data"
                        
                        class="table table-hover"
                        
                        data-toolbar="#toolbar"
                        data-search="true"
                        data-show-toggle="false"
                        data-show-columns="true"
                        data-show-export="true"
                        data-pagination="true"
                        
                        data-page-list="[10, 25, 50, 100, ALL]"
                        data-show-footer="false">
                    </table>
                </div>

                            </div>
                        </div>







                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        --%>
<div class="modal modal-header-success" role="dialog" tabindex="-1" id="CamposOcualtos">
    <asp:TextBox ID="txtIDPerfil" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtIDUsuario" runat="server" ></asp:TextBox>
</div>

    <script>
        //Activo
        document.getElementById('navbarDropdown').style.backgroundColor = "#128ff2";
        document.getElementById('navbarDropdown').style.borderColor = "#128ff2";
        //Normal
        document.getElementById('rptBotonera_primero_0').style.backgroundColor = "#00438c";
        document.getElementById('rptBotonera_primero_0').style.borderColor = "#356396";

        document.getElementById('rptBotonera_primero_1').style.backgroundColor = "#00438c";
        document.getElementById('rptBotonera_primero_1').style.borderColor = "#356396";

        document.getElementById('rptBotonera_primero_2').style.backgroundColor = "#00438c";
        document.getElementById('rptBotonera_primero_2').style.borderColor = "#356396";

        document.getElementById('rptBotonera_primero_3').style.backgroundColor = "#00438c";
        document.getElementById('rptBotonera_primero_3').style.borderColor = "#356396";

    </script>
</asp:Content>

