﻿Imports Newtonsoft.Json
Imports System.IO
Public Class frmIngresoSolicitud
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtExisteEnBVA_SN.Text = "N"
            txtIDUsuario.Text = Session("IDUsuario")
            txtIDPerfil.Text = Session("IDGrupo")
            If txtIDUsuario.Text = "" Then
                Response.Redirect("frmLogin.aspx")
            End If
        End If
    End Sub


    Public Shared Function ToJSON(pTable As DataTable) As String
        Dim vCadenaJSON As String = ""
        vCadenaJSON = JsonConvert.SerializeObject(pTable)
        If vCadenaJSON.Length <= 5 Then
            vCadenaJSON = "[{""NRO_SEGURO"":""0""}]"
        End If
        Return vCadenaJSON
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function Get_Seguro_BVA(_OrigenSeguro As String, _NroSeguro As String) As String

        Try


            Dim Exec As New Business.BANCHDB3
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._OrigenSeguro = _OrigenSeguro
            Exec._NroSeguro = _NroSeguro
            Exec.Get_SeguroBVA(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                'MsgBox(rp.Ds.Tables(0).Rows(0).Item(0))
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If

            ' ---------------------------------------------------------------------
            ' Seguros BIGSA
            ' ---------------------------------------------------------------------
            If _OrigenSeguro = "BIGSA" And rp.TieneDatos = False And rp.Errores = False Then
                Dim Exec_BIGSA As New Business.BANCHDB3
                Dim rp_BIGSA As New DotNetResponse.SQLPersistence

                Exec_BIGSA._OrigenSeguro = _OrigenSeguro
                Exec_BIGSA._NroSeguro = _NroSeguro
                Exec_BIGSA.Get_SeguroBIGSA(rp_BIGSA)
                dtJSON = Nothing
                If rp_BIGSA.TieneDatos Then
                    dtJSON = rp_BIGSA.Ds.Tables(0)
                End If
                If rp_BIGSA.Errores Then
                End If
                rp_BIGSA.Dispose()
                Exec_BIGSA.Dispose()
            End If


            ' ---------------------------------------------------------------------
            ' Seguros SCS o ISOL
            ' ---------------------------------------------------------------------
            If (_OrigenSeguro = "SCS" Or _OrigenSeguro = "ISOL") And rp.TieneDatos = False And rp.Errores = False Then
                Dim Exec_SCS_ISOL As New Business.BANCHDB3
                Dim rp_SCS_ISOL As New DotNetResponse.SQLPersistence

                Exec_SCS_ISOL._OrigenSeguro = _OrigenSeguro
                Exec_SCS_ISOL._NroSeguro = _NroSeguro
                Exec_SCS_ISOL.Get_SeguroSCS(rp_SCS_ISOL)
                dtJSON = Nothing
                If rp_SCS_ISOL.TieneDatos Then
                    dtJSON = rp_SCS_ISOL.Ds.Tables(0)
                End If
                If rp_SCS_ISOL.Errores Then
                End If
                rp_SCS_ISOL.Dispose()
                Exec_SCS_ISOL.Dispose()
            End If

            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function Get_Seguro_BVA2(_OrigenSeguro As String, _NroSeguro As String) As String

        Try


            Dim Exec As New Business.BANCHDB3
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._OrigenSeguro = _OrigenSeguro
            Exec._NroSeguro = _NroSeguro
            Exec.Get_SeguroBVA(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                'MsgBox(rp.Ds.Tables(0).Rows(0).Item(0))
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function Get_DatosSeguro(_OrigenSeguro As String, _NroSeguro As String) As String
        Try

            Dim Exec As New Business.BANCHDB3
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._NroSeguro = _NroSeguro
            If _OrigenSeguro = "SCS" Then
                Exec.Get_DatosSeguroSCS(rp)
            End If

            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function Get_diaHabil(_AnoVenta As String, _MesVenta As String, _dia_cierre As String) As String
        Try

            Dim Exec As New Business.BANCHDB3
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._AnoVenta = _AnoVenta
            Exec._MesVenta = _MesVenta
            Exec._dia_cierre = _dia_cierre

            Exec.Get_diaHabil(rp)

            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function Get_Funcionario(_Rut As String) As String
        Try

            Dim Exec As New Business.BANCHDB3
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._RutEjecutivo = _Rut
            Exec.Get_Funcionario(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                'MsgBox(rp.Ds.Tables(0).Rows(0).Item(0))
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    <System.Web.Services.WebMethod()>
    Public Shared Function Get_Ejecutivo(_Rut As String) As String
        Try

            Dim Exec As New Business.BANCHDB3
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._RutEjecutivo = _Rut
            Exec.Get_Ejecutivos(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                'MsgBox(rp.Ds.Tables(0).Rows(0).Item(0))
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function Get_EjecutivoBancaPropia(_Rut As String) As String
        Try

            Dim Exec As New Business.BANCHDB3
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._RutEjecutivo = _Rut
            Exec.Get_EjecutivoBancaPropia(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                'MsgBox(rp.Ds.Tables(0).Rows(0).Item(0))
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function Get_buscaBvaBigsaMaestra(_txtRutTitular As String, _txtNombreTitular As String) As String
        Try

            Dim Exec As New Business.BANCHDB3
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._txtRutTitular = _txtRutTitular
            Exec._txtNombreTitular = _txtNombreTitular
            Exec.Get_buscaBvaBigsaMaestra(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                'MsgBox(rp.Ds.Tables(0).Rows(0).Item(0))
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function Get_FechaCierreVenta() As String
        Try

            Dim Exec As New Business.BANCHDB3
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec.Get_FechaCierreVenta(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function Get_MotivoReasignacion() As String
        Try

            Dim Exec As New Business.BANCHDB3
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable


            Exec.Get_MotivoReasignacion(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then

                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    '<System.Web.Services.WebMethod()>
    'Public Shared Function Get_Reasignacion_x_Estado() As String
    '    Try

    '        Dim Exec As New Business.BANCHDB3
    '        Dim rp As New DotNetResponse.SQLPersistence

    '        Dim dtJSON As DataTable


    '        Exec.Get_Reasignacion_x_Estado(rp)
    '        dtJSON = Nothing
    '        If rp.TieneDatos Then

    '            dtJSON = rp.Ds.Tables(0)
    '        End If
    '        If rp.Errores Then
    '        End If
    '        rp.Dispose()
    '        Exec.Dispose()

    '        Return ToJSON(dtJSON)
    '    Catch ex As Exception
    '        Return Nothing
    '    End Try

    'End Function



    <System.Web.Services.WebMethod()>
    Public Shared Function Grabar(_IDReasignacion As String, _OrigenSeguro As String, _NroSeguro As String, _RutTitular As String, _RutEjecutivo As String, _MtoUF As String, _TipoProducto As String, _EstadoVenta As String, _CanalVenta As String, _MarcaReferido As String, _CUI As String, _MesContable As String, _AnnoContable As String, _IDMotivoReasignacion As String, _MotivoReasignacion As String, _RutEjecutivoReasignacion As String, _DVEjecutivoReasignacion As String, _NombreEjecutivoReasignacion As String, _CanalVentaReasignacion As String, _CUIReasignacion As String, _Observacion As String, _IDUsuario As String, _IDTipoSolicitud As String, _Sucursal As String, _Zona As String, _EsInternet As String, _ExisteEnBVA As String, _SucursalEje As String, _ZonaEje As String, _txtEsSupervisorInternet As String, _txtDiferenteFolioSN As String) As String
        Try

            Dim Exec As New Business.BANCHDB3
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._IDReasignacion = _IDReasignacion
            Exec._OrigenSeguro = _OrigenSeguro
            Exec._NroSeguro = _NroSeguro
            Exec._RutTitular = _RutTitular
            Exec._RutEjecutivo = _RutEjecutivo
            Exec._MtoUF = _MtoUF
            Exec._TipoProducto = _TipoProducto
            Exec._EstadoVenta = _EstadoVenta
            Exec._CanalVenta = _CanalVenta
            Exec._MarcaReferido = _MarcaReferido
            Exec._CUI = _CUI
            Exec._MesContable = _MesContable
            Exec._AnnoContable = _AnnoContable
            Exec._IDMotivoReasignacion = _IDMotivoReasignacion
            Exec._MotivoReasignacion = _MotivoReasignacion
            Exec._RutEjecutivoReasignacion = _RutEjecutivoReasignacion
            Exec._DVEjecutivoReasignacion = _DVEjecutivoReasignacion
            Exec._NombreEjecutivoReasignacion = _NombreEjecutivoReasignacion
            Exec._CanalVentaReasignacion = _CanalVentaReasignacion
            Exec._CUIReasignacion = _CUIReasignacion
            Exec._Observacion = _Observacion
            Exec._IDUsuario = _IDUsuario
            Exec._IDTipoSolicitud = _IDTipoSolicitud
            Exec._Sucursal = _Sucursal
            Exec._Zona = _Zona
            Exec._EsInternet = _EsInternet
            Exec._ExisteEnBVA = _ExisteEnBVA
            Exec._ZonaEje = _ZonaEje
            Exec._SucursalEje = _SucursalEje
            Exec._txtEsSupervisorInternet = _txtEsSupervisorInternet
            Exec._txtDiferenteFolioSN = _txtDiferenteFolioSN

            Exec.Put_Reasignacion(rp)

            dtJSON = Nothing
            If rp.TieneDatos Then
                'MsgBox(rp.Ds.Tables(0).Rows(0).Item(0))
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function




    <System.Web.Services.WebMethod()>
    Public Shared Function Get_DerivacionFirma(_IDUsuario As String) As String
        Try
            Dim Exec As New Business.BANCHDB3
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._IDUsuario = _IDUsuario

            Exec.Get_DerivacionFirma(rp)

            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function Get_EsDerivacionInternet(_IDUsuario As String) As String
        Try
            Dim Exec As New Business.BANCHDB3
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._IDUsuario = _IDUsuario

            Exec.Get_EsDerivacionInternet(rp)

            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function Put_SubirArchivo(_IDReasignacion As String, _NombreAdjunto As String, _TipoAdjunto As String, _Comentario As String, _ArchivoBase64 As String, _IDUsuario As String, _NroSeguro As String) As String
        Try
            Dim dtJSON As DataTable

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Exec._IDReasignacion = _IDReasignacion
            Exec._NombreAdjunto = _NombreAdjunto
            Exec._TipoAdjunto = _TipoAdjunto
            Exec._Comentario = _Comentario
            Exec._Adjunto_Doc = _ArchivoBase64
            Exec.IDUsuario = _IDUsuario
            Exec._NroSeguro = _NroSeguro

            Exec.Put_Adjunto(rp)

            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function




    'Protected Sub pbUpdateFile_click(sender As Object, e As EventArgs) Handles pbUpdateFile.Click

    '    Dim path As String = ConfigurationManager.AppSettings("Path_FileUpload")
    '    'Dim a As String = "H:"
    '    'ClientScript.RegisterStartupScript(Me.GetType(), "AlertMessageBox", "alert('" + a + "');", True)

    '    If txtComentario.Text = "" Then
    '        ClientScript.RegisterStartupScript(Me.GetType(), "AlertMessageBox", "alert('Debe ingresar una descripcion del archivo que esta ingresando');", True)
    '        Exit Sub
    '    End If


    '    Dim fileOK As Boolean = False
    '    Dim _NombreArchivo As String
    '    Dim _Adjunto As Byte()

    '    If fuSubirArchivo.HasFile Then
    '        Dim fileExtension As String
    '        fileExtension = IO.Path.GetExtension(fuSubirArchivo.FileName).ToLower()
    '        _NombreArchivo = fuSubirArchivo.FileName

    '        fuSubirArchivo.PostedFile.SaveAs(path & fuSubirArchivo.FileName)
    '        Dim szBase64 As String
    '        szBase64 = ConvertFileToBase64(path & fuSubirArchivo.FileName)


    '        Dim Exec As New Business.BANCHDB13
    '        Dim rp As New DotNetResponse.SQLPersistence


    '        Exec._IDReasignacion = 0 'txtNroSolicitudBCO.Text
    '        Exec._NombreAdjunto = fuSubirArchivo.FileName
    '        Exec._TipoAdjunto = fileExtension
    '        Exec._Comentario = txtComentario.Text
    '        Exec._Adjunto_Doc = szBase64
    '        Exec.IDUsuario = txtIDUsuario.Text
    '        Exec._NroSeguro = txtNroSeguro_Doc.Text
    '        Exec.Put_Adjunto(rp)

    '        If rp.TieneDatos Then

    '        End If
    '        If rp.Errores Then
    '            'MsgBox1.ShowMessage(rp.MensajeError)
    '            ClientScript.RegisterStartupScript(Me.GetType(), "AlertMessageBox", "alert('Carga Completada con Errores, favor Reportar');", True)
    '        Else

    '        End If

    '        rp.Dispose()
    '        Exec.Dispose()
    '    Else
    '        ClientScript.RegisterStartupScript(Me.GetType(), "AlertMessageBox", "alert('Debe seleccionar archivo a cargar');", True)
    '        Exit Sub

    '    End If

    '    'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Pop", "f_ListadoAdjuntos(" & txtIDDevolucion.Text & ");", True)
    '    ClientScript.RegisterStartupScript(Me.GetType(), "Pop", "AdjuntarDocumentacion();", True)

    'End Sub




    'Sub CargarFileDB(_NombreArchivo As String)

    '    Try
    '        Dim Exec As New Business.BANCHDB13
    '        Dim rp As New DotNetResponse.SQLPersistence


    '        'Exec._IDDevoluciones = txtNroSolicitudBCO.Text
    '        'Exec._NombreAdjunto = txtUsuario_Session.Text
    '        'Exec._TipoAdjunto = ""
    '        'Exec._Comentario = _Comentario.Text
    '        'Exec._NombreAdjunto = txtUsuario_Session.Text
    '        Exec._Adjunto_Doc = ""
    '        Exec.Put_Adjunto(rp)

    '        If rp.TieneDatos Then

    '        End If
    '        If rp.Errores Then
    '            'MsgBox1.ShowMessage(rp.MensajeError)
    '            ClientScript.RegisterStartupScript(Me.GetType(), "AlertMessageBox", "alert('Carga Completada con Errores, favor Reportar');", True)
    '        Else

    '        End If

    '        rp.Dispose()
    '        Exec.Dispose()

    '    Catch ex As Exception

    '        ClientScript.RegisterStartupScript(Me.GetType(), "AlertMessageBox", "alert('Carga Completada con Errores, favor Reportar');", True)

    '    End Try

    'End Sub


    Public Function ConvertFileToBase64(ByVal fileName As String) As String

        Dim ReturnValue As String = ""

        If My.Computer.FileSystem.FileExists(fileName) Then
            Using BinaryFile As FileStream = New FileStream(fileName, FileMode.Open)
                Dim BinRead As BinaryReader = New BinaryReader(BinaryFile)
                Dim BinBytes As Byte() = BinRead.ReadBytes(CInt(BinaryFile.Length))
                ReturnValue = Convert.ToBase64String(BinBytes)
                BinaryFile.Close()
            End Using
        End If
        Return ReturnValue
    End Function



    <System.Web.Services.WebMethod()>
    Public Shared Function Get_ListadoAdjunto(_NroSeguro As String) As String
        Try
            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Exec._NroSeguro = _NroSeguro

            Exec.Get_ListadoAdjuntoDevolucion(rp)

            Dim dtJSON As DataTable

            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function



    <System.Web.Services.WebMethod()>
    Public Shared Function Get_PermiteIngresarSeguro(_OrigenSeguro As String, _NroSeguro As String) As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._OrigenSeguro = _OrigenSeguro
            Exec._NroSeguro = _NroSeguro

            Exec.Get_PermiteIngresarSeguro(rp)

            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    <System.Web.Services.WebMethod()>
    Public Shared Function Get_Download(_IDReasignacion As String, _NroSeguro As String) As String
        Try
            Dim szPATH As String
            szPATH = ConfigurationManager.AppSettings("Path_FileDownload")

            Dim szNombreArchivo As String
            Dim szBase64 As String

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Exec._IDReasignacion = _IDReasignacion
            Exec._NroSeguro = _NroSeguro

            Exec.Get_Adjunto(rp)
            szNombreArchivo = "Error"
            If rp.TieneDatos Then
                szNombreArchivo = rp.Ds.Tables(0).Rows(0).Item("NombreAdjunto")
                szBase64 = rp.Ds.Tables(0).Rows(0).Item("Adjunto_Doc")

                If (System.IO.File.Exists(szPATH & "DB_" & szNombreArchivo)) Then
                    System.IO.File.Delete(szPATH & "DB_" & szNombreArchivo)
                End If

                ' get your base64 string... b64str

                Dim binaryData() As Byte = Convert.FromBase64String(szBase64)

                ' Assuming it's a jpg :)
                Dim fs As New FileStream(szPATH & "DB_" & szNombreArchivo, FileMode.CreateNew)

                ' Elimina archivo existente con el mismo nombre

                ' write it out
                fs.Write(binaryData, 0, binaryData.Length)

                ' close it down.
                fs.Close()
            End If
            If rp.Errores Then
                'MsgBox1.ShowMessage(rp.MensajeError)
                'ClientScript.RegisterStartupScript(Me.GetType(), "AlertMessageBox", "alert('Carga Completada con Errores, favor Reportar');", True)
            Else

            End If

            rp.Dispose()
            Exec.Dispose()

            ' Creamos el objeto DataTable
            Dim dt As DataTable = New DataTable("Datos")
            ' Creamos una nueva columna
            Dim dc_Archivo As New DataColumn("Archivo", Type.GetType("System.String"))

            ' Añadimos la columna al objeto DataTable
            dt.Columns.Add(dc_Archivo)

            Dim drG As DataRow = dt.NewRow
            ' Le Asignamos un valor a la columna Precio
            drG.Item("Archivo") = "DB_" & szNombreArchivo
            ' Añadimos la fila al objeto DataTable
            dt.Rows.Add(drG)

            Return ToJSON(dt)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function




    <System.Web.Services.WebMethod()>
    Public Shared Function Get_ValidarCUI(_CUI As String) As String
        Try
            Dim Exec As New Business.BANCHDB3
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._CUI = _CUI

            Exec.Get_ValidarCUI(rp)

            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

End Class