﻿<%@ Page Language="vb" AutoEventWireup="false"  MasterPageFile="~/Pages/Sitio.Master" CodeBehind="frmDerivacionFirma.aspx.vb" Inherits="Reasignacion.Web.frmDerivacionFirma" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {

            $(document).ajaxStart($.blockUI({ message: '<h1><img src="../Img/loading.gif" /> Procesando...</h1>' })).ajaxStop($.unblockUI);
            GetCoordinador();
            GetSupervisor();
            GetSupervisorInternet();
            GetGerente();

            GetDerivacionFirma();

            

            $("#btnGrabar").click(function () {

                var $Coordinador = $('#cboCoordinador').val();
                var $Supervisor = $('#cboSupervisor').val();
                var $SuperInt = $('#cboSupervisorInt').val();
                var $Gerente = $('#cboGerente').val();

                if ($Coordinador == "") {
                    showError('Debe ingresar Coordinador');
                    return
                }

            //    if ($Supervisor == "") {
            //        showError('Debe ingresar Supervisor');
            //        return
            //    }

            //    if ($SuperInt == "") {
            //        showError('Debe ingresar Supervisor Internet');
            //        return
            //    }

           //     if ($Gerente == "") {
           //         showError('Debe ingresar Gerente');
           //         return
           //     }
                if ($('#flagMantencion').val() == 'I') {
                    $.ajax({
                        type: "POST",
                        url: "frmDerivacionFirma.aspx/InsertDerivacionFirma",
                        data: '{UsuarioCoor: "' + $Coordinador + '", UsuarioSuper: "' + $Supervisor + '", UsuarioSuperInt: "' + $SuperInt + '", UsuarioGerente: "' + $Gerente + '" }',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: onSuccessUpdateDerivacionFirma,
                        failure: function (response) {
                            alert(response.d);
                        }
                    });
                }
                else {
                    $.ajax({
                        type: "POST",
                        url: "frmDerivacionFirma.aspx/UpdateDerivacionFirma",
                        data: '{UsuarioCoor: "' + $Coordinador + '", UsuarioSuper: "' + $Supervisor + '", UsuarioSuperInt: "' + $SuperInt + '", UsuarioGerente: "' + $Gerente + '" }',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: onSuccessUpdateDerivacionFirma,
                        failure: function (response) {
                            alert(response.d);
                        }
                    });
                }
            });

            $.unblockUI();

        });

        function onSuccessUpdateDerivacionFirma(response) {
            datos = JSON.parse(response.d);
            $(datos).each(function () {
                if (this.Respuesta == 'OK') {
                    showSuccess('Grabó Correctamente');
                    ClearValues();
                    GetDerivacionFirma();
                    $("#modal_derivacion").modal('hide');
                }
                else if (this.Respuesta == 'Encontro') {
                    showSuccess('Ya Existe');
                    return
                }
                else {
                    showError('ERROR al ingresar la información');
                    return;
                }
            });
            
           // showSuccess('Registro actualizado correctamente.');
            
            
        }


        function GetCoordinador() {

            $.ajax({
                type: "POST",
                url: "frmDerivacionFirma.aspx/GetCoordinador",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccessGetCoordinador,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function onSuccessGetCoordinador(response) {
            datos = JSON.parse(response.d);
            $(datos).each(function () {

                var option = $(document.createElement('option'));
                option.text(this.NombreCoordinador);
                option.val(this.Usuario);
                $("#cboCoordinador").append(option);
            });
        }

        function GetSupervisor() {

            $.ajax({
                type: "POST",
                url: "frmDerivacionFirma.aspx/GetSupervisor",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccessGetSupervisor,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function onSuccessGetSupervisor(response) {
            datos = JSON.parse(response.d);
            $(datos).each(function () {

                var option = $(document.createElement('option'));
                option.text(this.NombreSupervisor);
                option.val(this.Usuario);
                $("#cboSupervisor").append(option);
            });
        }

        function GetSupervisorInternet() {

            $.ajax({
                type: "POST",
                //url: "frmDerivacionFirma.aspx/GetSupervisorInternet",
                url: "frmDerivacionFirma.aspx/GetSupervisor",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccessGetSupervisorInternet,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function onSuccessGetSupervisorInternet(response) {
            datos = JSON.parse(response.d);
            $(datos).each(function () {

                var option = $(document.createElement('option'));
                //option.text(this.NombreSuperInternet);
                option.text(this.NombreSupervisor);
                option.val(this.Usuario);
                $("#cboSupervisorInt").append(option);
            });
        }

        function GetGerente() {

            $.ajax({
                type: "POST",
                url: "frmDerivacionFirma.aspx/GetGerente",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccessGetGerente,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function onSuccessGetGerente(response) {
            datos = JSON.parse(response.d);
            $(datos).each(function () {

                var option = $(document.createElement('option'));
                option.text(this.NombreGerente);
                option.val(this.Usuario);
                $("#cboGerente").append(option);
            });
        }

        function GetDerivacionFirma() {

            $.ajax({
                type: "POST",
                url: "frmDerivacionFirma.aspx/GetDerivacionFirma",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccessGetDerivacionFirma,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function GetListadoUsuarios() {

            $.ajax({
                type: "POST",
                url: "frmDerivacionFirma.aspx/GetListaUsuario",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccessGetListaUsuario,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function onSuccessGetDerivacionFirma(response) {
            datos = JSON.parse(response.d);
            $('#tbl_Data').bootstrapTable('destroy')
            $('#tbl_Data').bootstrapTable({
                columns: [
                    { field: 'Coordinador', title: 'Coordinador', align: 'center', visible: true, sortable: true }
                    , { field: 'Supervisor', title: 'Supervisor', align: 'center', sortable: true }
                    , { field: 'SupervisorInternet', title: 'Supervisor Internet', align: 'center', sortable: true }
                    , { field: 'Gerente', title: 'Gerente', align: 'center', sortable: true }
                    , { title: 'Editar', align: 'center', formatter: col_Editar, sortable: false }
                ]
                , data: datos
            });
        }

        function col_Editar(value, row, index) {
            return [
                '<button type="button" class="btn btn-default" aria-label="Left Align" data-toggle="tooltip" href="#menu1" title="Editar" onclick="Editar(\'' + row.Coordinador + '\', \'' + row.Supervisor + '\', \'' + row.SupervisorInternet + '\', \'' + row.Gerente + '\', \'M\');">',
                '<span class="fas fa-edit fa-lg" aria-hidden="true"></span>',
                '</button>'
            ].join('');
        }

        function Editar(Coordinador, Supervisor, SupervisorInternet, Gerente,flagMantencion) {
            $("#cboCoordinador").val(Coordinador).change();
            $("#cboSupervisor").val(Supervisor).change();
            $("#cboSupervisorInt").val(SupervisorInternet).change();
            $("#cboGerente").val(Gerente).change();
            $('#flagMantencion').val(flagMantencion);
            $('#modal_derivacion').modal();
        }


        function ClearValues() {

            $('#cboCoordinador').val('');
            $('#cboSupervisor').val('');
            $('#cboSupervisorInt').val('');
            $('#cboGerente').val('');
            $('#flagMantencion').val('');
            
        }

        function f_Exportar() {
                $.ajax({
                    type: "POST",
                    url: "frmDerivacionFirma.aspx/GetDerivacionFirma",
                    data: '{}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: onSuccessExportarExcel,
                    failure: function (response) {
                        alert(response.d);
                    }
                });
        }
            //$("#btnExportar").click(function () {
            //    alert('1');


            //});

        function onSuccessExportarExcel(response) {
            datos = JSON.parse(response.d);

            var createXLSLFormatObj = [];

            var xlsHeader = ["Coordinador", "Supervisor", "SupervisorInternet", "Gerente"];

            var xlsRows = datos;

            createXLSLFormatObj.push(xlsHeader);

            $.each(xlsRows, function (index, value) {
                var innerRowData = [];
                // $("tbody").append('<tr><td>' + value.IDUsuario + '</td><td>' + value.Usuario + '</td></tr>');
                $.each(value, function (ind, val) {

                    innerRowData.push(val);
                });
                createXLSLFormatObj.push(innerRowData);
            });

            /* File Name */
            var filename = "ListadoDerivaciones.xlsx";

            /* Sheet Name */
            var ws_name = "Derivaciones";

            if (typeof console !== 'undefined') console.log(new Date());
            var wb = XLSX.utils.book_new(),
                ws = XLSX.utils.aoa_to_sheet(createXLSLFormatObj);

            /* Add worksheet to workbook */
            XLSX.utils.book_append_sheet(wb, ws, ws_name);

            /* Write workbook and Download */
            if (typeof console !== 'undefined') console.log(new Date());
            XLSX.writeFile(wb, filename);
            if (typeof console !== 'undefined') console.log(new Date());

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="content content--full">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Derivación de Firma</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">

                        <div class="panel panel-primary">
                            <div class="panel-body text-center">
                                <button type="button" class="btn btn-default" aria-label="Left Align" data-toggle="tooltip" href="#menu1" title="Agregar Derivación Firma" onclick="Editar('', '', '', '','I');">
                                        <img src="../Img/btnAgregar3.jpg" style="border:none;" width="" height=""/>
                                </button>
                                
                                <div class="modal modal-header-success" role="dialog" tabindex="-1" id="hdd">
                                    <input id="txtCoordinadorHidden" value="" type="text" />
                                    <input id="txtSupervisorHidden" value="" type="text" />
                                    <input id="txtSuperIntHidden" value="" type="text" />
                                    <input id="txtGerenteHidden" value="" type="text" />
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div>
                                            <div class="container">
                                                <table id="tbl_Data"
                                                    <%--class="table table-bordered table-hover col-xs-12"--%>
                                                    class="table table-hover"
                                                    <%--data-click-to-select="true"--%>
                                                    data-toolbar="#toolbar"
                                                    data-search="true"
                                                    data-show-toggle="false"
                                                    data-show-columns="false"
                                                    data-show-export="false"
                                                    data-pagination="true"
                                                    <%--data-id-field="Id"--%>
                                                    data-page-list="[10, 25, 50, 100, ALL]"
                                                    data-show-footer="false">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="modal modal-header-success" role="dialog" tabindex="-1" id="modal_derivacion">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h6 class="modal-title text-white">Mantenedor Usuario</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: white;">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                 <div class="panel-group">
                     <div class="panel panel-default">
                           <div id="collapse1" class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                          <div class="row">
                                               <div class=" col-md-6 form-group">
                                                    <label for="exampleFormControlSelect1">Gerente</label>
                                                    <select class="form-control input-sm" id="cboGerente" tabindex="1"></select>
                                                </div>
                                                <div class=" col-md-6 form-group">
                                                    <label for="exampleFormControlSelect1">Coordinador</label>
                                                    <select class="form-control input-sm" id="cboCoordinador" tabindex="3"></select>
                                                </div>
                                          </div>

                                          <div class="row">
                                               <div class=" col-md-6 form-group">
                                                    <label for="exampleFormControlSelect1">Supervisor</label>
                                                    <select class="form-control input-sm" id="cboSupervisor" tabindex="2"></select>
                                               </div>
                                                        

                                               <div class=" col-md-6 form-group">
                                                   <label for="exampleFormControlSelect1">Supervisor Internet</label>
                                                   <select class="form-control input-sm" id="cboSupervisorInt" tabindex="4"></select>
                                               </div>
                                          </div>
                                          <br />
                                          <div class="row">
                                               <div class="col-md-4"></div>
                                               <div class="col-md-4"></div>
                                               <div class="col-md-2">
                                                   <button class="btn btn-primary form-control" onclick="f_Exportar();" type="button" id="btnExportar">Exportar</button>
                                               </div>
                                               <div class="col-md-2">
                                                   <button class="btn btn-primary form-control" onclick="" type="button" id="btnGrabar">Grabar</button>
                                               </div>
                                          </div>
                                          <br />
                                    </div>
                                </div>
                           </div>
                     </div>
                  </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-header-success" role="dialog" tabindex="-1" id="CamposOcualtos">
     <asp:TextBox ID="txtIDPerfil" runat="server"></asp:TextBox>
     <asp:TextBox ID="txtIDUsuario" runat="server" ></asp:TextBox>
     <input id="flagMantencion" value="" type="text" />
</div>

<script>
    //Activo
    document.getElementById('navbarDropdown').style.backgroundColor = "#128ff2";
    document.getElementById('navbarDropdown').style.borderColor = "#128ff2";
    //Normal
    document.getElementById('rptBotonera_primero_0').style.backgroundColor = "#00438c";
    document.getElementById('rptBotonera_primero_0').style.borderColor = "#356396";

    document.getElementById('rptBotonera_primero_1').style.backgroundColor = "#00438c";
    document.getElementById('rptBotonera_primero_1').style.borderColor = "#356396";

    document.getElementById('rptBotonera_primero_2').style.backgroundColor = "#00438c";
    document.getElementById('rptBotonera_primero_2').style.borderColor = "#356396";
   
    document.getElementById('rptBotonera_primero_3').style.backgroundColor = "#00438c";
    document.getElementById('rptBotonera_primero_3').style.borderColor = "#356396";

    </script>

</asp:Content>
