﻿Imports Newtonsoft.Json
Imports System.Web.Services

Public Class frmActMotivos
    Inherits Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            txtIDUsuario.Text = Session("IDUsuario")
            txtIDPerfil.Text = Session("IDGrupo")
            If txtIDUsuario.Text = "" Then
                Response.Redirect("frmLogin.aspx")
            End If
        End If
    End Sub
    Public Shared Function ToJSON(pTable As DataTable) As String
        Dim vCadenaJSON As String = ""
        vCadenaJSON = JsonConvert.SerializeObject(pTable)
        If vCadenaJSON.Length <= 5 Then
            vCadenaJSON = "[{""Nro_Seguro"":""0""}]"
        End If
        Return vCadenaJSON
    End Function

    <WebMethod()>
    Public Shared Function GetMotivosReasig() As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec.GetMotivoReasignacion(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <WebMethod()>
    Public Shared Function DelMotivo(IDMotivoReasignacion As String) As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._IDMotivoReasignacion = IDMotivoReasignacion
            Exec.DelMotivo(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <WebMethod()>
    Public Shared Function InsertMotivosReasig(MotivoReasig As String) As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._MotivoReasignacion = MotivoReasig
            Exec.InsertMotivosReasig(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <WebMethod()>
    Public Shared Function UpdateMotivosReasig(IDMotivoReasignacion As String, MotivoReasig As String) As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._IDMotivoReasignacion = IDMotivoReasignacion
            Exec._MotivoReasignacion = MotivoReasig

            Exec.UpdateMotivosReasig(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


End Class