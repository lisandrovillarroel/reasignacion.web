﻿Public Class Sitio
    Inherits System.Web.UI.MasterPage
    Public _Menu As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim a As String
        'a = Request.ServerVariables("AUTH_USER").ToString()
        'AdmOpc.Disabled = True
        GetLinks()

    End Sub

    Protected Sub GetLinks()

        Try
            Dim grupoPermiso = Session("IDGrupo")
            Dim NombreEjecutivo = Session("NombreEjecutivo")
            Dim IDUsuario = Session("IDUsuario")

            Dim dtPermiso As DataTable

            navbardrop.InnerText = NombreEjecutivo

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Exec.grupoPermiso = grupoPermiso
            Exec.IDUsuario = IDUsuario

            Exec.GetPermiso(rp)


            dtPermiso = rp.Ds.Tables(0)

            If rp.TieneDatos = True Then
                CrearMenu(rp.Ds.Tables(0))

            Else
                Response.Redirect("frmLogin.aspx")
            End If

            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()
        Catch ex As Exception
        End Try


    End Sub
    Private Sub CrearMenu(ByRef Tabla As System.Data.DataTable)
        Dim szMenu As String
        szMenu = "<ul class='nav navbar-nav'> "

        CrearMenu(Tabla, 0, Nothing, 0, 1)

        szMenu = szMenu + _Menu + " </ul>"

        NewMenu.Text = szMenu

    End Sub

    Private Sub CrearMenu(ByRef Tabla As System.Data.DataTable, ByVal indicePadre As Integer, ByVal nodePadre As TreeNode, ByVal CantHijos As Integer, ByVal HijosCargados As Integer)
        Try
            Dim dataViewHijos As New System.Data.DataView(Tabla)
            dataViewHijos.RowFilter = Tabla.Columns("IDPermisoMaestro").ColumnName + " = " + indicePadre.ToString

            Dim dataRowCurrent As System.Data.DataRowView

            For Each dataRowCurrent In dataViewHijos
                If dataRowCurrent("Url").ToString().Trim() = "#" Then
                    CantHijos = dataRowCurrent("CantidadHijos").ToString().Trim()
                    HijosCargados = 0
                    Dim _Class = dataRowCurrent("Nombre").ToString().Trim().Replace(" ", "")
                    _Menu = _Menu + " <li class='dropdown " & _Class & "'><a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='True' aria-expanded='False'>" & dataRowCurrent("Nombre").ToString().Trim() & "<span class='caret'></span></a>"
                    _Menu = _Menu + " <ul class='dropdown-menu'>"
                Else
                    Dim _Class = Mid(dataRowCurrent("Url").ToString().Trim(), 1, InStr(dataRowCurrent("Url").ToString().Trim(), ".") - 1)
                    _Menu = _Menu + " <li class='" & _Class & "'><a href='" & dataRowCurrent("Url").ToString().Trim() & "'>" & dataRowCurrent("Nombre").ToString().Trim() & "</a></li> "
                    HijosCargados = HijosCargados + 1
                    If CantHijos = HijosCargados Then
                        _Menu = _Menu + " </ul> </li>"
                    End If
                End If
                CrearMenu(Tabla, CInt(dataRowCurrent("IDPermiso")), Nothing, CantHijos, HijosCargados)
            Next
        Catch ex As Exception

        End Try

    End Sub



End Class