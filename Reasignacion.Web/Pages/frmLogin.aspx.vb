﻿Imports Newtonsoft.Json
Imports HttpContext.Current.Session
Imports System.IO
Imports System.Security.Cryptography
Public Class frmLogin
    Inherits Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormsAuthentication.SignOut()
        Session.Clear()
        Session.Abandon()
        ClientScript.RegisterStartupScript(Me.GetType(), "AlertMessageBox", "ShowPopUps();", True)
    End Sub


    Public Shared Function ToJSON(pTable As DataTable) As String
        Dim vCadenaJSON As String = ""
        vCadenaJSON = JsonConvert.SerializeObject(pTable)
        If vCadenaJSON.Length <= 5 Then
            vCadenaJSON = "[{""Nro_Seguro"":""0""}]"
        End If
        Return vCadenaJSON
    End Function

    Protected Sub btnIngresar_Click(sender As Object, e As EventArgs)

        Dim usuario, pass As String


        Try
            usuario = txtUsuario.Text
            pass = txtPassword.Text

            If usuario = "" Or pass = "" Then
                Exit Sub
            End If
            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            'Dim dtJSON As DataTable

            Exec.usuario = usuario
            Exec.password = Encrypt(pass.Trim())

            Exec.GetUsuario(rp)
            ' dtJSON = Nothing

            If rp.TieneDatos = True Then

                If rp.Ds.Tables(0).Rows(0).Item("Respuesta") = "ER" Then
                    'ClientScript.RegisterStartupScript(Me.GetType(), "AlertMessageBox", "showError('Valide los datos ingresados para Usuario y Clave');", True)
                    Exit Sub
                End If

                'MsgBox(rp.Ds.Tables(0).Rows(0).Item(0))
                'dtJSON = rp.Ds.Tables(0)
                Session("NombreEjecutivo") = rp.Ds.Tables(0).Rows(0).Item(0)
                Session("Usuario") = rp.Ds.Tables(0).Rows(0).Item(1)
                Session("Email") = rp.Ds.Tables(0).Rows(0).Item(2)
                Session("IDGrupo") = rp.Ds.Tables(0).Rows(0).Item(3)
                Session("IDUsuario") = rp.Ds.Tables(0).Rows(0).Item(4)
                Response.Redirect("frmIngresoSolicitud.aspx")
            Else

                'ClientScript.RegisterStartupScript(Me.GetType(), "AlertMessageBox", "showError('Valide los datos ingresados para Usuario y Clave');", True)
                ClientScript.RegisterStartupScript(Me.GetType(), "AlertMessageBox", "ShowPopUps();", True)

            End If

            If rp.Errores Then

            End If

            'Response.Redirect("frmIngresoSolicitud.aspx")

            rp.Dispose()
            Exec.Dispose()

            ' Return ToJSON(dtJSON)
        Catch ex As Exception
            ' Return Nothing
        End Try

    End Sub




    <System.Web.Services.WebMethod()>
    Public Shared Function f_ValidaUsuario(_Usuario As String, _Password As String) As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec.usuario = _Usuario
            Exec.password = Encrypt(_Password.Trim)

            Exec.GetUsuario(rp)

            dtJSON = Nothing
            If rp.TieneDatos Then
                If rp.Ds.Tables(0).Rows(0).Item(0) <> "ER" Then
                    HttpContext.Current.Session("NombreEjecutivo") = rp.Ds.Tables(0).Rows(0).Item(0)
                    HttpContext.Current.Session("Usuario") = rp.Ds.Tables(0).Rows(0).Item(1)
                    HttpContext.Current.Session("Email") = rp.Ds.Tables(0).Rows(0).Item(2)
                    HttpContext.Current.Session("IDGrupo") = rp.Ds.Tables(0).Rows(0).Item(3)
                    HttpContext.Current.Session("IDUsuario") = rp.Ds.Tables(0).Rows(0).Item(4)
                End If
                dtJSON = rp.Ds.Tables(0)
                End If
                If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function f_enviaClave(_Usuario As String) As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Dim Respuesta As String
            Dim _destinatario As String
            Dim _clave As String
            Dim _nombreCompleto As String


            Exec.usuario = _Usuario

            Exec.GetEnviaClave(rp)

            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If

            Respuesta = dtJSON.Rows(0).Field(Of String)("Respuesta")
            _destinatario = dtJSON.Rows(0).Field(Of String)("Correo")
            _clave = dtJSON.Rows(0).Field(Of String)("Clave")
            _nombreCompleto = dtJSON.Rows(0).Field(Of String)("nombreCompleto")


            If Respuesta = "OK" And rp.Errores = False Then
                Dim Exec_envioCorreo As New Business.BANCHDB13
                Dim rp_envioCorreo As New DotNetResponse.SQLPersistence

                Dim dtJSON_enviocorreo As DataTable

                Exec.destinatario = _destinatario
                Exec.clave = Decrypt(_clave)
                Exec.nombre = _nombreCompleto
                Exec.usuario = _Usuario
                Exec.GetEnviaCorreo(rp_envioCorreo)
                If rp_envioCorreo.TieneDatos Then
                    dtJSON_enviocorreo = rp_envioCorreo.Ds.Tables(0)
                End If
            End If


            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return ex.Message
        End Try

    End Function
    Private Shared Function Encrypt(clearText As String) As String
        Dim EncryptionKey As String = "MAKV2SPBNI99212"
        Dim clearBytes As Byte() = Encoding.Unicode.GetBytes(clearText)
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, _
             &H65, &H64, &H76, &H65, &H64, &H65, _
             &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using ms As New MemoryStream()
                Using cs As New CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write)
                    cs.Write(clearBytes, 0, clearBytes.Length)
                    cs.Close()
                End Using
                clearText = Convert.ToBase64String(ms.ToArray())
            End Using
        End Using
        Return clearText
    End Function

    Private Shared Function Decrypt(cipherText As String) As String
        Dim EncryptionKey As String = "MAKV2SPBNI99212"
        Dim cipherBytes As Byte() = Convert.FromBase64String(cipherText)
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, _
             &H65, &H64, &H76, &H65, &H64, &H65, _
             &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using ms As New MemoryStream()
                Using cs As New CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write)
                    cs.Write(cipherBytes, 0, cipherBytes.Length)
                    cs.Close()
                End Using
                cipherText = Encoding.Unicode.GetString(ms.ToArray())
            End Using
        End Using
        Return cipherText
    End Function
End Class