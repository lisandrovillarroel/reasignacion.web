﻿Public Class Sitio
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim a As String
        'a = Request.ServerVariables("AUTH_USER").ToString()
        AdmOpc.Disabled = True
        GetLinks()

    End Sub

    Protected Sub GetLinks()

        Try
            Dim grupoPermiso = Session("IDGrupo")
            Dim NombreEjecutivo = Session("NombreEjecutivo")
            Dim IDUsuario = Session("IDUsuario")

            Dim dtPermiso As DataTable

            navbardrop.InnerText = NombreEjecutivo

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Exec.grupoPermiso = grupoPermiso
            Exec.IDUsuario = IDUsuario

            Exec.GetPermiso(rp)


            dtPermiso = rp.Ds.Tables(0)

            If rp.TieneDatos = True Then

                For Each row As DataRow In dtPermiso.Rows

                    If (row("NombrePermiso") = "Administración") Then
                        AdmOpc.Disabled = False
                        row.Delete()
                    End If
                Next


                rptBotonera.DataSource = rp.Ds
                rptBotonera.DataBind()


            Else
                Response.Redirect("frmLogin.aspx")
            End If

            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()
        Catch ex As Exception
        End Try


    End Sub

    Protected Sub rptBotonera_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then

            If (AdmOpc.Disabled = False) Then

                Dim IDUsuario = Session("IDUsuario")
                Dim NombrePermiso As String = "Administración"

                Dim Exec As New Business.BANCHDB13
                Dim rp As New DotNetResponse.SQLPersistence

                Exec.NombrePermiso = NombrePermiso
                Exec.IDUsuario = IDUsuario

                Exec.GetSuperPermiso(rp)

                rptAdmin.DataSource = rp.Ds
                rptAdmin.DataBind()

            End If

        End If
    End Sub

End Class