﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Pages/Sitio.Master" CodeBehind="frmMantUsuario.aspx.vb" Inherits="Reasignacion.Web.frmMantUsuario" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).ajaxStart($.blockUI({ message: '<h1><img src="../Img/loading.gif" /> Procesando...</h1>' })).ajaxStop($.unblockUI);
            GetGrupo();
            GetListadoUsuario();
            $(".pasteOnlyNumbers").on('paste', function (e) {
                //alert(e.originalEvent.clipboardData.getData('Text'));
                if (e.originalEvent.clipboardData.getData('Text').match(/[^\d]/)) {
                    e.preventDefault();
                }
            });

            $(".onlyNumbers").on("keypress", function (e) {
                // console.log(e.keyCode);
                if (e.keyCode < 48 || e.keyCode > 57) {
                    return false;
                }
            });

            $("input.nombre").bind('keypress', function (event) {
                var regex = new RegExp("^[a-zA-Z ]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });

            $(".FiltroCaracteresEspeciales").on('keypress', function (e) {
              //  alert('aaaa');
              //  alert(e.originalEvent.keypress.getData('Text'));
                //alert(e.originalEvent.keypress.getData('Text'));
            //    if (e.originalEvent.keypress.getData('Text').match(/['"°|;]/)) {
            //        e.preventDefault();
            //    }
            });



            //var _txtComentario = $('#txtComentario');

            //_txtComentario.val(_txtComentario.val().replace(/['"°|;]/g, ''));

            //_txtComentario.keypress(function (event) {
            //    if (event.keyCode == 13) {
            //        event.preventDefault();
            //    }
            //});


            $(".dvCalc").on("keyup", function (e) {
                var idDv = this.id.replace("Rut", "Dv");
                var T = this.value, M = 0, S = 1, dvCalc;
                for (; T; T = Math.floor(T / 10))
                    S = (S + T % 10 * (9 - M++ % 6)) % 11;

                dvCalc = S ? S - 1 : 'K';

                $('#' + idDv).val(dvCalc);

                //$('#txtDvEjc').val(dvCalc);
            });

            $("#btnLimpiar").click(function () {
                ClearValues();
                GetListadoUsuario();
            });

            $("#btnExportar").click(function () {

                $.ajax({
                    type: "POST",
                    url: "frmMantUsuario.aspx/GetListUsers",
                    data: '{}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: onSuccessGetListAllUsuario,
                    failure: function (response) {
                        alert(response.d);
                    }
                });

            });

            $("#btnGrabar").click(function () {
                var $IDUsuario = $('#txtIDUsuarioHdd').val();
                var $Usuario = $('#txtUsuario').val();
             //   var $RutUsuario = $('#txtRutUsuario').val();
             //   var $DvUsuario = $('#txtDvUsuario').val();
                var $NombreUsuario = $('#txtNombre').val();
                var $ApePat = $('#txtApePat').val();
                var $ApeMat = $('#txtApeMat').val();
                var $Mail = $('#txtMail').val();
                var $Pass = $('#txtPassw').val();
                var $IdGrupo = $('#cboPerfil').val();
                var $IDvigente = $('#cboVigente').val();

                var returnValidationValue = fnValidarDatos();

                if (returnValidationValue == true) {

                    if ($IDUsuario == "") {

                        $.ajax({
                            type: "POST",
                            url: "frmMantUsuario.aspx/InsertUsuario",
                            //  data: '{Usuario: "' + $Usuario + '", Rut: "' + $RutUsuario + '", Dv: "' + $DvUsuario + '", Nombre: "' + $NombreUsuario + '", ApePaterno: "' + $ApePat + '", ApeMaterno: "' + $ApeMat + '", Mail: "' + $Mail + '", Clave: "' + $Pass + '", IDGrupo: "' + $IdGrupo + '", Vigente: "' + $IDvigente + '" }',
                            data: '{Usuario: "' + $Usuario + '", Nombre: "' + $NombreUsuario + '", ApePaterno: "' + $ApePat + '", ApeMaterno: "' + $ApeMat + '", Mail: "' + $Mail + '", Clave: "' + $Pass + '", IDGrupo: "' + $IdGrupo + '", Vigente: "' + $IDvigente + '" }',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: onSuccessInsertUsuario,
                            failure: function (response) {
                                alert(response.d);
                            }
                        });
                    }
                    else {
                        $.ajax({
                            type: "POST",
                            url: "frmMantUsuario.aspx/UpdateUsuario",
                            //data: '{IDUsuario: "' + $IDUsuario + '", Usuario: "' + $Usuario + '", Rut: "' + $RutUsuario + '", Dv: "' + $DvUsuario + '", Nombre: "' + $NombreUsuario + '", ApePaterno: "' + $ApePat + '", ApeMaterno: "' + $ApeMat + '", Mail: "' + $Mail + '", Clave: "' + $Pass + '", IDGrupo: "' + $IdGrupo + '", Vigente: "' + $IDvigente + '" }',
                            data: '{IDUsuario: "' + $IDUsuario + '", Usuario: "' + $Usuario + '", Nombre: "' + $NombreUsuario + '", ApePaterno: "' + $ApePat + '", ApeMaterno: "' + $ApeMat + '", Mail: "' + $Mail + '", Clave: "' + $Pass + '", IDGrupo: "' + $IdGrupo + '", Vigente: "' + $IDvigente + '" }',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: onSuccessUpdateUsuario,
                            failure: function (response) {
                                alert(response.d);
                            }
                        });
                    }
                }

            });

            //$.unblockUI();


        });


        function onSuccessInsertUsuario(response) {
            
           datos = JSON.parse(response.d);
           $(datos).each(function () {
               if (this.Respuesta == 'OK') {
                   showSuccess('Registro Ingresado correctamente');
                   ClearValues();
               }
               else if (this.Respuesta == 'Encontro') {
                   showSuccess('Usuario Ya Existe');
               }
               else {
                   showError('ERROR al ingresar la información');
                   return;
               }
           });

            
            GetListadoUsuario();
        }

        function onSuccessUpdateUsuario(response) {

           datos = JSON.parse(response.d);
           $(datos).each(function () {
               if (this.Respuesta == 'OK') {
                   showSuccess('Registro actualizado correctamente');
                   $("#modal_Usuario").modal('hide');
               }
               else if (this.Respuesta == 'ER') {
                   showSuccess('Valide que usuario no tenga Aprobaciones Pendiente');
               }
               else {
                   showError('ERROR al actualizar la información');
                   return;
               }
           });
            ClearValues();
            GetListadoUsuario();
        }



        function fnValidarDatos() {
            var returnValidationValue = true;

            try {

                if ($('#txtPassw').val() != $('#txtRePassw').val()) {
                    showError('Las contraseñas no son iguales.');
                    $('#txtPassw').attr("class", "form-control is-invalid");
                    $('#txtRePassw').attr("class", "form-control is-invalid");
                    var returnValidationValue = false;
                }
                else if ($('#txtPassw').val().length < 8) {
                    showError('La contraseña debe ser mayor o igual a 8 caracteres.');
                    $('#txtPassw').attr("class", "form-control is-invalid");
                    var returnValidationValue = false;
                }
                else {
                    $('#txtPassw').attr("class", "form-control is-valid");
                    $('#txtRePassw').attr("class", "form-control is-valid");
                }

                if ($('#txtMail').val() == "") {
                    showError('Debe ingresar el mail del usuario.');
                    $('#txtMail').attr("class", "form-control is-invalid");
                    var returnValidationValue = false;
                }
                else {
                    var filter = /^[A-Za-z][A-Za-z0-9_.]*@[A-Za-z0-9_]+\.[A-Za-z0-9_.]+[A-za-z]$/;

                    valido = true;

                    if (!filter.test($('#txtMail').val())) {
                        valido = false;
                    }
                    if (valido == false) {
                        showError("Ingrese un mail del usuario válido. ");
                        $('#txtMail').attr("class", "form-control is-invalid");
                        returnValidationValue = false;
                    }
                    else {

                        $('#txtMail').attr("class", "form-control is-valid");
                    }
                }

                if ($('#txtRePassw').val() == "") {
                    showError('Debe ingresar repetir contraseña del Usuario.');
                    $('#txtRePassw').attr("class", "form-control is-invalid");
                    var returnValidationValue = false;
                }

                if ($('#txtApeMat').val() == "") {
                    showError('Debe ingresar apellido materno del Usuario.');
                    $('#txtApeMat').attr("class", "form-control is-invalid");
                    var returnValidationValue = false;
                }
                else {
                    $('#txtApePat').attr("class", "form-control is-valid");
                }

                if ($('#txtApePat').val() == "") {
                    showError('Debe ingresar apellido paterno del Usuario.');
                    $('#txtApePat').attr("class", "form-control is-invalid");
                    var returnValidationValue = false;
                }
                else {
                    $('#txtApePat').attr("class", "form-control is-valid");
                }


                if ($('#txtNombre').val() == "") {
                    showError('Debe ingresar un Nombre de Usuario.');
                    $('#txtNombre').attr("class", "form-control is-invalid");
                    var returnValidationValue = false;
                }
                else {
                    $('#txtNombre').attr("class", "form-control is-valid");
                }


            /*    if ($('#txtRutUsuario').val() == "") {
                    showError('Debe ingresar el Rut del usuario.');
                    $('#txtRutUsuario').attr("class", "form-control is-invalid");
                    var returnValidationValue = false;
                }
                else {
                    $('#txtRutUsuario').attr("class", "form-control is-valid");
                }

            */
                if ($('#txtUsuario').val() == "") {
                    showError('Debe ingresar el identificador del Usuario.');
                    $('#txtUsuario').attr("class", "form-control is-invalid");
                    var returnValidationValue = false;
                }
                else {
                    $('#txtUsuario').attr("class", "form-control is-valid");
                }

            }
            catch (e) {
                var x = "";
                alert(e);
            }
            return returnValidationValue;
        }

        function GetListadoUsuario() {

            $.ajax({
                type: "POST",
                url: "frmMantUsuario.aspx/GetListUsers",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccessGetListUsuario,
                failure: function (response) {
                    alert(response.d);
                }
            });

        }

        function onSuccessGetListUsuario(response) {
            datos = JSON.parse(response.d);
            $('#tbl_Data').bootstrapTable('destroy')
            $('#tbl_Data').bootstrapTable({
                columns: [
                    { field: 'IDUsuario', title: 'IDUsuario', visible: false, sortable: true }
                    , { field: 'Usuario', title: 'Usuario', sortable: true }
                  //  , { field: 'Rut', title: 'Rut', sortable: true }
                  //  , { field: 'Dv', title: 'Dv', visible: false, sortable: true }
                    , { field: 'Nombre', title: 'Nombre', sortable: true }
                    , { field: 'ApePaterno', title: 'Apellido Paterno', sortable: true }
                    , { field: 'ApeMaterno', title: 'Apellido Materno', sortable: true }
                    , { field: 'Email', title: 'Email', sortable: true }
                    , { field: 'Vigente', title: 'Vigente', sortable: true }
                    , { field: 'IDGrupo', title: 'IDGrupo', visible: false, sortable: true }
                    , { field: 'Perfil', title: 'Perfil', sortable: true }
                    , { title: 'Editar', align: 'center', formatter: col_Editar, sortable: false }
                ]
                , data: datos
            });
        }

        function col_Editar(value, row, index) {
            return [
                //'<button type="button" class="btn btn-default" aria-label="Left Align" data-toggle="tooltip" href="#menu1" title="Editar" onclick="Editar(\'' + row.IDUsuario + '\', \'' + row.Usuario + '\', \'' + row.Rut + '\', \'' + row.Dv + '\', \'' + row.Nombre + '\', \'' + row.ApePaterno + '\', \'' + row.ApeMaterno + '\', \'' + row.Email + '\', \'' + row.Vigente + '\', \'' + row.IDGrupo + '\', \'' + row.Perfil + '\', \'' + row.Clave + '\');">',
                '<button type="button" class="btn btn-default" aria-label="Left Align" data-toggle="tooltip" href="#menu1" title="Editar" onclick="Editar(\'' + row.IDUsuario + '\', \'' + row.Usuario + '\', \'' + row.Nombre + '\', \'' + row.ApePaterno + '\', \'' + row.ApeMaterno + '\', \'' + row.Email + '\', \'' + row.Vigente + '\', \'' + row.IDGrupo + '\', \'' + row.Perfil + '\', \'' + row.Clave + '\');">',
                '<span class="fas fa-edit fa-lg" aria-hidden="true"></span>',
                '</button>'
            ].join('');
        }

        function agregarUsuatio() {
            ClearValues();
            $('#modal_Usuario').modal();
            $("#txtRutUsuario").focus();
        }
        //function Editar(IDUsuario, Usuario, Rut, Dv, Nombre, ApePaterno, ApeMaterno, Email, Vigente, IDGrupo, Perfil, Clave) {
        function Editar(IDUsuario, Usuario, Nombre, ApePaterno, ApeMaterno, Email, Vigente, IDGrupo, Perfil, Clave) {
            $("#txtUsuario").prop("disabled", true);

            $("#txtRutUsuario").focus();

            $('#txtIDUsuarioHdd').val(IDUsuario);
            $('#txtUsuario').val(Usuario);
         //   $('#txtRutUsuario').val(Rut);
         //   $('#txtDvUsuario').val(Dv);
            $('#txtNombre').val(Nombre);
            $('#txtApePat').val(ApePaterno);
            $('#txtApeMat').val(ApeMaterno);
            $('#txtMail').val(Email);
            $('#txtPassw').val(Clave);
            $('#txtRePassw').val(Clave);
            
            $("#cboVigente").val(Vigente).change();
            $("#cboPerfil").val(IDGrupo).change();
            $('#modal_Usuario').modal();
        }

        function GetGrupo() {

            $.ajax({
                type: "POST",
                url: "frmMantUsuario.aspx/GetGrupo",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccessGetGrupo,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }


        function onSuccessGetGrupo(response) {
            datos = JSON.parse(response.d);
            $(datos).each(function () {

                var option = $(document.createElement('option'));
                option.text(this.Nombre);
                option.val(this.IDGrupo);
                $("#cboPerfil").append(option);
            });
        }




        function ClearValues() {


            $('#txtIDUsuarioHdd').val('');

            $('#txtUsuario').val('');
            $('#txtUsuario').attr("class", "form-control");

         //   $('#txtRutUsuario').val('');
         //   $('#txtRutUsuario').attr("class", "form-control");

         //   $('#txtDvUsuario').val('');

            $('#txtNombre').val('');
            $('#txtNombre').attr("class", "form-control");

            $('#txtApePat').val('');
            $('#txtApePat').attr("class", "form-control");

            $('#txtApeMat').val('');
            $('#txtApeMat').attr("class", "form-control");

            $('#txtMail').val('');
            $('#txtMail').attr("class", "form-control");

            $('#txtPassw').val('');
            $('#txtPassw').attr("class", "form-control");

            $('#txtRePassw').val('');
            $('#txtRePassw').attr("class", "form-control");

            //$('#cboPerfil').val(0);
            //$('#cboVigente').val(0);

            $('#txtNombre').val('');

            $("#txtUsuario").prop("disabled", false);

        }


        function onSuccessGetListAllUsuario(response) {
            datos = JSON.parse(response.d);

            var createXLSLFormatObj = [];

            var xlsHeader = ["IDUsuario", "Usuario", "Rut","Digito","Nombre", "ApePaterno", "ApeMaterno", "Email", "Vigente", "IDGrupo", "Perfil"];
            //var xlsHeader = ["IDUsuario", "Usuario", "Rut", "Dv", "Nombre", "ApePaterno", "ApeMaterno", "Email", "Vigente", "IDGrupo", "Perfil"];

            var xlsRows = datos;

            createXLSLFormatObj.push(xlsHeader);

            $.each(xlsRows, function (index, value) {
                var innerRowData = [];
                // $("tbody").append('<tr><td>' + value.IDUsuario + '</td><td>' + value.Usuario + '</td></tr>');
                $.each(value, function (ind, val) {

                    if (ind != "Clave") {
                        innerRowData.push(val);
                    }
                });
                createXLSLFormatObj.push(innerRowData);
            });

            /* File Name */
            var filename = "ListadoUsuario.xlsx";

            /* Sheet Name */
            var ws_name = "Usuarios";

            if (typeof console !== 'undefined') console.log(new Date());
            var wb = XLSX.utils.book_new(),
                ws = XLSX.utils.aoa_to_sheet(createXLSLFormatObj);

            /* Add worksheet to workbook */
            XLSX.utils.book_append_sheet(wb, ws, ws_name);

            /* Write workbook and Download */
            if (typeof console !== 'undefined') console.log(new Date());
            XLSX.writeFile(wb, filename);
            if (typeof console !== 'undefined') console.log(new Date());

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="content content--full">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Mantenedor de Usuarios</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-body text-center">
                             
                                <button type="button" class="btn btn-default" aria-label="Left Align" data-toggle="tooltip" href="#menu1" title="Agregar Usuario" onclick="agregarUsuatio();">
                                        <img src="../Img/btnAgregar3.jpg" style="border:none;" width="" height=""/>
                                </button>
                                <div class="modal modal-header-success" role="dialog" tabindex="-1" id="hdd">
                                    
                                    <input id="txtIDUsuarioHdd" value="" type="text" />
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div>
                                            <div class="container">
                                                <table id="tbl_Data"
                                                    <%--class="table table-bordered table-hover col-xs-12"--%>
                                                    class="table table-hover"
                                                    <%--data-click-to-select="true"--%>
                                                    data-toolbar="#toolbar"
                                                    data-search="true"
                                                    data-show-toggle="false"
                                                    data-show-columns="true"
                                                    data-show-export="true"
                                                    data-pagination="true"
                                                    <%--data-id-field="Id"--%>
                                                    data-page-list="[10, 25, 50, 100, ALL]"
                                                    data-show-footer="false">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="modal modal-header-success" role="dialog" tabindex="-1" id="modal_Usuario">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h6 class="modal-title text-white">Mantenedor Usuario</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: white;">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                    <div class="panel-group">
                                    <div class="panel panel-default">
                                        <div id="collapse1" class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                         <div class=" col-md-6 form-group">
                                                                <label for="exampleFormControlSelect1">Usuario</label>
                                                                <input id="txtUsuario" class="form-control input-sm"  type="text" tabindex='1'   maxlength="15" required="required"/>
                                                          </div>

                                                          <div class=" col-md-6 form-group">
                                                                <label for="exampleFormControlSelect1">Contraseña</label>
                                                                <input id="txtPassw" class="form-control input-sm"  type="password" tabindex='6'   maxlength="15"/>
                                                          </div>
                                                    </div>

                                                    <div class="row">
                                                         <div class=" col-md-6 form-group">
                                                                <label for="exampleFormControlSelect1">Nombre</label>
                                                                <input id="txtNombre" class="form-control input-sm"  type="text" tabindex='2'   maxlength="30" title="Nombre" pattern="[a-zA-'-]{2,30}"  required="required"/>
                                                          </div>

                                                          <div class=" col-md-6 form-group">
                                                                <label for="exampleFormControlSelect1">Repetir Contraseña</label>
                                                                <input id="txtRePassw" class="form-control input-sm"  type="password" tabindex='7'   maxlength="15"/>
                                                          </div>
                                                    </div>


                                                    <div class="row">

                                                        <div class=" col-md-6 form-group">
                                                                <label for="exampleFormControlSelect1">Apellido Paterno</label>
                                                                <input id="txtApePat" class="form-control input-sm"  type="text" tabindex='3'   maxlength="20" title="Apellido Paterno" pattern="[a-zA-'-]{2,20}"  required="required"/>
                                                          </div>

                                                        <div class=" col-md-6 form-group">
                                                              <label for="exampleFormControlSelect1">Perfil</label>
                                                                <select class="form-control input-sm" id="cboPerfil" tabindex="8"></select>
                                                          </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class=" col-md-6 form-group">
                                                             <label for="exampleFormControlSelect1">Apellido Materno</label>
                                                             <input id="txtApeMat" class="form-control input-sm"  type="text" tabindex='4'   maxlength="20" title="Apellido Materno" pattern="[a-zA-'-]{2,20}"  required="required"/>
                                                         </div>

                                                         <div class=" col-md-6 form-group">
                                                              <label for="exampleFormControlSelect1">Vigente</label>
                                                                <select class="form-control input-sm" id="cboVigente" tabindex="9">
                                                                    <option value="SI">SI</option>
                                                                    <option value="NO">NO</option>
                                                                </select>
                                                          </div>

                                                    </div>

                                                    <div class="row">
                                                        <div class=" col-md-6 form-group">
                                                             <label for="exampleFormControlSelect1">Correo</label>
                                                             <input id="txtMail" class="form-control input-sm"  type="text" tabindex='5'   maxlength="60"/>
                                                         </div>

                                                    </div>


                                                    <br />
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                        </div>
                                                        <div class="col-md-3">
                                                            <button class="btn btn-primary" onclick="" type="button" id="btnLimpiar">Limpiar</button>
                                                        </div>
                                                         <div class="col-md-3">
                                                            <button class="btn btn-primary" onclick="" type="button" id="btnExportar">Exportar</button>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <button class="btn btn-primary" onclick="" type="button" id="btnGrabar">Grabar</button>
                                                        </div>
                                                        <div class="col-md-1">
                                                        </div>
                                                    </div>
                                                    <br />

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-header-success" role="dialog" tabindex="-1" id="CamposOcualtos">
     <asp:TextBox ID="txtIDPerfil" runat="server"></asp:TextBox>
     <asp:TextBox ID="txtIDUsuario" runat="server" ></asp:TextBox>
</div>           

<script>
    //Activo
    document.getElementById('navbarDropdown').style.backgroundColor = "#128ff2";
    document.getElementById('navbarDropdown').style.borderColor = "#128ff2";
    //Normal
    document.getElementById('rptBotonera_primero_0').style.backgroundColor = "#00438c";
    document.getElementById('rptBotonera_primero_0').style.borderColor = "#356396";

    document.getElementById('rptBotonera_primero_1').style.backgroundColor = "#00438c";
    document.getElementById('rptBotonera_primero_1').style.borderColor = "#356396";

    document.getElementById('rptBotonera_primero_2').style.backgroundColor = "#00438c";
    document.getElementById('rptBotonera_primero_2').style.borderColor = "#356396";

    document.getElementById('rptBotonera_primero_3').style.backgroundColor = "#00438c";
    document.getElementById('rptBotonera_primero_3').style.borderColor = "#356396";

    </script>
</asp:Content>

