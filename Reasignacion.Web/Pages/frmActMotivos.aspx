﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Pages/Sitio.Master" CodeBehind="frmActMotivos.aspx.vb" Inherits="Reasignacion.Web.frmActMotivos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $(document).ready(function () {

            GetMotivosReasig();


            $("#btnLimpiar").click(function () {
                ClearValues();
                GetMotivosReasig();
            });

            $("#btnGrabar").click(function () {

                var $IDMotivoReasig = $('#txtIDMotivoReasigHidden').val();
                var $MotivoReasig = $('#txtMotivoReasig').val();

                if ($MotivoReasig == "") {
                    showError('El Motivo de reasignacion no puede estar en blanco.');
                }
                else {
                    if ($IDMotivoReasig == "") {

                        $.ajax({
                            type: "POST",
                            url: "frmActMotivos.aspx/InsertMotivosReasig",
                            data: '{MotivoReasig: "' + $MotivoReasig + '" }',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: onSuccessInsertMotivosReasig,
                            failure: function (response) {
                                alert(response.d);
                            }
                        });
                    }
                    else {
                        $.ajax({
                            type: "POST",
                            url: "frmActMotivos.aspx/UpdateMotivosReasig",
                            data: '{IDMotivoReasignacion: "' + $IDMotivoReasig + '", MotivoReasig: "' + $MotivoReasig + '" }',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: onSuccessUpdateMotivosReasig,
                            failure: function (response) {
                                alert(response.d);
                            }
                        });

                    }
                }

            });
        });

        function onSuccessInsertMotivosReasig(response) {
            datos = JSON.parse(response.d);
            $(datos).each(function () {
                if (this.Respuesta == 'OK') {
                    showSuccess('Registro Ingresado correctamente');
                    ClearValues();
                    GetMotivosReasig();
                    $("#modal_motivo").modal('hide');
                }
                else if (this.Respuesta == 'Encontro') {
                    showSuccess('Ya Existe');
                }
                else {
                    showError('ERROR al ingresar la información');
                    return;
                }
            });
//            showSuccess('Motivo de reasignación ingresado satisfactoriamente.');

        }

        function onSuccessUpdateMotivosReasig(response) {
            showSuccess('Motivo de reasignación actualizado satisfactoriamente.');
            ClearValues();
            GetMotivosReasig();
            $("#modal_motivo").modal('hide');
        }

        function ClearValues() {

            $('#txtMotivoReasig').val('');
            $('#txtIDMotivoReasigHidden').val('');
        }



        function GetMotivosReasig() {

            $.ajax({
                type: "POST",
                url: "frmActMotivos.aspx/GetMotivosReasig",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccessGetMotivosReasig,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function onSuccessGetMotivosReasig(response) {
            datos = JSON.parse(response.d);
            $('#tbl_Data').bootstrapTable('destroy')
            $('#tbl_Data').bootstrapTable({
                columns: [
                    { field: 'IDMotivoReasignacion', title: 'IDMotivoReasignacion', visible: false, sortable: true }
                    , { field: 'MotivoReasignacion', align: 'left', title: 'Motivo de reasignación', sortable: true }
                    //  , { field: 'Vigente', align: 'center', title: 'Estado', sortable: true }
                    , { title: '', align: 'center', formatter: col_Editar, sortable: false }
                ]
                , data: datos
            });
        }

        function col_Editar(value, row, index) {
            return [

                '<button type="button" class="btn btn-default" aria-label="Left Align" data-toggle="tooltip" href="#menu1" title="Editar" onclick="Editar(\'' + row.IDMotivoReasignacion + '\', \'' + row.MotivoReasignacion + '\');">'
                , '<span class="fas fa-edit fa-lg" aria-hidden="true"></span>'
                , '</button>'
                , '<button type="button" class="btn btn-default" aria-label="Left Align" data-toggle="tooltip" title="Eliminar" onclick="Eliminar(\'' + row.IDMotivoReasignacion + '\');">'
                , '<span class="fas fa-trash fa-lg" aria-hidden="true"></span>'
                , '</button>'

            ].join('');
        }

        function Editar(IDMotivoReasignacion, MotivoReasignacion) {
            $('#txtMotivoReasig').val(MotivoReasignacion);
            $('#txtIDMotivoReasigHidden').val(IDMotivoReasignacion);
            $('#modal_motivo').modal();
            //Get_Motivo(_CIA);
        }

        function Eliminar(IDMotivoReasignacion) {
            $.ajax({
                type: "POST",
                url: "frmActMotivos.aspx/DelMotivo",
                data: '{IDMotivoReasignacion: "' + IDMotivoReasignacion + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccess_Eliminar,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function onSuccess_Eliminar(response) {
            showSuccess('Motivo eliminado Satisfactoriamente');
            ClearValues();
            GetMotivosReasig();
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="content content--full">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Motivos Reasignación</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">

                        <div class="panel panel-primary">
                            <div class="panel-body text-center">


                                <button type="button" class="btn btn-default" aria-label="Left Align" data-toggle="tooltip" href="#menu1" title="Agregar Motivo" onclick="Editar('', '');">
                                        <img src="../Img/btnAgregar3.jpg" style="border:none;" width="" height=""/>
                                </button>

                                <div class="modal modal-header-success" role="dialog" tabindex="-1" id="CamposOcultos">
                                    <input id="txtIDMotivoReasigHidden" value="" type="text" />
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div>
                                            <div class="container">
                                                <table id="tbl_Data"
                                                    <%--class="table table-bordered table-hover col-xs-12"--%>
                                                    class="table table-hover"
                                                    <%--data-click-to-select="true"--%>
                                                    data-toolbar="#toolbar"
                                                    data-search="true"
                                                    data-show-toggle="false"
                                                    data-show-columns="false"
                                                    data-show-export="false"
                                                    data-pagination="true"
                                                    <%--data-id-field="Id"--%>
                                                    data-page-list="[10, 25, 50, 100, ALL]"
                                                    data-show-footer="false">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-header-success" role="dialog" tabindex="-1" id="modal_motivo">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h6 class="modal-title text-white">Motivos Reasignación</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: white;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="panel-group">
                        <div class="panel panel-default">
                            <div id="collapse1" class="panel-body">
                                <div class="row text-center">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class=" col-md-6 form-group">
                                                 <label for="exampleFormControlSelect1">Motivo de Reasignación</label>
                                                 <input id="txtMotivoReasig" class="form-control input-sm"  type="text"   maxlength="60"  required="required"/>
                                             </div>
                                            <div class=" col-md-3"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-2">
                                                 <button class="btn btn-primary form-control" onclick="" type="button" id="btnLimpiar">Limpiar</button>
                                            </div>
                                            <div class="col-md-2">
                                                 <button class="btn btn-primary form-control" onclick="" type="button" id="btnGrabar">Grabar</button>
                                            </div>
                                            <div class="col-md-4"></div>
                                         </div>
                                    </div>
                                </div>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-header-success" role="dialog" tabindex="-1" id="CamposOcualtos">
        <asp:TextBox ID="txtIDPerfil" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtIDUsuario" runat="server" ></asp:TextBox>
    </div>
        <script>
        //Activo
        document.getElementById('navbarDropdown').style.backgroundColor = "#128ff2";
        document.getElementById('navbarDropdown').style.borderColor = "#128ff2";
        //Normal
        document.getElementById('rptBotonera_primero_0').style.backgroundColor = "#00438c";
        document.getElementById('rptBotonera_primero_0').style.borderColor = "#356396";

        document.getElementById('rptBotonera_primero_1').style.backgroundColor = "#00438c";
        document.getElementById('rptBotonera_primero_1').style.borderColor = "#356396";

        document.getElementById('rptBotonera_primero_2').style.backgroundColor = "#00438c";
        document.getElementById('rptBotonera_primero_2').style.borderColor = "#356396";

        document.getElementById('rptBotonera_primero_3').style.backgroundColor = "#00438c";
        document.getElementById('rptBotonera_primero_3').style.borderColor = "#356396";

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
</asp:Content>
