﻿<%@ Page Language="vb" AutoEventWireup="false"  MasterPageFile="~/Pages/Sitio.Master"  CodeBehind="frmListadoEstadoAprobacion.aspx.vb" Inherits="Reasignacion.Web.frmListadoEstadoAprobacion" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

   <script type="text/javascript">
       $(document).ready(function () {
           Get_Datos();

       });




       function Get_Datos() {


           $.ajax({
               type: "POST",
               url: "frmListadoEstadoAprobacion.aspx/Get_Datos",
               data: '{}',
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               success: onSuccess_Reasignaciones,
               failure: function (response) {
                   alert(response.d);
               }
           });



       }

       function onSuccess_Reasignaciones(response) {

           lista = JSON.parse(response.d);

           $('#tbl_Data').bootstrapTable('destroy')
           $('#tbl_Data').bootstrapTable({
               columns: [
                 { field: 'IDEstadoAprobacion', title: 'ID Estado Aprobación', visible: true, sortable: true }
               , { field: 'Nombre', title: 'Nombre Estado Aprobación', visible: true, sortable: true }


               ]
               , data: lista
           });
       }





    </script>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="content content--full">
    <div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">listado Estados Aprobación</h3></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-primary">
                    <div class="panel-body">

                         <div class="row">
                            <div class="col-md-12">

                <div class="container">
                    <table id="tbl_Data"
                        <%--class="table table-bordered table-hover col-xs-12"--%>
                        class="table table-hover"
                        <%--data-click-to-select="true"--%>
                        data-toolbar="#toolbar"
                        data-search="true"
                        data-show-toggle="false"
                        data-show-columns="true"
                        data-show-export="true"
                        data-pagination="true"
                        <%--data-id-field="Id"--%>
                        data-page-list="[10, 25, 50, 100, ALL]"
                        data-show-footer="false">
                    </table>
                </div>

                            </div>
                        </div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal modal-header-success" role="dialog" tabindex="-1" id="CamposOcualtos">
        <asp:TextBox ID="txtIDPerfil" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtIDUsuario" runat="server" ></asp:TextBox>
    </div>    

<script>
    //Activo
    document.getElementById('navbarDropdown').style.backgroundColor = "#128ff2";
    document.getElementById('navbarDropdown').style.borderColor = "#128ff2";
    //Normal
    document.getElementById('rptBotonera_primero_0').style.backgroundColor = "#00438c";
    document.getElementById('rptBotonera_primero_0').style.borderColor = "#356396";

    document.getElementById('rptBotonera_primero_1').style.backgroundColor = "#00438c";
    document.getElementById('rptBotonera_primero_1').style.borderColor = "#356396";

    document.getElementById('rptBotonera_primero_2').style.backgroundColor = "#00438c";
    document.getElementById('rptBotonera_primero_2').style.borderColor = "#356396";

    document.getElementById('rptBotonera_primero_3').style.backgroundColor = "#00438c";
    document.getElementById('rptBotonera_primero_3').style.borderColor = "#356396";

    </script>

</asp:Content>

