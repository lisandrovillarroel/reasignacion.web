﻿<%@ Page Language="vb" AutoEventWireup="false"  MasterPageFile="~/Pages/Sitio.Master" CodeBehind="frmCambioClave.aspx.vb" Inherits="Reasignacion.Web.frmCambioClave" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

   <script type="text/javascript">
       $(document).ready(function () {

           $("#btnGrabar").click(function () {

               $txtClaveActual = $('#txtClaveActual').val();
               $txtClaveNueva = $('#txtClaveNueva').val();
               $txtRepiteClaveNueva = $('#txtRepiteClaveNueva').val();

               if ($txtClaveNueva.length < 8) {
                   showError('La contraseña debe ser mayor o igual a 8 caracteres.');
               } else {
                   if ($txtClaveNueva != $txtRepiteClaveNueva) {
                       showError('Las contraseñas no coinciden.');
                   } else {
                       $.ajax({
                           type: "POST",
                           async: false,
                           url: "frmCambioClave.aspx/UpdatePass",
                           data: '{ ClaveNueva: "' + $txtClaveNueva + '" , ClaveAct: "' + $txtClaveActual + '"}',
                           contentType: "application/json; charset=utf-8",
                           dataType: "json",
                           success: onSuccessUpdatePass,
                           failure: function (response) {
                               alert(response.d);
                           }
                       });
                   }
               }
           });
       });

       function onSuccessUpdatePass(response) {
           //console.log(response.d);
           datos = JSON.parse(response.d);
           $(datos).each(function () {
               if (this.estado == "OK") {
                   fnLimpiarDatos();
                   showSuccess('Cambio de contraseña Exitoso.');
               }
               else if (this.estado == "NOT_OK") {
                   showError('Contraseña Actual incorrecta');
               }
           });

       }


       function fnLimpiarDatos() {

           $('#txtClaveActual').val('');
           $('#txtClaveNueva').val('');
           $('#txtRepiteClaveNueva').val('');

       }

    </script>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="content content--full">
    <div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Cambio de Clave</h3></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-primary">
                    <div class="panel-body">

                         <div class="row">
                            <div class="col-md-12">
                                   <div class="row separacion-rows">

                                        <div class="col-md-2">
                                            <label>Contraseña Actual</label>
                                        </div>
                                        <div class="col-md-3">
                                            <input id="txtClaveActual" class="form-control input-sm"  type="password"  maxlength="15"/>
                                        </div>

                                    </div>
                                   <div class="row separacion-rows">

                                        <div class="col-md-2">
                                            <label>Nueva Contraseña</label>
                                        </div>
                                        <div class="col-md-3">
                                            <input id="txtClaveNueva" class="form-control input-sm"  type="password"  maxlength="15"/>
                                        </div>
                                    </div>
                                   <div class="row separacion-rows">
                                        <div class="col-md-2">
                                            <label>Repita nueva Contraseña</label>
                                        </div>
                                        <div class="col-md-3">
                                            <input id="txtRepiteClaveNueva" class="form-control input-sm"  type="password"  maxlength="15"/>
                                        </div>
                                    </div>                                   
                                    <div class="row">
                                        <div class="col-md-10">
                                 
                                      </div>
                                        <div class="col-md-2">
                                            <%--<asp:Button ID="pbBuscar" class="btn btn-primary form-conrol"  OnClientClick="LlenarDatos();return false;"  runat="server" Text="Buscar" />--%>
                                            <button class="btn btn-primary form-control"  type="button" id="btnGrabar">Grabar</button>
                                        </div>
                                    </div>
                            </div>
                        </div>




<%--<div class="modal modal-header-success" role="dialog" tabindex="-1" id="CamposOcualtos">
    <asp:TextBox ID="txtNroSolicitudBCO" runat="server" ></asp:TextBox>
    <asp:TextBox ID="txtUsuario_Session" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtIDPerfil" runat="server"></asp:TextBox>
    <input id="txtIDDevolucion" class="form-control input-sm"  type="text" value="0" />
    <input id="txtClaveActual_DB" class="form-control input-sm"  type="text" value="" />
</div>--%>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
    <div class="modal modal-header-success" role="dialog" tabindex="-1" id="CamposOcualtos">
        <asp:TextBox ID="txtIDPerfil" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtIDUsuario" runat="server" ></asp:TextBox>
    </div>

</asp:Content>

