﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class frmIngresoSolicitud

    '''<summary>
    '''Control txtComentario.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtComentario As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtIDUsuario.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtIDUsuario As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtIDPerfil.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtIDPerfil As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtIP_Actual.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtIP_Actual As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtSessionID_Actual.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtSessionID_Actual As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtIP_Session.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtIP_Session As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtSessionID_Session.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtSessionID_Session As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtUsuario_Session.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtUsuario_Session As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtNroSeguro_Doc.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtNroSeguro_Doc As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtExisteEnBVA_SN.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtExisteEnBVA_SN As Global.System.Web.UI.WebControls.TextBox
End Class
