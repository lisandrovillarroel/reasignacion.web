﻿Imports Newtonsoft.Json
Imports System.IO
Public Class frmHistoricos
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtIDUsuario.Text = Session("IDUsuario")
            txtIDPerfil.Text = Session("IDGrupo")
            If txtIDUsuario.Text = "" Then
                Response.Redirect("frmLogin.aspx")
            End If
        End If
    End Sub


    Public Shared Function ToJSON(pTable As DataTable) As String
        Dim vCadenaJSON As String = ""
        vCadenaJSON = JsonConvert.SerializeObject(pTable)
        If vCadenaJSON.Length <= 5 Then
            vCadenaJSON = "[{""NRO_SEGURO"":""-""}]"
        End If
        Return vCadenaJSON
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function Get_Datos(_IDReasignacion As String, _IDEstadoAprobacion As String, _IDTipoSolicitud As String, _IDEstadoGestion As String, _FechaDesde As String, _FechaHasta As String, _IDSupervisor As String, _RutEjecutivo As String, _NroSeguro As String, _IDUsuario As String, _IDCoordinador As String, _IDGerente As String, _enProceso As String) As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._IDReasignacion = _IDReasignacion
            Exec._IDEstadoAprobacion = IIf(_IDEstadoAprobacion = "null", 0, _IDEstadoAprobacion)
            Exec._IDTipoSolicitud = IIf(_IDTipoSolicitud = "null", 0, _IDTipoSolicitud)
            Exec._IDEstadoGestion = IIf(_IDEstadoGestion = "null", 0, _IDEstadoGestion)
            Exec._FechaDesde = _FechaDesde
            Exec._FechaHasta = _FechaHasta
            Exec._IDSupervisor = IIf(_IDSupervisor = "null", 0, _IDSupervisor)
            Exec._RutEjecutivo = _RutEjecutivo
            Exec._NroSeguro = _NroSeguro
            Exec._IDUsuario = _IDUsuario
            Exec._IDCoordinador = IIf(_IDCoordinador = "null", 0, _IDCoordinador)
            Exec._IDGerente = IIf(_IDGerente = "null", 0, _IDGerente)
            Exec._enProceso = IIf(_enProceso = "T", "", _enProceso)
            Exec.Get_ReasignacionHistorico(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                'MsgBox(rp.Ds.Tables(0).Rows(0).Item(0))
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    <System.Web.Services.WebMethod()>
    Public Shared Function Get_Excel(_IDReasignacion As String, _IDEstadoAprobacion As String, _IDTipoSolicitud As String, _IDEstadoGestion As String, _FechaDesde As String, _FechaHasta As String, _IDSupervisor As String, _RutEjecutivo As String, _NroSeguro As String, _IDUsuario As String, _IDCoordinador As String, _IDGerente As String) As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._IDReasignacion = _IDReasignacion
            Exec._IDEstadoAprobacion = IIf(_IDEstadoAprobacion = "null", 0, _IDEstadoAprobacion)
            Exec._IDTipoSolicitud = IIf(_IDTipoSolicitud = "null", 0, _IDTipoSolicitud)
            Exec._IDEstadoGestion = IIf(_IDEstadoGestion = "null", 0, _IDEstadoGestion)
            Exec._FechaDesde = _FechaDesde
            Exec._FechaHasta = _FechaHasta
            Exec._IDSupervisor = IIf(_IDSupervisor = "null", 0, _IDSupervisor)
            Exec._RutEjecutivo = _RutEjecutivo
            Exec._NroSeguro = _NroSeguro
            Exec._IDUsuario = _IDUsuario
            Exec._IDCoordinador = IIf(_IDCoordinador = "null", 0, _IDCoordinador)
            Exec._IDGerente = IIf(_IDGerente = "null", 0, _IDGerente)

            Exec.Get_ReasignacionHistoricaXLS(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                'MsgBox(rp.Ds.Tables(0).Rows(0).Item(0))
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function Get_TipoSolicitud() As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec.Get_TipoSolicitudTodas(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    <System.Web.Services.WebMethod()>
    Public Shared Function Get_EstadoAprovacion() As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec.Get_EstadoAprobacionTodos(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function Get_EstadoGestion() As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec.Get_EstadoGestionTodos(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function Get_SupervidorTodos() As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec.Get_SupervidorTodos(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function Get_ReasignacionResumenMisSolicitudes(_IDReasignacion As String, _IDUsuario As String) As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._IDReasignacion = _IDReasignacion
            Exec._IDUsuario = _IDUsuario

            Exec.Get_ReasignacionResumenMisSolicitudes(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function Get_Supervisor() As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec.Get_SupervisorMasBlanco(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    <System.Web.Services.WebMethod()>
    Public Shared Function Get_Gerente() As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec.Get_GerenteMasBlanco(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    <System.Web.Services.WebMethod()>
    Public Shared Function Upd_ReasignacionEstadoTipo(_IDReasignacion As String, _IDTipoSolicitud As String, _EstadoAprobacion As String, _IDSupervisor As String, _IDSupervisorOriginal As String, _IDGerente As String, _IDGerenteOriginal As String, _IDUsuario As String) As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._IDReasignacion = _IDReasignacion
            Exec._IDTipoSolicitud = _IDTipoSolicitud
            Exec._EstadoAprobacion = _EstadoAprobacion
            Exec._IDSupervisor = _IDSupervisor
            Exec._IDSupervisorOriginal = _IDSupervisorOriginal
            Exec._IDGerente = _IDGerente
            Exec._IDGerenteOriginal = _IDGerenteOriginal
            Exec._IDUsuario = _IDUsuario


            Exec.Upd_ReasignacionEstadoTipo(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function Get_Download(_IDReasignacion As String, _NroSeguro As String) As String
        Try
            Dim szPATH As String
            szPATH = ConfigurationManager.AppSettings("Path_FileDownload")

            Dim szNombreArchivo As String
            Dim szBase64 As String

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dataTable As DataTable

            ' Creamos el objeto DataTable
            Dim dt As DataTable = New DataTable("Datos")
            Dim dc_Archivo = New DataColumn("Archivo", Type.GetType("System.String"))

            ' Añadimos la columna al objeto DataTable
            dt.Columns.Add(dc_Archivo)

            Exec._IDReasignacion = _IDReasignacion
            Exec._NroSeguro = _NroSeguro

            Exec.Get_Adjunto(rp)
            szNombreArchivo = "Error"
            If rp.TieneDatos Then
                DataTable = rp.Ds.Tables(0)
                For Each Fila As DataRow In dataTable.Rows
                    szNombreArchivo = Fila.Field(Of String)("NombreAdjunto")
                    szBase64 = Fila.Field(Of String)("Adjunto_Doc")

                    If (System.IO.File.Exists(szPATH & "DB_" & szNombreArchivo)) Then
                        System.IO.File.Delete(szPATH & "DB_" & szNombreArchivo)
                    End If

                    ' get your base64 string... b64str

                    Dim binaryData() As Byte = Convert.FromBase64String(szBase64)

                    ' Assuming it's a jpg :)
                    Dim fs As New FileStream(szPATH & "DB_" & szNombreArchivo, FileMode.CreateNew)

                    ' Elimina archivo existente con el mismo nombre

                    ' write it out
                    fs.Write(binaryData, 0, binaryData.Length)

                    ' close it down.
                    fs.Close()
                    Dim drG As DataRow = dt.NewRow
                    ' Le Asignamos un valor a la columna Precio
                    drG.Item("Archivo") = "DB_" & szNombreArchivo
                    ' Añadimos la fila al objeto DataTable
                    dt.Rows.Add(drG)
                    ''Fin Graba Tabla
                Next
            Else
                Dim drG As DataRow = dt.NewRow
                ' Le Asignamos un valor a la columna Precio
                drG.Item("Archivo") = "DB_" & szNombreArchivo
                ' Añadimos la fila al objeto DataTable
                dt.Rows.Add(drG)
                ''Fin Graba Tabla
            End If
            If rp.Errores Then
                'MsgBox1.ShowMessage(rp.MensajeError)
                'ClientScript.RegisterStartupScript(Me.GetType(), "AlertMessageBox", "alert('Carga Completada con Errores, favor Reportar');", True)
            Else

            End If

            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dt)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

End Class