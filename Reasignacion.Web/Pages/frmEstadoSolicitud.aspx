﻿<%@ Page Language="vb" AutoEventWireup="false"  MasterPageFile="~/Pages/Sitio.Master" CodeBehind="frmEstadoSolicitud.aspx.vb" Inherits="Reasignacion.Web.frmEstadoSolicitud" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

   <script type="text/javascript">




       $(document).ready(function () {

            $(document).ajaxStart($.blockUI({ message: '<h1><img src="../Img/loading.gif" /> Procesando...</h1>' })).ajaxStop($.unblockUI);


           Get_TipoSolicitud();
           Get_TipoSolicitudResumen();
           Get_EstadoAprobacion();
           Get_EstadoAprobacionResumen();
           Get_EstadoGestion();
           //Get_SupervidorTodos();
           Get_Supervisor();
           Get_Gerente();

           var _IDUsuario = $('#<%=txtIDUsuario.ClientID%>').val();

           f_DerivacionFirma(_IDUsuario) 

           var _IDPerfil = $('#<%=txtIDPerfil.ClientID%>').val();
           //$('#pbGrabar').attr("visible", false);
           
           


           $('#pbGrabar').attr("disabled", true);

           if (_IDPerfil == 1) {
               $('#cbTipoSolicitudResumen').attr("disabled", false);
               $('#cbEstadoAprobacionResumen').attr("disabled", false);
               //$('#pbGrabar').attr("visible", true);
               $('#pbGrabar').attr("disabled", false);
               Get_Gerente_Filtro();
               f_Supervisor_x_Gerente();
           }
           else {
               var option_Gerente = $(document.createElement('option'));
               option_Gerente.text('');
               option_Gerente.val('0');
               $("#cbGerenteFiltro").append(option_Gerente);
               $('#cbGerenteFiltro').attr("disabled", true);

               if (_IDPerfil == 4) {
                   $('#cbGerenteFiltro').empty();
                   var option_Gerente = $(document.createElement('option'));
                   option_Gerente.text('');
                   option_Gerente.val(_IDUsuario);
                   $("#cbGerenteFiltro").append(option_Gerente);
                   $('#cbGerenteFiltro').attr("disabled", true);
                   f_Supervisor_x_Gerente();
               }
               else if (_IDPerfil == 3) {
                   var option_Supervisor = $(document.createElement('option'));
                   option_Supervisor.text('');
                   option_Supervisor.val(_IDUsuario);
                   $("#cbSupervidor").append(option_Supervisor);
                   $('#cbSupervidor').attr("disabled", true);
                   f_Coordinador_x_Supervisor();
               }
               else if (_IDPerfil == 2) {
                   $('#cbSupervidor').empty();
                   var option_Supervisor = $(document.createElement('option'));
                   option_Supervisor.text('');
                   option_Supervisor.val('0');
                   $("#cbSupervidor").append(option_Supervisor);
                   $('#cbSupervidor').attr("disabled", true);

                   var option_Coordinador = $(document.createElement('option'));
                   option_Coordinador.text('');
                   option_Coordinador.val(_IDUsuario);
                   $("#cbCoordinador").append(option_Coordinador);
                   $('#cbCoordinador').attr("disabled", true);
               }
           }


           Get_Reasignaciones();

            $.unblockUI();

       });


        // ---------------------------------------------------------------------
        // Derivacion Firma
        // ---------------------------------------------------------------------
        function f_DerivacionFirma(_IDUsuario) {

            $.ajax({
                type: "POST",
                async: false,
                url: "frmIngresoSolicitud.aspx/Get_DerivacionFirma",
                data: '{_IDUsuario: "' + _IDUsuario + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccess_DerivacionFirma,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function onSuccess_DerivacionFirma(response) {
            datos = JSON.parse(response.d);
            $(datos).each(function () {
                $('#txtSupervisor').val(this.Supervisor);
                $('#txtSupervisorInternet').val(this.SupervisorInternet);
                $('#txtGerente').val(this.Gerente);
            });
        }



        // ---------------------------------------------------------------------
        // Obtiene Reasignaciones
        // ---------------------------------------------------------------------

       function Get_Reasignaciones() {
           var _IDUsuario = $('#<%=txtIDUsuario.ClientID%>').val();
           var _IDReasignacion = $("#txtNroSolicitud").val();
           var _IDEstadoAprobacion = $("#cbEstadoAprobacion").val();
           var _IDTipoSolicitud = $("#cbTipoSolicitud").val();
           var _IDEstadoGestion = $("#cbEstadoGestion").val();
           var _FechaDesde = $("#txtFechaSolicitudDesde").val();
           var _FechaHasta = $("#txtFechaSolicitudHasta").val();
           var _IDSupervisor = $("#cbSupervidor").val();
           var _RutEjecutivo = $("#txtRutEjecutivo").val();
           var _NroSeguro = $("#txtNroSeguro").val();
           var _IDCoordinador = $("#cbCoordinador").val();
           var _IDGerente = $("#cbGerenteFiltro").val();
           var _enProceso = $("#cbEnProceso").val();

           if (_IDReasignacion == '') {
               _IDReasignacion = 0;
           }
           if (_RutEjecutivo == '') {
               _RutEjecutivo = 0;
           }
           
           $.ajax({
               type: "POST",
               async: false,
               url: "frmEstadoSolicitud.aspx/Get_Datos",
               data: '{_IDReasignacion: "' + _IDReasignacion + '", _IDEstadoAprobacion: "' + _IDEstadoAprobacion + '", _IDTipoSolicitud: "' + _IDTipoSolicitud + '", _IDEstadoGestion: "' + _IDEstadoGestion + '", _FechaDesde: "' + _FechaDesde + '", _FechaHasta: "' + _FechaHasta + '", _IDSupervisor: "' + _IDSupervisor + '", _RutEjecutivo: "' + _RutEjecutivo + '", _NroSeguro: "' + _NroSeguro + '", _IDUsuario: "' + _IDUsuario + '", _IDCoordinador: "' + _IDCoordinador  + '", _IDGerente: "' + _IDGerente + '", _enProceso: "' + _enProceso + '" }',
               //data: '{}',
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               success: onSuccess_Reasignaciones,
               failure: function (response) {
                   alert(response.d);
               }
           });
       }

       function onSuccess_Reasignaciones(response) {

           lista = JSON.parse(response.d);

           $('#tbl_Data').bootstrapTable('destroy')


           var _IDPerfil = $('#<%=txtIDPerfil.ClientID%>').val();

           if (_IDPerfil == 1 || _IDPerfil == 4) {
               $('#tbl_Data').bootstrapTable({
                   columns: [
                       { align: 'center', title: 'Ver', formatter: Col_Ver }
                       , { field: 'IDReasignacion', title: 'Nro', visible: true, sortable: true }
                       , { field: 'NombreTipoSolicitud', title: 'Tipo Solicitud', visible: true, sortable: true }
                       , { field: 'FechaIngreso', type: Date, title: 'Fecha Solicitud', visible: true, sortable: true, sorter: DateSorter, formatter: DateFormat }
                       , { field: 'CoordinadorSolicitante', title: 'Solicitante', visible: true, sortable: true }
                       , { field: 'Supervisor', title: 'Aprobador', visible: true, sortable: true }
                       , { field: 'EstadoAprobacion', title: 'Estado Solicitud', visible: true, sortable: true }
                       , { field: 'EstadoGestion', title: 'Estado Gestion', visible: true, sortable: true }
                       , { field: 'RutNuevoEjecutivo', title: 'Rut Eje. nuevo', visible: true, sortable: true }
                       , { field: 'NroSeguro', title: 'Nro Seguro', visible: true, sortable: true }
                       , { field: 'NroSeguroOtro', title: 'Nuevo Folio', visible: false, sortable: true }

                       , { field: 'RutEjecutivo', title: 'Rut Ej.Actual', visible: false, sortable: true }
                       , { field: 'MarcaReferido', title: 'Marca Referido', visible: false, sortable: true }
                       , { field: 'MotivoReasignacion', title: 'Motivo Reasignación', visible: false, sortable: true }
                       , { field: 'NombreEjecutivoReasignacion', title: 'Nombre Nvo.Eje.', visible: false, sortable: true }
                       , { field: 'Zona', title: 'Zona', visible: false, sortable: true }
                       , { field: 'Observacion', title: 'Observación', visible: false, sortable: true }
                       , { field: 'FechaGestion', title: 'Fec.Gestión', visible: false, sortable: true }
                       , { field: 'MesAnnoContable', title: 'Mes-Año Contable', visible: false, sortable: true }
                       , { field: 'Sucursal', title: 'Sucursal', visible: false, sortable: true }
                       , { field: 'CoordinadorSolicitante', title: 'Solicitante', visible: false, sortable: true }
                       , { field: 'MotivoRechazo', title: 'Motivo Rechazo', visible: false, sortable: true }
                       , { field: 'FechaEstado', title: 'Fec.Aprobación', visible: false, sortable: true }
                       , { field: 'EstadoAprobacion', title: 'Estado Aprobación', visible: false, sortable: true }
                       , { field: 'EstadoAprobacion', title: 'Estado Aprobación', visible: false, sortable: true }
                       , { field: 'EstadoAprobacion', title: 'Estado Aprobación', visible: false, sortable: true }
                       , { field: 'Gerente', title: 'Gerente', visible: false, sortable: true }
                       , { field: 'NroSeguroOtro', title: 'Nro.Seg.Reasignado', visible: false, sortable: true }
                       , { field: 'NroSeguro', title: 'Nro.Seguro', visible: false, sortable: true }
                       , { field: 'EnProceso', title: 'En Proceso', visible: false, sortable: true }
                   ]
                   , data: lista
               });
           }

           if (_IDPerfil == 2 ) {
               $('#tbl_Data').bootstrapTable({
                   columns: [
                       { align: 'center', title: 'Ver', formatter: Col_Ver }
                       , { field: 'IDReasignacion', title: 'Nro', visible: true, sortable: true }
                       , { field: 'NombreTipoSolicitud', title: 'Tipo Solicitud', visible: true, sortable: true }
                       , { field: 'FechaIngreso', type: Date, title: 'Fecha Solicitud', visible: true, sortable: true, sorter: DateSorter, formatter: DateFormat }
                       //, { field: 'CoordinadorSolicitante', title: 'Coordinador', visible: true, sortable: true }
                       , { field: 'Supervisor', title: 'Aprobador', visible: true, sortable: true }
                       , { field: 'EstadoAprobacion', title: 'Estado Solicitud', visible: true, sortable: true }
                       , { field: 'EstadoGestion', title: 'Estado Gestion', visible: true, sortable: true }
                       , { field: 'RutNuevoEjecutivo', title: 'Rut Eje. nuevo', visible: true, sortable: true }
                       , { field: 'NroSeguro', title: 'Nro Seguro', visible: true, sortable: true }
                       , { field: 'NroSeguroOtro', title: 'Nuevo Folio', visible: false, sortable: true }
                       , { field: 'RutEjecutivo', title: 'Rut Ej.Actual', visible: false, sortable: true }
                       , { field: 'MarcaReferido', title: 'Marca Referido', visible: false, sortable: true }
                       , { field: 'MotivoReasignacion', title: 'Motivo Reasignación', visible: false, sortable: true }
                       , { field: 'NombreEjecutivoReasignacion', title: 'Nombre Nvo.Eje.', visible: false, sortable: true }
                       , { field: 'Zona', title: 'Zona', visible: false, sortable: true }
                       , { field: 'Observacion', title: 'Observación', visible: false, sortable: true }
                       , { field: 'FechaGestion', title: 'Fec.Gestión', visible: false, sortable: true }
                       , { field: 'MesAnnoContable', title: 'Mes-Año Contable', visible: false, sortable: true }
                       , { field: 'Sucursal', title: 'Sucursal', visible: false, sortable: true }
                       , { field: 'CoordinadorSolicitante', title: 'Solicitante', visible: false, sortable: true }
                       , { field: 'MotivoRechazo', title: 'Motivo Rechazo', visible: false, sortable: true }
                       , { field: 'FechaEstado', title: 'Fec.Aprobación', visible: false, sortable: true }
                       , { field: 'EstadoAprobacion', title: 'Estado Aprobación', visible: false, sortable: true }
                       , { field: 'EstadoAprobacion', title: 'Estado Aprobación', visible: false, sortable: true }
                       , { field: 'EstadoAprobacion', title: 'Estado Aprobación', visible: false, sortable: true }
                       , { field: 'Gerente', title: 'Gerente', visible: false, sortable: true }
                       , { field: 'NroSeguroOtro', title: 'Nro.Seg.Reasignado', visible: false, sortable: true }
                       , { field: 'NroSeguro', title: 'Nro.Seguro', visible: false, sortable: true }
                       , { field: 'EnProceso', title: 'En Proceso', visible: false, sortable: true }
                   ]
                   , data: lista
               });
           }

           if (_IDPerfil == 3) {
               $('#tbl_Data').bootstrapTable({
                   columns: [
                       { align: 'center', title: 'Ver', formatter: Col_Ver }
                       , { field: 'IDReasignacion', title: 'Nro', visible: true, sortable: true }
                       , { field: 'NombreTipoSolicitud', title: 'Tipo Solicitud', visible: true, sortable: true }
                       , { field: 'FechaIngreso', type: Date, title: 'Fecha Solicitud', visible: true, sortable: true, sorter: DateSorter, formatter: DateFormat }
                       , { field: 'CoordinadorSolicitante', title: 'Solicitante', visible: true, sortable: true }
                       //, { field: 'Supervisor', title: 'Aprobador', visible: true, sortable: true }
                       , { field: 'EstadoAprobacion', title: 'Estado Solicitud', visible: true, sortable: true }
                       , { field: 'EstadoGestion', title: 'Estado Gestion', visible: true, sortable: true }
                       , { field: 'RutNuevoEjecutivo', title: 'Rut Eje. nuevo', visible: true, sortable: true }
                       , { field: 'NroSeguro', title: 'Nro Seguro', visible: true, sortable: true }
                       , { field: 'NroSeguroOtro', title: 'Nuevo Folio', visible: false, sortable: true }
                       , { field: 'RutEjecutivo', title: 'Rut Ej.Actual', visible: false, sortable: true }
                       , { field: 'MarcaReferido', title: 'Marca Referido', visible: false, sortable: true }
                       , { field: 'MotivoReasignacion', title: 'Motivo Reasignación', visible: false, sortable: true }
                       , { field: 'NombreEjecutivoReasignacion', title: 'Nombre Nvo.Eje.', visible: false, sortable: true }
                       , { field: 'Zona', title: 'Zona', visible: false, sortable: true }
                       , { field: 'Observacion', title: 'Observación', visible: false, sortable: true }
                       , { field: 'FechaGestion', title: 'Fec.Gestión', visible: false, sortable: true }
                       , { field: 'MesAnnoContable', title: 'Mes-Año Contable', visible: false, sortable: true }
                       , { field: 'Sucursal', title: 'Sucursal', visible: false, sortable: true }
                       , { field: 'CoordinadorSolicitante', title: 'Solicitante', visible: false, sortable: true }
                       , { field: 'MotivoRechazo', title: 'Motivo Rechazo', visible: false, sortable: true }
                       , { field: 'FechaEstado', title: 'Fec.Aprobación', visible: false, sortable: true }
                       , { field: 'EstadoAprobacion', title: 'Estado Aprobación', visible: false, sortable: true }
                       , { field: 'EstadoAprobacion', title: 'Estado Aprobación', visible: false, sortable: true }
                       , { field: 'EstadoAprobacion', title: 'Estado Aprobación', visible: false, sortable: true }
                       , { field: 'Gerente', title: 'Gerente', visible: false, sortable: true }
                       , { field: 'NroSeguroOtro', title: 'Nro.Seg.Reasignado', visible: false, sortable: true }
                       , { field: 'NroSeguro', title: 'Nro.Seguro', visible: false, sortable: true }
                       , { field: 'EnProceso', title: 'En Proceso', visible: false, sortable: true }
                   ]
                   , data: lista
               });
           }

           function DateSorter(a, b) {
               var d1 = new Date(a);
               var d2 = new Date(b);

               //var a = new Date(moment(a).format('YYYY/MM/DD'));
               //var b = new Date(moment(b).format('YYYY/MM/DD'));
               if (d1 < d2) return -1;
               if (d1 > d2) return 1;
               return 0;
           }

           function DateFormat(value, row, index) {
               return moment(value).format('DD/MM/YYYY');
           }

       }


       function Col_Ver(value, row, index) {
           return [
               '<button type="button" class="btn btn-default" aria-label="Left Align" data-toggle="tooltip" href="#menu1" title="Ver Resumen Solicitud" onclick="f_VerResumenReasignacion(\'' + row.IDReasignacion + '\', \'' + row.NroSeguro + '\');">',
               '<span class="fas fa-eye" aria-hidden="true"></span>',
               '</button>'
           ].join('');
       }


       //function f_VerDetalle() {

       //    $('#modal_Resumen').modal();

       //}


       // ---------------------------------------------------------------------
       // Tipo de Solicitud
       // ---------------------------------------------------------------------

       function Get_TipoSolicitud() {
           $.ajax({
               type: "POST",
               url: "frmEstadoSolicitud.aspx/Get_TipoSolicitud",
               data: '{}',
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               success: onSuccess_TipoSolicitud,
               failure: function (response) {
                   alert(response.d);
                   //alert('Error');
               }
           });
       }

       function onSuccess_TipoSolicitud(response) {
           datos = JSON.parse(response.d);
           $(datos).each(function () {
               var option = $(document.createElement('option'));
               option.text(this.Nombre);
               option.val(this.IDTipoSolicitud);
               $("#cbTipoSolicitud").append(option);
           });
       }

       function Get_TipoSolicitudResumen() {
           $.ajax({
               type: "POST",
               url: "frmEstadoSolicitud.aspx/Get_TipoSolicitud",
               data: '{}',
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               success: onSuccess_TipoSolicitudResumen,
               failure: function (response) {
                   alert(response.d);
                   //alert('Error');
               }
           });
       }

       function onSuccess_TipoSolicitudResumen(response) {
           datos = JSON.parse(response.d);
           $(datos).each(function () {
               var option = $(document.createElement('option'));
               option.text(this.Nombre);
               option.val(this.IDTipoSolicitud);
               $("#cbTipoSolicitudResumen").append(option);
           });
       }



       // ---------------------------------------------------------------------
       // Etado Aprobacion
       // ---------------------------------------------------------------------

       function Get_EstadoAprobacion() {
           $.ajax({
               type: "POST",
               url: "frmEstadoSolicitud.aspx/Get_EstadoAprovacion",
               data: '{}',
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               success: onSuccess_EstadoAprobacion,
               failure: function (response) {
                   alert(response.d);
                   //alert('Error');
               }
           });
       }

       function onSuccess_EstadoAprobacion(response) {
           datos = JSON.parse(response.d);
           $(datos).each(function () {
               var option = $(document.createElement('option'));
               option.text(this.Nombre);
               option.val(this.IDEstadoAprobacion);
               $("#cbEstadoAprobacion").append(option);
               //var option2 = $(document.createElement('option2'));
               //option2.text(this.Nombre);
               //option2.val(this.IDEstadoAprobacion);

               //$("#cbEstadoAprobacionResumen").append(option2);

           });
       }

       function Get_EstadoAprobacionResumen() {
           $.ajax({
               type: "POST",
               url: "frmEstadoSolicitud.aspx/Get_EstadoAprovacion",
               data: '{}',
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               success: onSuccess_EstadoAprobacionResumen,
               failure: function (response) {
                   alert(response.d);
                   //alert('Error');
               }
           });
       }

       function onSuccess_EstadoAprobacionResumen(response) {
           datos = JSON.parse(response.d);
           $(datos).each(function () {
               var option = $(document.createElement('option'));
               option.text(this.Nombre);
               option.val(this.IDEstadoAprobacion);
               $("#cbEstadoAprobacionResumen").append(option);
           });
       }


       // ---------------------------------------------------------------------
       // Etado Gestion
       // ---------------------------------------------------------------------

       function Get_EstadoGestion() {
           $.ajax({
               type: "POST",
               url: "frmEstadoSolicitud.aspx/Get_EstadoGestion",
               data: '{}',
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               success: onSuccess_EstadoGestion,
               failure: function (response) {
                   alert(response.d);
                   //alert('Error');
               }
           });
       }

       function onSuccess_EstadoGestion(response) {
           datos = JSON.parse(response.d);
           $(datos).each(function () {
               var option = $(document.createElement('option'));
               option.text(this.Nombre);
               option.val(this.IDEstadoGestion);
               $("#cbEstadoGestion").append(option);
           });
       }


       // ---------------------------------------------------------------------
       // Supervisores
       // ---------------------------------------------------------------------

       function Get_Supervisor() {
           $.ajax({
               type: "POST",
               url: "frmEstadoSolicitud.aspx/Get_Supervisor",
               data: '{}',
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               success: onSuccess_Supervisor,
               failure: function (response) {
                   alert(response.d);
                   //alert('Error');
               }
           });
       }

       function onSuccess_Supervisor(response) {
           datos = JSON.parse(response.d);
           $(datos).each(function () {
               var option = $(document.createElement('option'));
               option.text(this.NombreSupervisor);
               option.val(this.IDUsuario);
               $("#cbSupervisor").append(option);
           });
       }



       // ---------------------------------------------------------------------
       // Gerentes
       // ---------------------------------------------------------------------
       function Get_Gerente() {
           $.ajax({
               type: "POST",
               url: "frmEstadoSolicitud.aspx/Get_Gerente",
               data: '{}',
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               success: onSuccess_Gerente,
               failure: function (response) {
                   alert(response.d);
                   //alert('Error');
               }
           });
       }

       function onSuccess_Gerente(response) {
           datos = JSON.parse(response.d);
           
           $(datos).each(function () {
               var option = $(document.createElement('option'));
               option.text(this.NombreGerente);
               option.val(this.IDUsuario);
               $("#cbGerente").append(option);
           });
       }



       // ---------------------------------------------------------------------
       // Gerentes
       // ---------------------------------------------------------------------
       function Get_Gerente_Filtro() {
           $.ajax({
               type: "POST",
               url: "frmEstadoSolicitud.aspx/Get_Gerente",
               data: '{}',
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               success: onSuccess_Gerente_Filtro,
               failure: function (response) {
                   alert(response.d);
                   //alert('Error');
               }
           });
       }

       function onSuccess_Gerente_Filtro(response) {

           datos_2 = JSON.parse(response.d);
               $(datos_2).each(function () {
                   var option_2 = $(document.createElement('option'));
                   option_2.text(this.NombreGerente);
                   option_2.val(this.IDUsuario);
                   $("#cbGerenteFiltro").append(option_2);
               });
       }



       //// ---------------------------------------------------------------------
       //// Etado Gestion
       //// ---------------------------------------------------------------------

       //function Get_SupervidorTodos() {
       //    $.ajax({
       //        type: "POST",
       //        url: "frmEstadoSolicitud.aspx/Get_SupervidorTodos",
       //        data: '{}',
       //        contentType: "application/json; charset=utf-8",
       //        dataType: "json",
       //        success: onSuccess_SupervidorTodos,
       //        failure: function (response) {
       //            alert(response.d);
       //            //alert('Error');
       //        }
       //    });
       //}

       //function onSuccess_SupervidorTodos(response) {
       //    datos = JSON.parse(response.d);
       //    $(datos).each(function () {
       //        var option = $(document.createElement('option'));
       //        option.text(this.NombreSupervisor);
       //        option.val(this.IDUsuario);
       //        $("#cbSupervidor").append(option);
       //    });
       //}


       // ---------------------------------------------------------------------
       // Ver Resumen de Reasignación
       // ---------------------------------------------------------------------
       function f_VerResumenReasignacion(_IDReasignacion, _NroSeguro) {

           if (_IDReasignacion == 'undefined') {
               showError('No existe Registro a visualizar');
               return;
           }
           //alert(_NroSeguro);
           $("#txtIDReasignacion_Tabla").val(_IDReasignacion);
           $("#txtNroSeguro_Tabla").val(_NroSeguro);
           //alert($("#txtNroSeguro_Tabla").val());

           var _IDUsuario = $('#<%=txtIDUsuario.ClientID%>').val();

           $('#modal_Resumen').modal();

           $.ajax({
               type: "POST",
               url: "frmEstadoSolicitud.aspx/Get_ReasignacionResumenMisSolicitudes",
               data: '{_IDReasignacion: "' + _IDReasignacion + '", _IDUsuario: "' + _IDUsuario + '" }',
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               success: onSuccess_ReasignacionResumen,
               failure: function (response) {
                   alert(response.d);
               }
           });
       }

       function onSuccess_ReasignacionResumen(response) {

           datos = JSON.parse(response.d);
           $(datos).each(function () {

               var _IDPerfil = $('#<%=txtIDPerfil.ClientID%>').val();

               $("#txtFechaAprobacion").val(this.FechaAprobacion);
               //Solicitud pendiente
               if (this.IDEstadoAprobacion == 1) {
                   $("#txtFechaAprobacion").val('');
               }
              // $("#txtFechaAprobacion").val(this.FechaAprobacion);
               $("#txtFechaGestion").val(this.FechaGestion);
               $("#txtRutEjecutivoActual").val(this.RutEjecutivoActual);
               $("#txtMesAnnoContable").val(this.MesAnnoContable);
               $("#txtZona").val(this.Zona);
               $("#txtSucursal").val(this.Sucursal);
               $("#txtMotivoRechazo").val(this.MotivoRechazo);
               $("#cbTipoSolicitudResumen").val(this.IDTipoSolicitud);
               $("#cbEstadoAprobacionResumen").val(this.IDEstadoAprobacion);
               $("#cbSupervisor").val(this.IDSupervisor);
               $("#txtIDSupervisorOriginal").val(this.IDSupervisor);
               $("#cbGerente").val(this.IDGerente);
               $("#txtIDGerenteOriginal").val(this.IDGerente);
               $("#txtNroSeguroOriginal").val(this.NroSeguro);
               $("#txtNroSeguroReasignado").val(this.NroSeguroOtro);
               $("#txtObservacion").val(this.Observacion);

               $("#txtMarcaReferidos").val(this.MarcaReferido);
               $("#txtMotivoReasignacion").val(this.MotivoReasignacion);
               $("#txtRutNuevoEjecutivo").val(this.RutEjecutivoReasignacion);
               $("#txtNombreNuevoEjecutivo").val(this.NombreEjecutivoReasignacion);
               $("#txtNroSolicitudDetalle").val(this.IDReasignacion);
               $("#txtFechaSolicitudDetalle").val(this.FechaSolicitud);
               $("#txtCoordinadorDetalle").val(this.Coordinador);
               
               $("#txtEnProceso").val(this.EnProceso);

               

               

               
               
               if (this.IDSupervisor != 0) {
                   $('#cbSupervisor').attr("disabled", false);
               }
               
               if (this.IDGerente != 0) {
                   $('#cbGerente').attr("disabled", false);
               }
               if (_IDPerfil == 1) {
                   $('#pbGrabar').attr("disabled", true);
                   if (this.EstadoGestion == 'Pendiente') {
                       $('#pbGrabar').attr("disabled", false);
                   }
               }
           });
       }


       function f_Grabar() {

           var _IDReasignacion = $("#txtIDReasignacion_Tabla").val();
           var _IDTipoSolicitud = $("#cbTipoSolicitudResumen").val();
           var _EstadoAprobacion = $('#cbEstadoAprobacionResumen option:selected').text();
           var _IDSupervisor = $("#cbSupervisor").val();
           var _IDSupervisorOriginal = $("#txtIDSupervisorOriginal").val();
           var _IDGerente = $("#cbGerente").val();
           var _IDGerenteOriginal = $("#txtIDGerenteOriginal").val();
           var _IDUsuario = $('#<%=txtIDUsuario.ClientID%>').val();

           $.ajax({
               type: "POST",
               url: "frmEstadoSolicitud.aspx/Upd_ReasignacionEstadoTipo",
               data: '{_IDReasignacion: "' + _IDReasignacion + '", _IDTipoSolicitud: "' + _IDTipoSolicitud + '", _EstadoAprobacion: "' + _EstadoAprobacion + '", _IDSupervisor: "' + _IDSupervisor + '", _IDSupervisorOriginal: "' + _IDSupervisorOriginal + '", _IDGerente: "' + _IDGerente + '", _IDGerenteOriginal: "' + _IDGerenteOriginal + '", _IDUsuario: "' + _IDUsuario + '" }',
               //data: '{}',
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               success: onSuccess_EditarReasignacion,
               failure: function (response) {
                   alert(response.d);
               }
           });
       }


       function onSuccess_EditarReasignacion(response) {
           datos = JSON.parse(response.d);
           $(datos).each(function () {
               if (this.Respuesta == 'OK') {
                   showSuccess('Se graban datos de forma exitosa');
                   $("#modal_Resumen").modal('hide');
                   Get_Reasignaciones();
               }
               else {
                   showError('ERROR al grabar información');
               }
           });
       }



       // ---------------------------------------------------------------------
       // Bajar archivo enviado al Banco
       // ---------------------------------------------------------------------
       function f_VerAdjunto() {

           var _IDReasignacion = $("#txtIDReasignacion_Tabla").val();
           var _NroSeguro = '';

           $.ajax({
               type: "POST",
               url: "frmEstadoSolicitud.aspx/Get_Download",
               data: '{_IDReasignacion: "' + _IDReasignacion + '", _NroSeguro: "' + _NroSeguro + '" }',
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               success: onSuccess_Download,
               failure: function (response) {
                   alert(response.d);
               }
           });
       }

       function onSuccess_Download(response) {

           datos = JSON.parse(response.d);
           $(datos).each(function () {
               if (this.Archivo == 'DB_Error') {
                   showError('No tiene documentos adjunto');
                   return;
               }
               var loc = window.location;
               var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
               var _URL_Sitio = loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - (pathName.length - 7)));
             //  alert(loc);
             //  alert(pathName);
             //  alert(_URL_Sitio);
               var _Archivo = _URL_Sitio + '/Download/' + this.Archivo;
             //  alert('ruta: ' + _Archivo);
               window.open(_Archivo, '_blank');
               
           });
       }

       function f_ExportarExcel() {
          var _RowsTabla = document.getElementById("tbl_Data").rows.length;
          if (_RowsTabla == 0) {
              showError("Debe tener datos para Exportar");
              return false;
          }

           var _IDUsuario = $('#<%=txtIDUsuario.ClientID%>').val();
           var _IDReasignacion = $("#txtNroSolicitud").val();
           var _IDEstadoAprobacion = $("#cbEstadoAprobacion").val();
           var _IDTipoSolicitud = $("#cbTipoSolicitud").val();
           var _IDEstadoGestion = $("#cbEstadoGestion").val();
           var _FechaDesde = $("#txtFechaSolicitudDesde").val();
           var _FechaHasta = $("#txtFechaSolicitudHasta").val();
           var _IDSupervisor = $("#cbSupervidor").val();
           var _RutEjecutivo = $("#txtRutEjecutivo").val();
           var _NroSeguro = $("#txtNroSeguro").val();
           var _IDCoordinador = $("#cbCoordinador").val();
           var _IDGerente = $("#cbGerenteFiltro").val();


           if (_IDReasignacion == '') {
               _IDReasignacion = 0;
           }
           if (_RutEjecutivo == '') {
               _RutEjecutivo = 0;
           }
           
           $.ajax({
               type: "POST",
               async: false,
               url: "frmEstadoSolicitud.aspx/Get_MisSolicitudesXLS",
               data: '{_IDReasignacion: "' + _IDReasignacion + '", _IDEstadoAprobacion: "' + _IDEstadoAprobacion + '", _IDTipoSolicitud: "' + _IDTipoSolicitud + '", _IDEstadoGestion: "' + _IDEstadoGestion + '", _FechaDesde: "' + _FechaDesde + '", _FechaHasta: "' + _FechaHasta + '", _IDSupervisor: "' + _IDSupervisor + '", _RutEjecutivo: "' + _RutEjecutivo + '", _NroSeguro: "' + _NroSeguro + '", _IDUsuario: "' + _IDUsuario + '", _IDCoordinador: "' + _IDCoordinador  + '", _IDGerente: "' + _IDGerente + '" }',
               //data: '{}',
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               success: onSuccess_Excel,
               failure: function (response) {
                   alert(response.d);
               }
           });
       }

        function onSuccess_Excel(response) {
            datos = JSON.parse(response.d);

            var createXLSLFormatObj = [];

            var xlsHeader = ["Nro de Solicitud", "Tipo Solicitud", "Nro. Seguro", "Fecha Solicitud", "Mes-Año Contable", "Rut Titular", "Rut Ejecutivo Actual", "UF", "Tipo Producto", "Estado Venta", "Canal Venta","Marca Referidos","CUI","Motivo Reasignación","Nuevo Rut Ejecutivo","Nuevo Nombre Ejecutivo","Canal Venta Nuevo Ejecutivo","CUI Nuevo Ejecutivo","Coordinador","Supervisor","Gerente","Zona","Sucursal","Estado Aprobación","Fecha Aprobación","Estado Gestión","Fecha Gestión","Motivo Rechazo","Observación","Nro Seguro Reasignado"];

            var xlsRows = datos;

            createXLSLFormatObj.push(xlsHeader);

            $.each(xlsRows, function (index, value) {
                var innerRowData = [];
                // $("tbody").append('<tr><td>' + value.IDUsuario + '</td><td>' + value.Usuario + '</td></tr>');
                $.each(value, function (ind, val) {

                    innerRowData.push(val);
                });
                createXLSLFormatObj.push(innerRowData);
            });

            /* File Name */
            var filename = "MisSolicitudes.xlsx";

            /* Sheet Name */
            var ws_name = "Reasignaciones";

            if (typeof console !== 'undefined') console.log(new Date());
            var wb = XLSX.utils.book_new(),
                ws = XLSX.utils.aoa_to_sheet(createXLSLFormatObj);

            /* Add worksheet to workbook */
            XLSX.utils.book_append_sheet(wb, ws, ws_name);

            /* Write workbook and Download */
            if (typeof console !== 'undefined') console.log(new Date());
            XLSX.writeFile(wb, filename);
            if (typeof console !== 'undefined') console.log(new Date());

        }



        // ---------------------------------------------------------------------
        // Supervisor por Gerente
        // ---------------------------------------------------------------------
       function f_Supervisor_x_Gerente() {
           var _Gerente = $("#cbGerenteFiltro").val();
           var _IDUsuario = $('#<%=txtIDUsuario.ClientID%>').val();

            $.ajax({
                type: "POST",
                async: false,
                url: "frmEstadoSolicitud.aspx/Get_SupervisorMasTodos_x_Gerente",
                data: '{_Gerente: "' + _Gerente + '", _IDUsuario: "' + _IDUsuario + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccess_Supevisor_x_Gerente,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function onSuccess_Supevisor_x_Gerente(response) {
            datos = JSON.parse(response.d);
            $('#cbSupervidor').empty();
           $(datos).each(function () {
               var option = $(document.createElement('option'));
               option.text(this.Nombre);
               option.val(this.IDUsuario);
               $("#cbSupervidor").append(option);
           });

            f_Coordinador_x_Supervisor();

        }

        // ---------------------------------------------------------------------
        // Coordinador por Supervisor
        // ---------------------------------------------------------------------
       function f_Coordinador_x_Supervisor() {
           var _Supervisor = $("#cbSupervidor").val();
           var _IDUsuario = $('#<%=txtIDUsuario.ClientID%>').val();
           
            $.ajax({
                type: "POST",
                async: false,
                url: "frmEstadoSolicitud.aspx/Get_CoordinadorMasTodos_x_Supervisor",
                data: '{_Supervisor: "' + _Supervisor + '", _IDUsuario: "' + _IDUsuario + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccess_Coordinador_x_Supervisor,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function onSuccess_Coordinador_x_Supervisor(response) {
            datos = JSON.parse(response.d);
            
            $('#cbCoordinador').empty();
           $(datos).each(function () {
               var option = $(document.createElement('option'));
               option.text(this.Nombre);
               option.val(this.IDUsuario);
               $("#cbCoordinador").append(option);
           });
        }



    </script>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="content content--full">
    <div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Estado Reasignaciones Solicitadas</h3></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-primary">
                    <div class="panel-body">

                        <div class="panel-group">
                          <div class="panel panel-default">
                            <div class="panel-heading">
                              <h4 class="panel-title">
                                <a data-toggle="collapse" class="collapsed" href="#collapse1">Filtros <i class="fa"></i></a>
                              </h4>
                            </div>
                            <div id="collapse1" class="panel-body collapse">


                        <div class="row">
                            <div class="col-md-12">

                                <div class="row">
                                    <div class=" col-md-6 form-group">
                                        <label for="exampleFormControlSelect1">Nro Solicitud</label>
                                        <input id="txtNroSolicitud" class="form-control input-sm"  type="text" tabindex='1'   maxlength="15"/>
                                    </div>

                                    <div class=" col-md-6 form-group">
                                        <label for="exampleFormControlSelect1">Estado Aprobación</label>
                                        <select class="form-control input-sm" id="cbEstadoAprobacion" tabindex="6"></select>
                                    </div>
                                </div>
                                 
                                <div class="row">
                                    <div class=" col-md-6 form-group">
                                        <label for="exampleFormControlSelect1">Tipo Solicitud</label>
                                        <select class="form-control input-sm" id="cbTipoSolicitud" tabindex='2'></select>
                                    </div>

                                    <div class=" col-md-6 form-group">
                                        <label for="exampleFormControlSelect1">Estado Gestión</label>
                                        <select class="form-control input-sm" id="cbEstadoGestion" tabindex="7"></select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class=" col-md-6 form-group">
                                        <label for="exampleFormControlSelect1">Gerente</label>
                                        <select class="form-control input-sm" id="cbGerenteFiltro" onchange="f_Supervisor_x_Gerente();" tabindex="3"></select>
                                    </div>

                                    <div class=" col-md-6 form-group">
                                        <label for="exampleFormControlSelect1">Nro Seguro</label>
                                        <input id="txtNroSeguro" class="form-control input-sm"  type="text"  tabindex="8" maxlength="15"/>
                                    </div>
  
                                </div>

                                 <div class="row">
 
                                    <div class=" col-md-6 form-group">
                                        <label for="exampleFormControlSelect1">Supervisor</label>
                                        <select class="form-control input-sm" id="cbSupervidor" onchange="f_Coordinador_x_Supervisor()" tabindex="4"></select>
                                    </div>

                                    <div class=" col-md-6 form-group">
                                        <label for="exampleFormControlSelect1">Rut Ejecutivo</label>
                                        <input id="txtRutEjecutivo" class="form-control input-sm"  type="text"   maxlength="15" tabindex="9"/>
                                    </div>
                                </div>

                                 <div class="row">
                                    <div class=" col-md-6 form-group">
                                        <label for="exampleFormControlSelect1">Solicitante</label>
                                        <select class="form-control input-sm" id="cbCoordinador" tabindex="5"></select>
                                    </div>

                                    <div class="col-md-1 form-group align-self-center">
                                            <label>Fecha Solicitud</label>
                                     </div>
                                    <div class=" col-md-2 form-group">
                                        <label for="exampleFormControlSelect1">Desde</label>
                                        <input id="txtFechaSolicitudDesde" class="form-control input-sm"  type="date"  tabindex="10"/>
                                    </div>

                                    <div class=" col-md-2 form-group">
                                        <label for="exampleFormControlSelect1">Hasta</label>
                                        <input id="txtFechaSolicitudHasta" class="form-control input-sm"  type="date" tabindex="11" />
                                    </div>
                                     <div class="col-md-1 form-group">
                                     </div>
                                </div>

                                <div class="row">
                                     <div class=" col-md-6 form-group">
                                            <label for="exampleFormControlSelect1">En proceso</label>
                                            <select class="form-control input-sm" id="cbEnProceso" tabindex="5">
                                                <option value="T">Todos</option> 
                                                <option value="True">Si</option> 
                                                <option value="False">No</option>
                                            </select>
                                        </div>

                                       <div class="col-md-2"></div>
                                        <div class="col-md-2">
                                            <button class="btn btn-primary form-control" onclick="f_ExportarExcel();" type="button" id="btn_Excel" tabindex="13">Exportar Excel</button>
                                        </div>
                                        <div class="col-md-2">
                                            <button class="btn btn-primary form-control" onclick="Get_Reasignaciones();" type="button" id="btn_Buscar" tabindex="12">Filtrar</button>
                                        </div>
                                    </div>
                                </div>                     

                            </div>
                        </div>
                            </div>
                          </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12"  >

  

<div class="modal modal-header-success" role="dialog" tabindex="-1" id="CamposOcualtos">
    <asp:TextBox ID="txtNroSolicitudBCO" runat="server" ></asp:TextBox>
    <asp:TextBox ID="txtUsuario_Session" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtIDPerfil" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtIDUsuario" runat="server" ></asp:TextBox>
    <input id="txtIDReasignacion_Tabla" class="form-control input-sm" value="0" type="text" />
    <input id="txtIDSupervisorOriginal"value="0" type="text" />
    <input id="txtIDGerenteOriginal"  value="0" type="text" />
    <input id="txtNroSeguro_Tabla"  value="" type="text" />
    <input id="txtSupervisor" class="form-control input-sm" value="" type="text" />
    <input id="txtSupervisorInternet" class="form-control input-sm" value="" type="text" />
    <input id="txtGerente" class="form-control input-sm" value="" type="text" />

</div>

<div>


                <div class="container">
                    <table id="tbl_Data"
                        <%--class="table table-bordered table-hover col-xs-12"--%>
                        class="table table-hover"
                        <%--data-click-to-select="true"--%>
                        data-toolbar="#toolbar"
                        data-search="true"
                        data-show-toggle="false"
                        data-show-columns="true"
                        data-show-export="true"
                        data-pagination="true"
                        <%--data-id-field="Id"--%>
                        data-page-list="[10, 25, 50, 100, ALL]"
                        data-show-footer="false">
                    </table>
                </div>


    </div>
</div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
</div>

<!--
    MODAL
-->


<div class="modal modal-header-success" role="dialog" tabindex="-1" id="modal_Resumen">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h6 class="modal-title text-white">Detalle Solicitud</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: white;">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row separacion-rows">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4">
                               <label>Rut Ej. Actual</label>
                            </div>
                            <div class="col-md-4">
                                <input id="txtRutEjecutivoActual" class="form-control input-sm" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                               <label>Marca Referidos</label>
                            </div>
                            <div class="col-md-8">
                                <input id="txtMarcaReferidos" class="form-control input-sm" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                               <label>Motivo Reasignación</label>
                            </div>
                            <div class="col-md-8">
                                <input id="txtMotivoReasignacion" class="form-control input-sm" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                               <label>Nuevo RUT Ejecutivo </label>
                            </div>
                            <div class="col-md-4">
                                <input id="txtRutNuevoEjecutivo" class="form-control input-sm" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                               <label>Nombre Ejecutivo</label>
                            </div>
                            <div class="col-md-8">
                                <input id="txtNombreNuevoEjecutivo" class="form-control input-sm" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Zona</label>
                            </div>
                            <div class="col-md-8">
                                <input id="txtZona" class="form-control input-sm" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Nro Solicitud</label>
                            </div>
                            <div class="col-md-4">
                                <input id="txtNroSolicitudDetalle" class="form-control input-sm" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Fecha Solicitud</label>
                            </div>
                            <div class="col-md-4">
                                <input id="txtFechaSolicitudDetalle" class="form-control input-sm" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Tipo Solicitud</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group select">
                                <select id="cbTipoSolicitudResumen" class="input-sm form-control"  disabled="disabled" ></select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Supervisor</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group select">
                                <select id="cbSupervisor" class="input-sm form-control"  disabled="disabled" ></select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Observación</label>
                            </div>
                            <div class="col-md-8">
                                <textarea id="txtObservacion"  disabled="disabled"  class="form-control input-sm"  maxlength="200" rows="4" onkeydown="f_RestrictInvalidChar();"></textarea>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Fecha Gestión</label>
                            </div>
                            <div class="col-md-4">
                                <input id="txtFechaGestion" class="form-control input-sm" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Mes-Año contable</label>
                            </div>
                            <div class="col-md-4">
                                <input id="txtMesAnnoContable" class="form-control input-sm" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Sucursal</label>
                            </div>
                            <div class="col-md-8">
                                <input id="txtSucursal" class="form-control input-sm" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Solicitante</label>
                            </div>
                            <div class="col-md-8">
                                <input id="txtCoordinadorDetalle" class="form-control input-sm" disabled="disabled" />
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label>Adjunto</label>
                            </div>
                            <div class="col-md-4">
                                        <button type="button"  title="Información Adjunta" onclick="f_VerAdjunto();">
                                        <i class="fas fa-paperclip fa-2x"></i>
                                        </button>
                    
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Motivo Rechazo</label>
                            </div>
                            <div class="col-md-8">
                                <input id="txtMotivoRechazo" class="form-control input-sm" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Fecha Aprobación</label>
                            </div>
                            <div class="col-md-4">
                                <input id="txtFechaAprobacion" class="form-control input-sm" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Estado Aprobación</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group select">
                                <select id="cbEstadoAprobacionResumen" class="input-sm form-control"  disabled="disabled" ></select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Gerente</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group select">
                                <select id="cbGerente" class="input-sm form-control"  disabled="disabled" ></select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Nro Seguro Reasignado</label>
                            </div>
                            <div class="col-md-4">
                                <input id="txtNroSeguroReasignado"  disabled="disabled"  class="form-control input-sm"  type="text" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Nro Seguro</label>
                            </div>
                            <div class="col-md-4">
                                <input id="txtNroSeguroOriginal"  disabled="disabled"  class="form-control input-sm"  type="text" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>En Proceso</label>
                            </div>
                            <div class="col-md-4">
                                <input id="txtEnProceso"  disabled="disabled"  class="form-control input-sm"  type="text" />
                            </div>
                        </div>
                    </div>

                </div>









                <div class="row separacion-rows">
                    <div class="col-md-12"></div>
                </div>
                <div class="row separacion-rows">
                    <div class="col-md-12"></div>
                </div>
                <div class="row separacion-rows">
                    <div class="col-md-12"></div>
                </div>

                
                <div class="row">
                    <div class="col-md-8"></div>
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-2">
                        <button id="pbGrabar" onclick="f_Grabar();" class="btn btn-primary form-control" type="button" disabled="disabled">Grabar</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



<div class="modal modal-header-success" role="dialog" tabindex="-1" id="modal_Upload">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title bg-primary">Subir Respuesta Banco</h4></div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3">
                        <label>Archivo .CSV</label>
                    </div>
                    <div class="col-md-9">
                        <asp:FileUpload ID="fuSubirArchivo" runat="server" Width="90%" />
                    </div>
                </div>
                
                <div class="row">
                   
                    <div class="col-md-10"></div>
                    <div class="col-md-2">
                        <%--<button class="btn btn-primary" type="button">Subir</button>--%>
                        <asp:Button ID="pbUpdateFile" class="btn btn-primary form-control"  runat="server"  Text="Subir" />
                        <asp:Button ID="pbCancelar" class="btn btn-primary form-control"  runat="server"  Text="Cancelar" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-header-success" role="dialog" tabindex="-1" id="modal_Download">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title bg-primary">Descargar Archivo enviado al Banco</h4></div>
            <div class="modal-body">

                
                <div class="row">
                   
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <%--<button class="btn btn-primary" type="button">Subir</button>--%>
                        <%--<asp:Button ID="pbDescargar_" class="btn btn-primary"  runat="server"  Text="Subir" />--%>
                        <asp:Button ID="pbDescargar"  class="btn btn-primary" runat="server" Text="Descargar" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%--    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">        
    </asp:ToolkitScriptManager>--%>
    <script>
        if ($('#<%=txtIDPerfil.ClientID%>').val() == 4) {
            //Activo
            document.getElementById('rptBotonera_primero_0').style.backgroundColor = "#128ff2";
            document.getElementById('rptBotonera_primero_0').style.borderColor = "#128ff2";
            //Normal

            document.getElementById('rptBotonera_primero_1').style.backgroundColor = "#00438c";
            document.getElementById('rptBotonera_primero_1').style.borderColor = "#356396";

            document.getElementById('rptBotonera_primero_2').style.backgroundColor = "#00438c";
            document.getElementById('rptBotonera_primero_2').style.borderColor = "#356396";


        } else {
            //Activo
            document.getElementById('rptBotonera_primero_1').style.backgroundColor = "#128ff2";
            document.getElementById('rptBotonera_primero_1').style.borderColor = "#128ff2";
            //Normal

            document.getElementById('rptBotonera_primero_0').style.backgroundColor = "#00438c";
            document.getElementById('rptBotonera_primero_0').style.borderColor = "#356396";

            document.getElementById('rptBotonera_primero_2').style.backgroundColor = "#00438c";
            document.getElementById('rptBotonera_primero_2').style.borderColor = "#356396";

            document.getElementById('rptBotonera_primero_3').style.backgroundColor = "#00438c";
            document.getElementById('rptBotonera_primero_3').style.borderColor = "#356396";
        }


        document.getElementById('navbarDropdown').style.backgroundColor = "#00438c";
        document.getElementById('navbarDropdown').style.borderColor = "#356396";

    </script>

</asp:Content>
