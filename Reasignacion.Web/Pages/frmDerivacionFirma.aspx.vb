﻿Imports Newtonsoft.Json
Imports System.Web.Services

Public Class frmDerivacionFirma
    Inherits Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtIDUsuario.Text = Session("IDUsuario")
            txtIDPerfil.Text = Session("IDGrupo")
            If txtIDUsuario.Text = "" Then
                Response.Redirect("frmLogin.aspx")
            End If
        End If
    End Sub

    Public Shared Function ToJSON(pTable As DataTable) As String
        Dim vCadenaJSON As String = ""
        vCadenaJSON = JsonConvert.SerializeObject(pTable)
        If vCadenaJSON.Length <= 5 Then
            vCadenaJSON = "[{""Nro_Seguro"":""0""}]"
        End If
        Return vCadenaJSON
    End Function

    <WebMethod()>
    Public Shared Function GetCoordinador() As String
        Try
            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec.GetCoordinador(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <WebMethod()>
    Public Shared Function GetSupervisor() As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec.GetSupervisor(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <WebMethod()>
    Public Shared Function GetSupervisorInternet() As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            'Exec.GetSupervisorInternet(rp)
            Exec.GetSupervisor(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <WebMethod()>
    Public Shared Function GetGerente() As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec.GetGerente(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    <WebMethod()>
    Public Shared Function GetDerivacionFirma() As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec.GetDerivacionFirma(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    <WebMethod()>
    Public Shared Function UpdateDerivacionFirma(UsuarioCoor As String, UsuarioSuper As String, UsuarioSuperInt As String, UsuarioGerente As String) As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._UsuarioCoor = UsuarioCoor
            Exec._UsuarioSuper = UsuarioSuper
            Exec._UsuarioSuperInter = UsuarioSuperInt
            Exec._UsuarioGerente = UsuarioGerente

            Exec.UpdateDerivacionFirma(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <WebMethod()>
    Public Shared Function InsertDerivacionFirma(UsuarioCoor As String, UsuarioSuper As String, UsuarioSuperInt As String, UsuarioGerente As String) As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._UsuarioCoor = UsuarioCoor
            Exec._UsuarioSuper = UsuarioSuper
            Exec._UsuarioSuperInter = UsuarioSuperInt
            Exec._UsuarioGerente = UsuarioGerente

            Exec.InsertDerivacionFirma(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
End Class