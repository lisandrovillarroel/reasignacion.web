﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Pages/Sitio.Master" CodeBehind="frmIngresoSolicitud.aspx.vb" Inherits="Reasignacion.Web.frmIngresoSolicitud" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">


        $(document).ready(function () {

           
            $(document).ajaxStart($.blockUI({ message: '<h1><img src="../Img/loading.gif" /> Procesando...</h1>' })).ajaxStop($.unblockUI);

            f_FechaCierreVenta();
            Get_MotivoReasignacion();

            var _IDUsuario = $('#<%=txtIDUsuario.ClientID%>').val();

            f_DerivacionFirma(_IDUsuario);
            f_EsSupervisorInternet(_IDUsuario);
            //$.unblockUI();

            $(".pasteOnlyNumbers").on('paste', function (e) {
                if (e.originalEvent.clipboardData.getData('Text').match(/[^\d]/)) {
                    e.preventDefault();
                }
            });

            $(".onlyNumbers").on("keypress", function (e) {
                // console.log(e.keyCode);
                if (e.keyCode < 48 || e.keyCode > 57) {
                    return false;
                }
            });


            //var ambit = $(document);

            //Disable Cut + Copy + Paste (input)
            //ambit.on('copy paste cut', function (e) {
            //    e.preventDefault(); //disable cut,copy,paste
            //    return false;
            //});


        });

        //---------------------------------------------------------------------
        // Buscar Titular
        //---------------------------------------------------------------------
        function f_buscarTitular() {
            $("#txtRutTitular").val('');
            $("#txtNombreTitular").val('');

            $('#showBuscarTitular').modal('show');
            $('#showBuscarTitularBody').empty();
            $('#showBuscarTitularBody').html();

            f_llenaTitular();
            return
        }


        function f_llenaTitular() {
            $('#tbl_dataTitular').bootstrapTable('destroy')
            $('#tbl_dataTitular').bootstrapTable({
                columns: [
                     { align: 'center', title: 'Seleccionar', formatter: Col_Seleccionar }
                    , { field: '', title: '#', visible: true, sortable: true }
                    , { field: '', title: 'Nombre del Seguro', visible: true, sortable: true}
                    , { field: '', title: 'Oficina', visible: true, sortable: true }
                    , { field: '', title: 'CUI', visible: true, sortable: true }
                    , { field: '', title: 'Nombre Ejecutivo', visible: true, sortable: true }
                    , { field: '', title: 'Rut Ejecutivo', visible: true, sortable: true }
                    , { field: '', title: 'Folio', visible: true, sortable: true }
                ]
                , data: ''
            });
        }


        function Col_Seleccionar(value, row, index) {
            return [
                '<button type="button" class="btn btn-default" aria-label="Left Align" data-toggle="tooltip" href="#menu1" title="Aprobar Solicitud" onclick="f_Seleccionar(\'' + row.folio + '\');">',
                '<span class="fa fa-check  fa-lg" style="color:' + row.ColorAprobada + '\;"  aria-hidden="true"></span>',
                '</button>'
            ].join('');
        }

        function Col_Fila(value, row, index) {
            index++; {
                return index;
            }
        }

        function f_Seleccionar(nroSeguro){
            //alert(nroSeguro);
            $('#showBuscarTitular').modal('hide')
            $("#txtNroSeguro").val(nroSeguro);
            f_BuscarDatosSeguro();
        }

        function f_llenaTitularBusca() {

            var _txtRutTitular = $("#txtRutTitular").val();
            var _txtNombreTitular = $("#txtNombreTitular").val();
            //alert('rut titular: ' + $("#txtRutTitular").val());
            $.ajax({
                type: "POST",
                url: "frmIngresoSolicitud.aspx/Get_buscaBvaBigsaMaestra",
                data: '{_txtRutTitular: "' + _txtRutTitular + '", _txtNombreTitular: "' + _txtNombreTitular + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccess_buscaBvaBigsaMaestra,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function onSuccess_buscaBvaBigsaMaestra(response) {

            lista = JSON.parse(response.d);
            $('#tbl_dataTitular').bootstrapTable('destroy')
            $('#tbl_dataTitular').bootstrapTable({
                columns: [
                      { align: 'center', title: 'Seleccionar', formatter: Col_Seleccionar }
                    , {  title: '#', formatter: Col_Fila, visible: true, sortable: true }
                    , { field: 'nombreDelSeguro', title: 'Nombre del Seguro', visible: true, sortable: true }
                    , { field: 'oficina', title: 'Oficina', visible: true, sortable: true }
                    , { field: 'cui', title: 'CUI', visible: true, sortable: true }
                    , { field: 'nombreEjecutivo', title: 'Nombre Ejecutivo', visible: true, sortable: true }
                    , { field: 'rutEjecutivo', title: 'Rut Ejecutivo', visible: true, sortable: true }
                    , { field: 'folio', title: 'Folio', visible: true, sortable: true }
                  ]
                  , data: lista
              });
          }
          


        // ---------------------------------------------------------------------
        // Obtiene Estado del Seguro
        // ---------------------------------------------------------------------
        function f_FechaCierreVenta() {

            $.ajax({
                type: "POST",
                //async: false,
                url: "frmIngresoSolicitud.aspx/Get_FechaCierreVenta",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccess_FechaCierreVenta,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function onSuccess_FechaCierreVenta(response) {

            datos = JSON.parse(response.d);

            $(datos).each(function () {
                $('#txtMesCierreVenta').val(this.MES_VENTA);
                $('#txtAnnoCierreVenta').val(this.ANIO_VENTA);
                $('#txtFechaTerminoMes').val(this.FECHA_TERMINO_MES);
                $('#txtFechaCierreVenta').val(this.FECHA_CIERRE);
                $('#txtDiaActual').val(this.DiaActual);
                $('#txtMesAnterior').val(this.MesAnterior);
                $('#txtMesCalendario').val(this.mesCalendario);
            });
        }

        // ---------------------------------------------------------------------
        // Derivacion Firma
        // ---------------------------------------------------------------------
        function f_DerivacionFirma(_IDUsuario) {

            $.ajax({
                type: "POST",
                async: false,
                url: "frmIngresoSolicitud.aspx/Get_DerivacionFirma",
                data: '{_IDUsuario: "' + _IDUsuario + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccess_DerivacionFirma,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function onSuccess_DerivacionFirma(response) {
            datos = JSON.parse(response.d);
            $(datos).each(function () {
                $('#txtSupervisor').val(this.Supervisor);
                $('#txtSupervisorInternet').val(this.SupervisorInternet);
                $('#txtGerente').val(this.Gerente);
            });
        }

        // ---------------------------------------------------------------------
        // Permite ver si es Supervisor Internet en tabla Derivaciones
        // ---------------------------------------------------------------------
        function f_EsSupervisorInternet(_IDUsuario) {

            $.ajax({
                type: "POST",
                async: false,
                url: "frmIngresoSolicitud.aspx/Get_EsDerivacionInternet",
                data: '{_IDUsuario: "' + _IDUsuario + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccess_EsDerivacionInternet,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function onSuccess_EsDerivacionInternet(response) {
            datos = JSON.parse(response.d);
            $(datos).each(function () {
                if (this.SupervisorInternet == undefined) {
                    $('#txtEsSupervisorInternet').val('N');
                }
                else {
                    $('#txtEsSupervisorInternet').val('S');
                }
            });
        }
        // ---------------------------------------------------------------------
        // Obtener Datos del seguro
        // ---------------------------------------------------------------------

        function f_BuscarDatosSeguro() {
            
            var _OrigenSeguro = $("input[name='rbTipoSeguro']:checked").val();
            var _LargoNroSeguro = $("#txtNroSeguro").val().length;
            var _NroSeguro = $("#txtNroSeguro").val();
            var _IDPerfil = $('#<%=txtIDPerfil.ClientID%>').val();

            if (_NroSeguro == '') {
                showError('Debe ingresar número de seguro a buscar');
                return;
            }

            if (_OrigenSeguro == 'SCS' && _LargoNroSeguro != 15) {
                showError('Largo de nro de seguro no corresponde a SCS');
                return;
            }
            if (_OrigenSeguro == 'VIAJE') {
                if (_LargoNroSeguro != 10 && _LargoNroSeguro != 8) {
                    showError('Largo de nro de seguro no corresponde a Viaje');
                    return;
                }
                var _Texto = _NroSeguro.substring(0, 2);
                if (_LargoNroSeguro == 10 && _Texto != 'VJ') {
                    showError('Nro de Seguro de Viaje Inválido');
                    return;
                }
                if (_LargoNroSeguro == 8) {
                    $("#txtNroSeguro").val('VJ' + _NroSeguro)
                }
            }
            if (_OrigenSeguro == 'RCI') {
                if (_LargoNroSeguro != 11 && _LargoNroSeguro != 8) {
                    showError('Largo de nro de seguro no corresponde a RCI');
                    return;
                }

                var _Texto = _NroSeguro.substring(0, 3);
                if (_LargoNroSeguro == 11 && _Texto != 'RCI') {
                    showError('Nro de Seguro RCI Inválido');
                    return;
                }
                if (_LargoNroSeguro == 8) {
                    $("#txtNroSeguro").val('RCI' + _NroSeguro)
                }
            }
            if (_OrigenSeguro == 'BIGSA') {
                if (_LargoNroSeguro != 7) {
                    showError('Largo de nro de seguro no corresponde a BIGSA');
                    return;
                }
            }
            if (_OrigenSeguro == 'ISOL') {
                if (_LargoNroSeguro != 15 && _LargoNroSeguro != 7) {
                    showError('Largo de nro de seguro no corresponde a ISOL');
                    return;
                }
            }

            $('#<%=txtNroSeguro_Doc.ClientID%>').val($("#txtNroSeguro").val())
            f_PermiteIngresarSeguro();
            if ($('#txtSeguirValidando').val() == 'N') {
                return;
            }
            f_BuscarDatosSeguroBVA_2();
            f_BuscarDatosSeguroBVA();
            
            if ($('#txtSeguirValidando').val() == 'N') {
                return;
            }

            if (_IDPerfil != 1) {
                if ($('#txtRamoAceptado').val() == 'N') {
                    showError('Este tipo de seguro  está asociado a la información de la operación. Para poder solicitar la reasignación banco debe cambiar antes los datos de la operación');
                    $('#txtSeguirValidando').val('N')
                    f_LimpiarDatos();
                    return
                }
            }
            f_Validaciones();
        }

        function f_BuscarDatosSeguroBVA_2() {
            var _OrigenSeguro = $("input[name='rbTipoSeguro']:checked").val();
            var _NroSeguro = $('#txtNroSeguro').val();
            $.ajax({
                type: "POST",
                async: false,
                url: "frmIngresoSolicitud.aspx/Get_Seguro_BVA2",
                data: '{_OrigenSeguro: "' + _OrigenSeguro + '" , _NroSeguro : "' + _NroSeguro + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccess_SegurosBVA_SN,
                failure: function (response) {
                    alert(response.d);
                    //alert('Error');
                }
            });
        }


        function onSuccess_SegurosBVA_SN(response) {
            datos = JSON.parse(response.d);
            $(datos).each(function () {
                if (this.NRO_SEGURO != 0) {
                    $('#txtExisteEnBVA').val('S');
                }
            });
        }

        function f_BuscarDatosSeguroBVA() {
            var sesion='<%=Session("IDUsuario")%>';
            if(sesion==''){
                window.location.href = "frmLogin.aspx";
            }
            // PageMethods.revisarSesion();
            /*
            $.ajax({
                type: "POST",
                async: false,
                url: "frmIngresoSolicitud.aspx/revisarSesion",
                data: '{}',
                contentType: "application/json; charset=utf-8",
            });
            */
            var _OrigenSeguro = $("input[name='rbTipoSeguro']:checked").val();
            var _NroSeguro = $('#txtNroSeguro').val();
          
            $(document).ajaxStart($.blockUI({ message: '<h1><img src="../Img/loading.gif" /> Procesando...</h1>' })).ajaxStop($.unblockUI);

            $.ajax({
                type: "POST",
                async: false,
                url: "frmIngresoSolicitud.aspx/Get_Seguro_BVA",
                data: '{_OrigenSeguro: "' + _OrigenSeguro + '" , _NroSeguro : "' + _NroSeguro + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccess_SegurosBVA,
                failure: function (response) {
                    alert(response.d);
                    //alert('Error');
                }
            });
        }

        function onSuccess_SegurosBVA(response) {
            datos = JSON.parse(response.d);
            $(datos).each(function () {

                if (this.NRO_SEGURO == '0') {
                    $('#txtSeguirValidando').val('N')
                    showError('No existe Seguro');
                    return;
                }

                if (this.RutTitular == undefined) {
                    return;
                }
                if (this.ESTADO_VENTA == 'CANCELADO') {
                    $('#txtSeguirValidando').val('N')
                    showError('Seguro no esta Vigente');
                    return;
                }

                $('#txtRutCliente').val(this.RutTitular);
                $('#txtNroRutTitular').val(this.NroRutTitular);
                $('#txtRutEjecutivo').val(this.RutEjecutivo);
                $('#txtNombreEjecutivo').val(this.NombreEjecutivo);
                $('#txtUF').val(this.PBRUTAUF);
                $('#txtTipoProducto').val(this.TIPO_PRODUCTO);
                $('#txtEstadoVenta').val(this.ESTADO_VENTA);
                $('#txtCanalVenta').val(this.DescCanalDetalle);
                if (this.Internet == 'S') {
                    $('#txtMarcaReferidos').val('');
                }
                else {
                    $('#txtMarcaReferidos').val(this.MARCA_REFERIDOS);
                }
                $('#txtCUI').val(this.IdVtaCui);
                $('#txtMesContable').val(this.MES_CONTABLE);
                $('#txtAnnoContable').val(this.ANNO_CONTABLE);
                $('#txtRamoAceptado').val(this.RamoAceptado);
                $('#txtEsInternet').val(this.Internet);

                $('#txtSucursal').val(this.DescVtaSuc);
                $('#txtZona').val(this.DescVtaZonMod);
            });
        }



        function f_LimpiarDatos_TipoSeguro() {
            var _NroSeguro = $('#txtNroSeguro').val();
            if (_NroSeguro != '') {
                f_LimpiarDatos();
                //f_BuscarDatosSeguro();
            }


        }

        function ValidarImagen(obj) {
            var uploadFile = obj.files[0];

            if (!window.FileReader) {
                showError('El navegador no soporta la lectura de archivos');
                $('#fileUp-input').val(null);
                return;
            }

            if (!(/\.(msg)$/i).test(uploadFile.name)) {
                showError('El archivo a adjuntar no es una archivo .msg');
                $('#fileUp-input').val(null);
                return;
            }
            else if (uploadFile.size > 18000000) {
                showError('El peso de la imagen no puede exceder los 18 MB');
                $('#fileUp-input').val(null);
                return;
            }
           
        }

       

        function f_LimpiarDatos() {
            document.getElementById("pbGrabar").disabled = false;
            //$('#txtNroSeguro').val('');
            $('#txtExisteEnBVA').val('N');
            $('#txtRutCliente').val('');
            $('#txtNroRutTitular').val('0');

            $('#txtRutEjecutivo').val('');
            $('#txtNombreEjecutivo').val('');
            $('#txtUF').val('');
            $('#txtTipoProducto').val('');
            $('#txtEstadoVenta').val('');
            $('#txtCanalVenta').val('');
            $('#txtMarcaReferidos').val('');
            $('#txtCUI').val('');
            $('#txtMesContable').val('');
            $('#txtAnnoContable').val('');
            $('#cbMotivoReasignacion').val('0');
            //var _IDMotivoReasignacion = $('#cbMotivoReasignacion').val();
            //            var _MotivoReasignacion = $('#cbMotivoReasignacion option:selected').text();
            $('#txtNuevoRutEjecutivo').val('');
            $('#txtNuevoDVEjecutivo').val('');
            $('#txtNombreReasignacion').val('');
            $('#txtCanalVentaReasignacion').val('');
            $('#txtCUI_Reasignacion').val('');
            //Desabilitar CUI nuevo
            document.getElementById("txtCUI_Reasignacion").disabled = false;
            $('#txtObservacion').val('');
            $('#txtNroRutTitular').val('0'); 
            $('#txtEjecutivoActivo').val('S');
            $('#txtRamoAceptado').val('N');
            $('#txtClienteEsFuncionario').val('N');
            //$('#txtMesCierreVenta').val('0');
            //$('#txtAnnoCierreVenta').val('0');
            //$('#txtFechaCierreVenta').val('0');
            //$('#txtSupervisor').val('');
            //$('#txtSupervisorInternet').val('');
            //$('#txtGerente').val('');
            
            $('#txtEsInternet').val('');
            $('#txtIDTipoSolicitud').val('0');
            $('#txtSucursal').val('');
            $('#txtZona').val('');

            $('#txtZonaEje').val('');
            $('#txtSucursalEje').val('');
            
          ////  $('#txtExisteEnBVA').val('S');
            //$('#txtDiaActual').val('0');
            //$('#txtMesAnterior').val('000000');
            $('#txtIngresoDocumento').val('N');
            $('#txtSeguirValidando').val('S');
            $('#txtValidaSeguro').val('S');
            
            $('#txtComentario').val('S');

            $('#<%=txtComentario.ClientID%>').val('')
            $('#fileUp-input').val('');
            $('#fechaDiaHabil').val(0);
            
            $('#pbGrabar').attr("disabled", false);
            $('#pbAdjuntar_Doc').attr("disabled", false);

            $("#txtRutTitular").val('');
            $("#txtNombreTitular").val('');
        }


        function f_Validaciones() {
            var _OrigenSeguro = $("input[name='rbTipoSeguro']:checked").val();
            var _IDPerfil = $('#<%=txtIDPerfil.ClientID%>').val();
            if (_OrigenSeguro == 'SCS') {
                //OJO   f_BuscarEstadoSeguro();
            }

            if ($('#txtSeguirValidando').val() == 'N') {
                return;
            }

            f_BuscarClienteEsFuncionario();


            var _MarcaReferido = $('#txtMarcaReferidos').val();
            var _FechaContable = $('#txtAnnoContable').val() + ('0' + $('#txtMesContable').val()).slice(-2);
            var _MesContable = ('0' + $('#txtMesContable').val()).slice(-2);
            var _FechaVenta = $('#txtAnnoCierreVenta').val() + ('0' + $('#txtMesCierreVenta').val()).slice(-2);
            var _AnoVenta = $('#txtAnnoCierreVenta').val();

            var _MesVenta = ('0' + $('#txtMesCierreVenta').val()).slice(-2);
            var _EsInternet = $('#txtEsInternet').val();
            var _Supervisor = $('#txtSupervisor').val();
            var _SupervisorInternet = $('#txtSupervisorInternet').val();
            var _Gerente = $('#txtGerente').val();
            var _DiaActual = $('#txtDiaActual').val();
            var _MesAnterior = ('0' + $('#txtMesAnterior').val()).slice(-2);
            var _MesCalendario = $('#txtMesCalendario').val();
            var _IDPerfil = $('#<%=txtIDPerfil.ClientID%>').val();
            var validaOk = 'Ok';

            //if ($('#txtMesContable').val() == $('#txtMesCierreVenta').val() && $('#txtAnnoContable').val() == $('#txtAnnoCierreVenta').val()) {
            //alert(_MesAnterior);
           // alert('#txtExisteEnBVA: ' + $('#txtExisteEnBVA').val());
            if ($('#txtExisteEnBVA').val() === 'S' && _IDPerfil != 1) {
                //alert('_FechaContable: ' + _FechaContable);
                //alert('_FechaVenta: ' + _FechaVenta);
                if (_FechaContable < _FechaVenta) {
                    if ((_FechaVenta - _FechaContable) != 1) {
                        showError('Ingreso de seguro fuera del Periodo, máximo ingreso hasta el mes anterior');
                        validaOk = 'Not';
                        //return;
                        if (_IDPerfil != 1) {
                            $('#pbGrabar').attr("disabled", true);
                            $('#pbAdjuntar_Doc').attr("disabled", true);
                        }
                    }
                    //alert('_MesAnterior: ' + _MesAnterior);
                    //alert('_MesContable: ' + _MesContable);
                    //alert('_DiaActual: ' + _DiaActual);
                    //alert('_MesCalendario: ' + _MesCalendario);
                    //alert('_FechaVenta: ' + _FechaVenta);
                    var _DiaActual_2 = _DiaActual;
                    if (_FechaVenta < _MesCalendario)
                        _DiaActual_2 = 30;

                    //alert('_DiaActual nuevo: ' + _DiaActual_2);
                    if (_MesAnterior == _MesContable && _DiaActual_2 >= 21) {
                        showError('Ventas del mes anterior solo se pueden ingresar hasta el día 20');
                        validaOk = 'Not';
                        //return;
                        if (_IDPerfil != 1) {
                            $('#pbGrabar').attr("disabled", true);
                            $('#pbAdjuntar_Doc').attr("disabled", true);
                        }
                    }
                }

                // ---------------------------------------------------------------------
                // Reasignación
                // ---------------------------------------------------------------------
                if (validaOk == 'Ok') {
                    if (_EsInternet == 'N') {
                        if (_MarcaReferido != '' && _MarcaReferido != null) {
                            _EsInternet = 'S'
                            $('#txtEsInternet').val(_EsInternet);
                        }
                    }
                    
                    var diaTermino = $('#txtFechaTerminoMes').val().substr(6, 2); // Fecha de termino del mes
                    var diaCierre = $('#txtFechaCierreVenta').val().substr(6, 2); // Fecha de Cierre
                    
                   // alert('diaTermino: ' + diaTermino);
                    f_diaHabil(_AnoVenta, _MesVenta, 3);
                  //  _DiaActual=5 para prueba
                    var _DiaActual_2 = _DiaActual // solo para mes anterior
                    //var ano = $('#fechaDiaHabil').val().substr(0, 4);
                    //var mes = $('#fechaDiaHabil').val().substr(5, 2);
                    var _diahabil = $('#fechaDiaHabil').val().substr(8, 2);
                    var RelRea = ""
                   // alert('_FechaContable:' + _FechaContable);
                   // alert('_FechaVenta' + _FechaVenta);
                    if (_FechaContable == _FechaVenta) {
                        RelRea = "Reasignacion"
                       // alert('Dia Actual: ' + _DiaActual);
                       // alert('Dia Cierre: ' + diaCierre);
                        //alert('Dia Habil: ' + _diahabil);
                        if (_DiaActual >= 1 && _DiaActual <= diaCierre)
                            _DiaActual_2 = 32

                        //alert('_DiaActual: ' + _DiaActual);
                        //alert('_DiaActual_2: ' + _DiaActual_2);
                        //alert('diaTermino: ' + diaTermino);
                        //alert('_diahabil: ' + _diahabil);
                        if (_DiaActual_2 <= diaTermino || _DiaActual <= _diahabil) {
                        //if (_DiaActual <= _diahabil) {
                            RelRea = "Reasignacion"
                        }
                        else {
                            RelRea = "Reliquidacion";
                        }
                    } else {
                        if (_FechaContable > _FechaVenta)// cuando la solicitud es mayor que el mes venta
                            RelRea = "Reasignacion";
                        else
                            RelRea = "Reliquidacion";
                    }
                  //  alert('RelRea: ' + RelRea);
                    if (RelRea === "Reasignacion") {

                        if (_EsInternet == 'N' && _Supervisor == '') {
                            $('#txtIDTipoSolicitud').val(3);    // Especial - Reasignación
                        }
                        else if (_EsInternet == 'S' && _SupervisorInternet == '') {
                            $('#txtIDTipoSolicitud').val(3);    // Especial - Reasignación
                        }
                        else if (_EsInternet == 'N') {
                            $('#txtIDTipoSolicitud').val(1);    // Normal - Reasignación
                        }
                        else if (_EsInternet == 'S') {
                            $('#txtIDTipoSolicitud').val(2);    // Referido - Reasignación
                        }
                        else {
                            $('#txtIDTipoSolicitud').val(4);    // No Definido - Reasignación
                        }

                    }
                    else {

                        if (_EsInternet == 'N' && _Supervisor == '') {
                            $('#txtIDTipoSolicitud').val(7);    // Especial - Reliquidación
                        }
                        else if (_EsInternet == 'S' && _SupervisorInternet == '') {
                            $('#txtIDTipoSolicitud').val(7);    // Especial - Reliquidación
                        }
                        else if (_EsInternet == 'N') {
                            $('#txtIDTipoSolicitud').val(5);    // Normal - Reliquidación
                        }
                        else if (_EsInternet == 'S') {
                            $('#txtIDTipoSolicitud').val(6);    // Referido - Reliquidación
                        }
                        else {
                            $('#txtIDTipoSolicitud').val(8);    // No Definido - Reliquidación
                        }

                        showInfo('Esta solicitud será RELIQUIDACIÓN');
                    }

                } else {
                 //   alert('paso por limpia');
                    $('#txtNroSeguro').val('');
                    f_LimpiarDatos();
                    $('#txtValidaSeguro').val('N');
                    return
                }

            }else {
                if (_EsInternet == 'N' && _Supervisor == '') {
                    $('#txtIDTipoSolicitud').val(3);    //  1 Especial - Reasignación
                }
                else if (_EsInternet == 'S' && _SupervisorInternet == '') {
                    $('#txtIDTipoSolicitud').val(3);    // Especial - Reasignación
                }
                else if (_EsInternet == 'N') {
                    $('#txtIDTipoSolicitud').val(1);    // Normal - Reasignación
                }
                else if (_EsInternet == 'S') {
                    $('#txtIDTipoSolicitud').val(2);    // Referido - Reasignación
                }
                else {
                    $('#txtIDTipoSolicitud').val(4);    // No Definido - Reasignación
                }
            }
            if ($('#txtEsSupervisorInternet').val() == 'S') {
                if ($('#txtIDTipoSolicitud').val() == 2 || $('#txtIDTipoSolicitud').val() == 6) {
                } else {
                    showError('Supervisor internet, Solo puede ingresar Solicitudes Referidas');
                    $('#txtNroSeguro').val('');
                    f_LimpiarDatos();
                    return
                }
            }
            $('#txtValidaSeguro').val('N');
            
        }


        //----------------------------------------------------------------------
        // Obtiene 3 dia habil
        //----------------------------------------------------------------------
        function f_diaHabil(_AnoVenta, _MesVenta,_dia_cierre) {
            
            $.ajax({
                type: "POST",
                async: false,
                url: "frmIngresoSolicitud.aspx/Get_diaHabil",
                data: '{_AnoVenta: "' + _AnoVenta + '" , _MesVenta : "' + _MesVenta + '" , _dia_cierre : "' + _dia_cierre + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccess_diaHabil,
                failure: function (response) {
                    alert(response.d);
                    //alert('Error');
                }
            });
        }

        function onSuccess_diaHabil(response) {
            datos = JSON.parse(response.d);
            $(datos).each(function () {
                $('#fechaDiaHabil').val(this.COD_DIA);
            });
        }

        // ---------------------------------------------------------------------
        // Obtiene Estado del Seguro
        // ---------------------------------------------------------------------
        function f_BuscarEstadoSeguro() {

            var _OrigenSeguro = $("input[name='rbTipoSeguro']:checked").val();
            var _NroSeguro = $('#txtNroSeguro').val();

            $.ajax({
                type: "POST",
                async: false,
                url: "frmIngresoSolicitud.aspx/Get_DatosSeguro",
                data: '{_OrigenSeguro: "' + _OrigenSeguro + '" , _NroSeguro : "' + _NroSeguro + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccess_EstadoSeguro,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function onSuccess_EstadoSeguro(response) {
            datos = JSON.parse(response.d);
            $(datos).each(function () {
                if (this.SeguroVigente != 'S') {
                    $('#txtSeguirValidando').val('N');
                    showError('Seguro no esta Vigente');
                    f_LimpiarDatos();
                }
            });
        }


        // ---------------------------------------------------------------------
        // Obtiene si Cliente es Funcionario
        // ---------------------------------------------------------------------

        function f_BuscarClienteEsFuncionario() {

            $('#txtClienteEsFuncionario').val('N');
            var _Rut = $('#txtNroRutTitular').val(); //'13932224'

            $.ajax({
                type: "POST",
                async: false,
                url: "frmIngresoSolicitud.aspx/Get_Funcionario",
                data: '{_Rut: "' + _Rut + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccess_ClienteEsFuncionario,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function onSuccess_ClienteEsFuncionario(response) {
            datos = JSON.parse(response.d);

            $(datos).each(function () {

                var _Rut = this.RUT;
                if (_Rut != undefined) {
                   if (this.Activo == 'S') {
                        $('#txtClienteEsFuncionario').val('S');
                        showInfo('Cliente es colaborador, sólo se puede reasignar a ejecutivo Banca Propia');
                    }
                }

            });
        }



        // ---------------------------------------------------------------------
        // Obtener Datos del Ejecutivo
        // ---------------------------------------------------------------------

        function f_BuscarDatosEjecutivoReasignacion() {
            $('#txtNuevoRutEjecutivo').val($('#txtNuevoRutEjecutivo').val().replace(/\./g, ''));
            $('#txtNuevoRutEjecutivo').val($('#txtNuevoRutEjecutivo').val().replace(/\,/g, ''));

            var _ClienteEsFuncionario = $('#txtClienteEsFuncionario').val();
            var _Rut = $('#txtNuevoRutEjecutivo').val();

            var _Digito = DigitoRut(_Rut);
            $('#txtNuevoDVEjecutivo').val(_Digito);

            if (_ClienteEsFuncionario == 'S') {
                f_BuscarDatosEjecutivoBancaPropia();
            }
            else {
                // f_BuscarDatosEjecutivoNormal();
                f_BuscarDatosEjecutivo();
            }

        }


        function f_BuscarDatosEjecutivoNormal() {

            var _Rut = $('#txtNuevoRutEjecutivo').val();

            //async: false,
            $.ajax({
                type: "POST",
                async: false,
                url: "frmIngresoSolicitud.aspx/Get_Funcionario",
                data: '{_Rut: "' + _Rut + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccess_Funcionario,
                failure: function (response) {
                    alert(response.d);
                }
            });

            
        }

        function onSuccess_Funcionario(response) {
            datos = JSON.parse(response.d);
            $(datos).each(function () {
                if (this.NOMBRE == undefined ) {
                    $('#txtNuevoRutEjecutivo').val('')
                    $('#txtNuevoDVEjecutivo').val('')
                    showError('Funcionario no Existe ');
                    return;
                } else {
                    if (this.Activo == "N") {
                        $('#txtNuevoRutEjecutivo').val('')
                        $('#txtNuevoDVEjecutivo').val('')
                        $('#txtNombreReasignacion').val('');
                        $('#txtCanalVentaReasignacion').val('');
                        $('#txtCUI_Reasignacion').val('');
                        showError('Funcionario no Vigente ');
                        return;
                    }
                }
                //$('#txtRutCliente').val(this.RutTitular);
                //OJO   $('#txtNuevoDVEjecutivo').val(this.RutTitular);
                //$('#txtCanalVentaReasignacion').val(this.RutTitular);
                $('#txtNombreReasignacion').val(this.NOMBRE);
                //alert('txtCUI_Reasignacion FUN' + this.CUI);
                $('#txtCUI_Reasignacion').val(this.CUI);
                $('#txtEjecutivoActivo').val(this.Activo);
                //OJO   
                //$('#txtSucursal').val(this.DescVtaSuc);
                //$('#txtZona').val(this.DescVtaZonMod);
                
            });
        }


        // ---------------------------------------------------------------------
        // Obtener Datos de Ejecutivo de Banca Propia
        // ---------------------------------------------------------------------

        function f_BuscarDatosEjecutivoBancaPropia() {


            var _Rut = $('#txtNuevoRutEjecutivo').val();


            $.ajax({
                type: "POST",
                async: false,
                url: "frmIngresoSolicitud.aspx/Get_EjecutivoBancaPropia",
                data: '{_Rut: "' + _Rut + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccess_EjecutivoBancaPropia,
                failure: function (response) {
                    alert(response.d);
                    //alert('Error');
                }
            });
        }

        function onSuccess_EjecutivoBancaPropia(response) {
            datos = JSON.parse(response.d);
            $(datos).each(function () {

           /*     if (this.NOMBRE == undefined) {
                    $('#txtNuevoRutEjecutivo').val('')
                    $('#txtNuevoDVEjecutivo').val('')
                    showError('Ejecutivo no Existe ');
                    return;
                }
                */
                if (this.NRO_SEGURO == '0') {
                    showError('Cliente es colaborador, sólo se puede reasignar a ejecutivo Banca Propia');
                    //$('#txtRutCliente').val(this.RutTitular);
                    $('#txtNuevoDVEjecutivo').val('');
                    $('#txtCanalVentaReasignacion').val('');
                    $('#txtNombreReasignacion').val('');
                    $('#txtCUI_Reasignacion').val('');
                    $('#txtEjecutivoActivo').val('');

                    $('#txtZonaEje').val('');
                    $('#txtSucursalEje').val('');
                    
                    //OJO
                    $('#txtSucursal').val('');
                    $('#txtZona').val('');

                    return;
                }
                //$('#txtRutCliente').val(this.RutTitular);
                $('#txtNuevoDVEjecutivo').val(this.DvRut);
                $('#txtCanalVentaReasignacion').val(this.DesSubCanal);
                $('#txtNombreReasignacion').val(this.NOMBRE);
               // alert('txtCUI_Reasignacion BancaPropia' + this.CUI);
                $('#txtCUI_Reasignacion').val(this.CUI);
                $('#txtEjecutivoActivo').val(this.Activo);

                if ($('#txtExisteEnBVA').val() === 'S') {
                    $('#txtSucursalEje').val(this.DescVtaSuc);
                    $('#txtZonaEje').val(this.DescVtaZonMod);
                } else {
                    $('#txtSucursalEje').val('');
                    $('#txtZonaEje').val('');
                }
                //OJO
                $('#txtSucursal').val(this.DescVtaSuc);
                $('#txtZona').val(this.DescVtaZonMod);

            });
        }



        // ---------------------------------------------------------------------
        // Obtener Datos del Ejecutivo
        // ---------------------------------------------------------------------

        function f_BuscarDatosEjecutivo() {


            var _Rut = $('#txtNuevoRutEjecutivo').val();


            $.ajax({
                type: "POST",
                url: "frmIngresoSolicitud.aspx/Get_Ejecutivo",
                data: '{_Rut: "' + _Rut + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccess_Ejecutivo,
                failure: function (response) {
                    alert(response.d);
                    //alert('Error');
                }
            });
        }

        function onSuccess_Ejecutivo(response) {
            datos = JSON.parse(response.d);
            $(datos).each(function () {
                //$('#txtRutCliente').val(this.RutTitular);
                $('#txtCanalVentaReasignacion').val(this.DesSubCanal);
                if (this.Nombre == undefined) {
                  //  $('#txtNuevoRutEjecutivo').val('')
                  //  $('#txtNuevoDVEjecutivo').val('')
                 //   showError('Ejecutivo no Existe1 ');
                    f_BuscarDatosEjecutivoNormal();
                    return;
                }
                //$('#txtRutCliente').val(this.RutTitular);
                //OJO   $('#txtNuevoDVEjecutivo').val(this.RutTitular);
                //$('#txtCanalVentaReasignacion').val(this.RutTitular);
                $('#txtNombreReasignacion').val(this.Nombre + ' ' + this.ApePat + ' ' + this.ApeMat);
                //alert('txtCUI_Reasignacion FUN' + this.CUI);
                $('#txtCUI_Reasignacion').val(this.CUIRVENT);
                // $('#txtEjecutivoActivo').val(this.Activo);
                if ((this.IdCanalDetalle == 201210) || (this.IdCanalDetalle == 201211) || (this.IdCanalDetalle == 201212) || (this.IdCanalDetalle == 301202) || (this.IdCanalDetalle == 301206) || (this.IdCanalDetalle == 201503) || (this.IdCanalDetalle == 201506) || (this.IdCanalDetalle == 201502)) {
                    $('#txtCUI_Reasignacion').val('');
                    //Desabilitar CUI nuevo
                    document.getElementById("txtCUI_Reasignacion").disabled = true;
                }
                if ($('#txtExisteEnBVA').val() === 'S') {
                    $('#txtSucursalEje').val(this.DescVtaSuc);
                    $('#txtZonaEje').val(this.DescVtaZonMod);
                } else {
                    $('#txtSucursalEje').val('');
                    $('#txtZonaEje').val('');
                }
            });
        }




        // ---------------------------------------------------------------------
        // Permite Ingresar Seguro(valida si existe un seguro en proceso)
        // ---------------------------------------------------------------------
        function f_PermiteIngresarSeguro() {

            var _OrigenSeguro = $("input[name='rbTipoSeguro']:checked").val();
            var _NroSeguro = $('#txtNroSeguro').val();

            $.ajax({
                type: "POST",
                async: false,
                url: "frmIngresoSolicitud.aspx/Get_PermiteIngresarSeguro",
                data: '{_OrigenSeguro: "' + _OrigenSeguro + '" , _NroSeguro : "' + _NroSeguro + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccess_PermiteIngresarSeguro,
                failure: function (response) {
                    alert(response.d);
                    //alert('Error');
                }
            });
        }

        function onSuccess_PermiteIngresarSeguro(response) {
            datos = JSON.parse(response.d);
            $(datos).each(function () {
                //$('#txtRutCliente').val(this.RutTitular);
                if (this.PermiteIngreso == 'N') {
                    $('#txtSeguirValidando').val('N');
                    //showError('Existe un Seguro que esta en Proceso');
                    showError(this.Mensaje);
                } else {
                    if (this.PermiteIngreso == 'U') {
                        $('#txtSeguirValidando').val('S');
                        //showError('Existe un Seguro que esta en Proceso');
                        showInfo(this.Mensaje);
                    }
                }
            });
        }


        // ---------------------------------------------------------------------
        // Motivo de Reasignacion
        // ---------------------------------------------------------------------

        function Get_MotivoReasignacion() {
            $.ajax({
                type: "POST",
                url: "frmIngresoSolicitud.aspx/Get_MotivoReasignacion",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccess_CIA,
                failure: function (response) {
                    alert(response.d);
                    //alert('Error');
                }
            });
        }

        function onSuccess_CIA(response) {
            datos = JSON.parse(response.d);

            $('#cbMotivoReasignacion').append(new Option('Seleccione motivo solicitud ........', '0'));

            $(datos).each(function () {
                var option = $(document.createElement('option'));
                option.text(this.MotivoReasignacion);
                option.val(this.IDMotivoReasignacion);
                $("#cbMotivoReasignacion").append(option);
            });
        }



        function f_Grabar() {

            //var _IDReasignacion = '0';
            //var _OrigenSeguro = $("input[name='rbTipoSeguro']:checked").val();
            //var _NroSeguro = $('#txtNroSeguro').val();
            //var _RutTitular = $('#txtRutCliente').val();
            //var _RutEjecutivo = $('#txtRutEjecutivo').val();
            //var _MtoUF = $('#txtUF').val();
            //var _TipoProducto = $('#txtTipoProducto').val();
            //var _EstadoVenta = $('#txtEstadoVenta').val();
            //var _CanalVenta = $('#txtCanalVenta').val();
            //var _MarcaReferido = $('#txtMarcaReferidos').val();
            //var _CUI = $('#txtCUI').val();
            //var _MesContable = $('#txtMesContable').val();
            //var _AnnoContable = $('#txtAnnoContable').val();
            var _IDMotivoReasignacion = $('#cbMotivoReasignacion').val();
            //var _MotivoReasignacion = $('#cbMotivoReasignacion option:selected').text();
            var _RutEjecutivoReasignacion = $('#txtNuevoRutEjecutivo').val();
            //var _DVEjecutivoReasignacion = $('#txtNuevoDVEjecutivo').val();
            //var _NombreEjecutivoReasignacion = $('#txtNombreReasignacion').val();
            //var _CanalVentaReasignacion = $('#txtCanalVentaReasignacion').val();
            var _CUIReasignacion = $('#txtCUI_Reasignacion').val();
            var _Observacion = $('#txtObservacion').val();
            var _IDUsuario = $('#<%=txtIDUsuario.ClientID%>').val();

            var _IDTipoSolicitud = $('#txtIDTipoSolicitud').val();
            var _FirmaSupervisor = $('#txtSupervisor').val();
            //var _FirmaGerente = $('#txtGerente').val();
            //var _EsInternet = $('#txtEsInternet').val();
            //var _Sucursal = $('#txtSucursal').val();
            //var _Zona = $('#txtZona').val();
            //var _ExisteEnBVA = $('#txtExisteEnBVA').val();

            var _IngresoDocumento = $('#txtIngresoDocumento').val();

            if ($('#txtValidaSeguro').val() == 'S') {
                showError('Debe ingresar número de seguro a buscar');
                return;
            }

            if (_IDMotivoReasignacion == '0') {
                showError('Debe ingresar Motivo');
                return;
            }

            // if (_IDTipoSolicitud != 3 && _IDTipoSolicitud != 7 && _IngresoDocumento == 'N') {
            //Solo si es coordinador

            if ($('#<%=txtIDPerfil.ClientID%>').val() == 2 && _IngresoDocumento == 'N') {
                showError('Debe Ingresar Documento');
                return;
            }

            if (_FirmaSupervisor == '') {
                _FirmaSupervisor = $('#txtSupervisorInternet').val();
            }

            if (_RutEjecutivoReasignacion == '' || _RutEjecutivoReasignacion == '0') {
                showError('Debe Ingresar el Ejecutivo al cual va a reasignar esta solicitud');
                return;
            }

            if ((_CUIReasignacion == '' || _CUIReasignacion == '0') && document.getElementById("txtCUI_Reasignacion").disabled == false){
                showError('Debe Ingresar CUI');
                return;
            }


            if (_Observacion == '') {
                showError('Debe Ingresar observación');
                return;
            }

            var _NombreTipoSolicitud = '';


            if (_IDTipoSolicitud == 1) { _NombreTipoSolicitud = 'Reasignación Normal'; }
            if (_IDTipoSolicitud == 2) { _NombreTipoSolicitud = 'Reasignación Referido'; }
            if (_IDTipoSolicitud == 3) { _NombreTipoSolicitud = 'Reasignación Especial'; }
            if (_IDTipoSolicitud == 4) { _NombreTipoSolicitud = 'Reasignación No Definida'; }
            if (_IDTipoSolicitud == 5) { _NombreTipoSolicitud = 'Reliquidación Normal'; }
            if (_IDTipoSolicitud == 6) { _NombreTipoSolicitud = 'Reliquidación Referido'; }
            if (_IDTipoSolicitud == 7) { _NombreTipoSolicitud = 'Reliquidación Especial'; }
            if (_IDTipoSolicitud == 8) { _NombreTipoSolicitud = 'Reliquidación No Definida'; }

            var _Mensaje = '¿Esta Seguro que desea grabar la Solicitud del tipo : <h3>' + _NombreTipoSolicitud + ' ?</h3>';

            //if (!confirm('¿Esta Seguro que desea grabar la Solicitud del tipo :' + _NombreTipoSolicitud + ' ?')) {
            //    return false;
            //}

            $('#showConfirmacion').modal('show');
            $('#showConfirmacionBody').empty();
            $('#showConfirmacionBody').html(_Mensaje);

        }

        function verPerfil() {

            if ($('#txtEsSupervisorInternet').val() == 'S') {
                f_prefuntaFolio();
            } else {
                f_ConfirmaGrabar();
            }
        }

        function f_prefuntaFolio() {
            
            
            var _Mensaje = '¿Ingresa con Diferente Folio?</h3>';
            $('#showDiferenteFolio').modal('show');
            $('#showDiferenteFolioBody').empty();
            $('#showDiferenteFolioBody').html(_Mensaje);
            
            return
        }

        function f_ConfirmaGrabar() {
            document.getElementById("pbGrabar").disabled = true;
            var _IDReasignacion = '0';
            var _OrigenSeguro = $("input[name='rbTipoSeguro']:checked").val();
            var _NroSeguro = $('#txtNroSeguro').val();
            var _RutTitular = $('#txtRutCliente').val();
            var _RutEjecutivo = $('#txtRutEjecutivo').val();
            var _MtoUF = $('#txtUF').val();
            var _TipoProducto = $('#txtTipoProducto').val();
            var _EstadoVenta = $('#txtEstadoVenta').val();
            var _CanalVenta = $('#txtCanalVenta').val();
            var _MarcaReferido = $('#txtMarcaReferidos').val();
            var _CUI = $('#txtCUI').val();
            var _MesContable = $('#txtMesContable').val();
            var _AnnoContable = $('#txtAnnoContable').val();
            var _IDMotivoReasignacion = $('#cbMotivoReasignacion').val();
            var _MotivoReasignacion = $('#cbMotivoReasignacion option:selected').text();
            var _RutEjecutivoReasignacion = $('#txtNuevoRutEjecutivo').val();
            var _DVEjecutivoReasignacion = $('#txtNuevoDVEjecutivo').val();
            var _NombreEjecutivoReasignacion = $('#txtNombreReasignacion').val();
            var _CanalVentaReasignacion = $('#txtCanalVentaReasignacion').val();
            var _CUIReasignacion = $('#txtCUI_Reasignacion').val();
            var _Observacion = $('#txtObservacion').val();
            var _IDUsuario = $('#<%=txtIDUsuario.ClientID%>').val();
            var _IDTipoSolicitud = $('#txtIDTipoSolicitud').val();
            var _FirmaSupervisor = $('#txtSupervisor').val();
            var _FirmaGerente = $('#txtGerente').val();
            var _EsInternet = $('#txtEsInternet').val();
            var _Sucursal = $('#txtSucursal').val();
            var _Zona = $('#txtZona').val();
            var _ExisteEnBVA = $('#txtExisteEnBVA').val();
            var _IngresoDocumento = $('#txtIngresoDocumento').val();
            var _SucursalEje = $('#txtSucursalEje').val();
            var _ZonaEje = $('#txtZonaEje').val();
            var _txtDiferenteFolioSN = $('#txtDiferenteFolioSN').val();
            var _txtEsSupervisorInternet = $('#txtEsSupervisorInternet').val();
            $.ajax({
                type: "POST",
                url: "frmIngresoSolicitud.aspx/Grabar",
                //data: '{}',
                data: '{_IDReasignacion: "' + _IDReasignacion + '" , _OrigenSeguro : "' + _OrigenSeguro + '" , _NroSeguro : "' + _NroSeguro + '" , _RutTitular : "' + _RutTitular + '" , _RutEjecutivo : "' + _RutEjecutivo + '" , _MtoUF  : "' + _MtoUF + '" , _TipoProducto  : "' + _TipoProducto + '" , _EstadoVenta  : "' + _EstadoVenta + '" , _CanalVenta : "' + _CanalVenta + '" , _MarcaReferido  : "' + _MarcaReferido + '" , _CUI  : "' + _CUI + '" , _MesContable  : "' + _MesContable + '" , _AnnoContable : "' + _AnnoContable + '" , _IDMotivoReasignacion  : "' + _IDMotivoReasignacion + '" , _MotivoReasignacion  : "' + _MotivoReasignacion + '" , _RutEjecutivoReasignacion  : "' + _RutEjecutivoReasignacion + '" , _DVEjecutivoReasignacion : "' + _DVEjecutivoReasignacion + '" , _NombreEjecutivoReasignacion  : "' + _NombreEjecutivoReasignacion + '" , _CanalVentaReasignacion  : "' + _CanalVentaReasignacion + '" , _CUIReasignacion  : "' + _CUIReasignacion + '" , _Observacion : "' + _Observacion + '" , _IDUsuario  : "' + _IDUsuario + '" , _IDTipoSolicitud : "' + _IDTipoSolicitud + '" , _Sucursal : "' + _Sucursal + '" , _Zona : "' + _Zona + '" , _EsInternet : "' + _EsInternet + '" , _ExisteEnBVA : "' + _ExisteEnBVA + '" , _SucursalEje : "' + _SucursalEje + '" , _ZonaEje : "' + _ZonaEje + '" , _txtEsSupervisorInternet: "' + _txtEsSupervisorInternet + '" , _txtDiferenteFolioSN: "' + _txtDiferenteFolioSN + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccess_Grabar,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }


        function onSuccess_Grabar(response) {

            var _IDTipoSolicitud = $('#txtIDTipoSolicitud').val();
            var _NombreTipoSolicitud = '';
            if (_IDTipoSolicitud == 1) { _NombreTipoSolicitud = 'Reasignación Normal'; }
            if (_IDTipoSolicitud == 2) { _NombreTipoSolicitud = 'Reasignación Referido'; }
            if (_IDTipoSolicitud == 3) { _NombreTipoSolicitud = 'Reasignación Especial'; }
            if (_IDTipoSolicitud == 4) { _NombreTipoSolicitud = 'Reasignación No Definida'; }
            if (_IDTipoSolicitud == 5) { _NombreTipoSolicitud = 'Reliquidación Normal'; }
            if (_IDTipoSolicitud == 6) { _NombreTipoSolicitud = 'Reliquidación Referido'; }
            if (_IDTipoSolicitud == 7) { _NombreTipoSolicitud = 'Reliquidación Especial'; }
            if (_IDTipoSolicitud == 8) { _NombreTipoSolicitud = 'Reliquidación No Definida'; }
            datos = JSON.parse(response.d);
            if (datos === null) {
                document.getElementById("pbGrabar").disabled = false;
              showError('ERROR al grabar la Solicitud de Reasignación');
            }

            $(datos).each(function () {
                document.getElementById("pbGrabar").disabled = false;
                if (this.IDReasignacion > 0) {
                    showSuccess('Solicitud de ' + _NombreTipoSolicitud + ' grabada de forma Exitosa');
                    //// ---------------------------------------------------------------------
                    //// Llama a funcion que Genera el PDF para Devolucion Comercial
                    //// ---------------------------------------------------------------------
                    //var _IDTipoDevolucionNormal = $('#cbTipoDevolucion').val();

                    //if (_IDTipoDevolucionNormal == 5) {
                    //    PDF_DevolucionComercial(this.IDDevoluciones);
                    //}
                    $('#txtNroSeguro').val('');
                    f_LimpiarDatos();
                }
                else {
                    showError('ERROR al grabar la Solicitud de Reasignación');
                }
            });
        }


        function AdjuntarDocumentacion() {

            var _NroSeguro = $('#<%=txtNroSeguro_Doc.ClientID%>').val();
            if (_NroSeguro == '' || _NroSeguro == 0) {
                showError('Debe Ingresar nro de seguro antes de adjuntar documentos');
                return;
            }

            if ($('#txtSeguirValidando').val() == 'N') {
                showError('Debe Ingresar nro de seguro habilitado para el ingreso');

                return;
            }

            f_ListadoAdjuntos(_NroSeguro);

            $('#modal_Upload').modal();
        }


        //function f_Adjuntar() {
        //    $('#modal_Upload').modal();


        //}

        function f_ListadoAdjuntos(_NroSeguro) {


            $.ajax({
                type: "POST",
                url: "frmIngresoSolicitud.aspx/Get_ListadoAdjunto",
                data: '{_NroSeguro: "' + _NroSeguro + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccess_ListadoAdjunto,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function onSuccess_ListadoAdjunto(response) {
            lista = JSON.parse(response.d);

            $('#tbl_Adjuntos').bootstrapTable('destroy')
            $('#tbl_Adjuntos').bootstrapTable({
                columns: [
                    { field: 'IDReasignacionAdjunto', title: 'IDReasignacionAdjunto', visible: false }
                    //, { align: 'center', title: 'Descargar Doc', formatter: Col_VerAdjunto }
                    , { field: 'NombreAdjunto', title: 'Nombre Adjunto' }
                    , { field: 'Comentario', title: 'Comentario' }
                    //, { align: 'center', title: 'Eliminar Doc', formatter: Col_EliminarAdjunto }
                ]
                , data: lista
            });

            var _RowsTabla = document.getElementById("tbl_Adjuntos").rows.length;

            //OJO   Revisar
            //if (_RowsTabla != 0) {
            //    $('#pb_UpdateFile').attr("disabled", true);

            //       //showError("Para descargar Excel antes debe haber buscado los registros");
            //       //return false;
            //   }

        }


        function Col_VerAdjunto(value, row, index) {
            return [
                '<button type="button" class="btn btn-default" aria-label="Left Align" data-toggle="tooltip" href="#menu1" title="Eliminar Documento Adjunto" onclick="f_VerAdjunto(\'' + row.IDDevolucionAdjunto + '\');">',
                '<span class="fas fa-file-download fa-lg" aria-hidden="true"></span>',
                '</button>'
            ].join('');
        }

        function Col_EliminarAdjunto(value, row, index) {
            return [
                '<button type="button" class="btn btn-default" aria-label="Left Align" data-toggle="tooltip" href="#menu1" title="Ver Documento Adjunto" onclick="f_EliminarAdjunto(\'' + row.IDDevolucionAdjunto + '\');">',
                '<span class="fas fa-trash fa-lg" aria-hidden="true"></span>',
                '</button>'
            ].join('');
        }

        // ---------------------------------------------------------------------
        // Bajar archivo enviado al Banco
        // ---------------------------------------------------------------------
        function f_VerAdjunto(_IDDevolucionAdjunto) {



            $.ajax({
                type: "POST",
                url: "frmIngresoSolicitud.aspx/Get_Download",
                data: '{_IDDevolucionAdjunto: "' + _IDDevolucionAdjunto + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccess_Download,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }



        function onSuccess_Download(response) {

            datos = JSON.parse(response.d);

            $(datos).each(function () {
                var loc = window.location;
                var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
                var _URL_Sitio = loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
                //var _Archivo = _URL_Sitio + 'Doc/' + this.Archivo;
                var _Archivo = _URL_Sitio + '/Download/' + this.Archivo;
                window.open(_Archivo, '_blank');
            });
        }


        function f_CancelarAdjunto() {
            $('#modal_Upload').modal('hide');
        }


        function subir() {
            var _Comentario = $('#<%=txtComentario.ClientID%>').val();

            if (_Comentario == '') {
                showError('Debe ingresar Descripción del documento que esta subiendo');

                return;
            }

            var i = document.getElementById('fileUp-input');

            if (i.files.length == 0) {
                showError('Debe seleccionar archivo a subir');
                return;
            }

            if (window.FileReader) {
                for (var j = 0; j < i.files.length; j++) {//como mi input file es múltiple, recorro sus elementos (archivos) que pueden ser varios
                    var reader = new FileReader();//instanciamos FileReader

                    reader.onloadend = (function (f) {//creamos la función que recogerá los datos
                        return function (e) {

                            var _archivo = f.name;

                            var _extension = _archivo.split('.').pop();

                            if (_extension != 'msg') {
                                showError('Archivo a subir debe tener extención ".msg"');
                                $('#fileUp-input').val(null);
                                return;
                            }
                            var content = e.target.result.split(",", 2)[1];//obtenemos el contenido del archivo, estará codificado en Base64
                            f_EnviarArchivo(f.name, content);//le paso a una función el nombre del archivo y su contenido. Esta función puede pasar el contenido por ajax u otro medio al servidor
                        }
                    })(i.files[j]);
                    reader.readAsDataURL(i.files[j]);//
                }
            }

            

        }




        function f_EnviarArchivo(_NombreAdjunto, _ArchivoBase64) {


            var _IDReasignacion = '0';
            var _TipoAdjunto = '';
            var _NroSeguro = $('#txtNroSeguro').val();
            var _IDUsuario = $('#<%=txtIDUsuario.ClientID%>').val();
            var _Comentario = $('#<%=txtComentario.ClientID%>').val();

            if (_Comentario == '') {
                showError('Debe ingresar Descripción del documento que esta subiendo');
                return;
            }


            //if (!confirm('¿Esta Seguro que desea grabar la Solicitud de Reasignación?')) {
            //    return false;
            //}

            $.ajax({
                type: "POST",
                url: "frmIngresoSolicitud.aspx/Put_SubirArchivo",
                //data: '{}',
                data: '{_IDReasignacion: "' + _IDReasignacion + '" , _NombreAdjunto : "' + _NombreAdjunto + '" , _TipoAdjunto : "' + _TipoAdjunto + '" , _Comentario : "' + _Comentario + '" , _ArchivoBase64 : "' + _ArchivoBase64 + '" , _IDUsuario  : "' + _IDUsuario + '" , _NroSeguro  : "' + _NroSeguro + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccess_SubirArchivo,
                failure: function (response) {
                    alert(response.d);
                }
            });


        }


        function onSuccess_SubirArchivo(response) {
            var _NroSeguro = $('#txtNroSeguro').val();

            datos = JSON.parse(response.d);
            $(datos).each(function () {
                if (this.Respuesta == 'OK') {
                    $('#txtIngresoDocumento').val('S');
                    showSuccess('Archivo subido de forma Exitosa');
                    f_ListadoAdjuntos(_NroSeguro);
                   // $('#pb_UpdateFile').attr("disabled", true);
                    $('#<%=txtComentario.ClientID%>').val('');
                    $('#fileUp-input').val('');
                    $('#modal_Upload').modal('hide'); //cierra ventana

                }
                else {
                    showError('Error al grabar documento adjunto');
                }
            });
        }

        function f_ValidarCUI() {

            var _CUI = $('#txtCUI_Reasignacion').val();

            $.ajax({
                type: "POST",
                url: "frmIngresoSolicitud.aspx/Get_ValidarCUI",
                //data: '{}',
                data: '{_CUI: "' + _CUI + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccess_ValidarCUI,
                failure: function (response) {
                    alert(response.d);
                }
            });


        }


        function onSuccess_ValidarCUI(response) {

            datos = JSON.parse(response.d);
            $(datos).each(function () {
                if (this.ExisteCUI == 'N') {
                    $('#txtCUI_Reasignacion').val('')
                    showError('Error, el CUI ingresado no existe');
                    $('#pbGrabar').attr("disabled", true);
                }
                else {
                    $('#pbGrabar').attr("disabled", false);
                }
            });
        }


        function f_RestrictInvalidChar() {


            var _txtComentario = $('#txtObservacion');

            _txtComentario.val(_txtComentario.val().replace(/['"°|;]/g, ''));

            _txtComentario.keypress(function (event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                }
            });

        }

        function f_diferenteFolio(valor) {
            $('#txtDiferenteFolioSN').val(valor);
            f_ConfirmaGrabar();
        }


        
    </script>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<asp:TextBox ID="txtIP_Actual"runat="server"  ></asp:TextBox>--%>


    <div class="content content--full">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Ingreso de Solicitud</h3>
            </div>
            <div class="panel-body pb-1 pt-1">
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-primary mb-1">
                            <div class="panel-heading">
                                <h3 class="panel-title">Datos Seguro</h3>
                                
                            </div>
                            <div class="panel-body pb-1 mt-1 pt-1">

                                <div class="row">
                                    
                                    <div class="col-md-2 text-left">
                                        <button type="button" id="bt_buscaTitular" title="Busca Titular" onclick="f_buscarTitular();">
                                            <i class="fas fa-search fa-2x"></i>
                                        </button>
                                    </div>
                                
                                    <div class="col-md-10 custom-control custom-radio text-center">
                                        <label class="pr-4" for="rbTipoSeguro1">
                                            <input type="radio" id="rbTipoSeguro1" name="rbTipoSeguro" onchange="f_LimpiarDatos_TipoSeguro();" value="SCS" checked="checked" />SCS</label>
                                        <label class="pr-4" for="rbTipoSeguro2">
                                            <input type="radio" id="rbTipoSeguro2" name="rbTipoSeguro" onchange="f_LimpiarDatos_TipoSeguro();" value="ISOL" />ISOL</label>
                                        <label class="pr-4" for="rbTipoSeguro3">
                                            <input type="radio" id="rbTipoSeguro3" name="rbTipoSeguro" onchange="f_LimpiarDatos_TipoSeguro();" value="BIGSA" />BIGSA</label>
                                        <label class="pr-4" for="rbTipoSeguro4">
                                            <input type="radio" id="rbTipoSeguro4" name="rbTipoSeguro" onchange="f_LimpiarDatos_TipoSeguro();" value="VIAJE" />VIAJE</label>
                                        <label  for="rbTipoSeguro5">
                                            <input type="radio" id="rbTipoSeguro5" name="rbTipoSeguro" onchange="f_LimpiarDatos_TipoSeguro();" value="RCI" />RCI</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class=" col-md-9 form-group formGroupCss">
                                        <label class="labelbottom" for="exampleFormControlInput1">Número Seguro</label>
                                        <input type="text" class="form-control formControlCss" id="txtNroSeguro" onchange="f_LimpiarDatos();" maxlength="15"/>
                                    </div>


                                    <div class="col-md-3 align-self-center pt-2" >
                                        <input type="button" id="pbBuscar" class="btn btn-primary" value="Buscar" onclick="f_BuscarDatosSeguro();" />

                                    </div>
                                </div>

                                <div class="row">
                                    <div class=" col-md-6 form-group formGroupCss">
                                        <label class="labelbottom" for="exampleFormControlInput1">Zona</label>
                                        <input type="text" class="form-control formControlCss input-sm" id="txtZona" readonly="readonly" />
                                    </div>

                                    <div class=" col-md-6 form-group formGroupCss">
                                        <label class="labelbottom" for="exampleFormControlInput1">Sucursal</label>
                                        <input type="text" class="form-control formControlCss input-sm" id="txtSucursal" readonly="readonly" />
                                    </div>

                                </div>

                                 <div class="row">
                                     <div class=" col-md-6 form-group formGroupCss">
                                        <label class="labelbottom" for="exampleFormControlInput1">Rut Ejecutivo</label>
                                        <input type="text" class="form-control formControlCss input-sm" id="txtRutEjecutivo" readonly="readonly" />
                                    </div>

                                    <div class=" col-md-6 form-group formGroupCss">
                                        <label class="labelbottom" for="exampleFormControlInput1">Nombre Ejecutivo</label>
                                        <input type="text" class="form-control formControlCss input-sm" id="txtNombreEjecutivo" readonly="readonly" />
                                    </div>

                                </div>
                                <div class="row">
                                    <div class=" col-md-6 form-group formGroupCss">
                                        <label class="labelbottom" for="exampleFormControlInput1">Rut Titular</label>
                                        <input type="text" class="form-control formControlCss input-sm" id="txtRutCliente" readonly="readonly" />
                                    </div>

                                    <div class=" col-md-6 form-group formGroupCss">
                                        <label class="labelbottom" for="exampleFormControlInput1">UF</label>
                                        <input type="text" class="form-control formControlCss input-sm" id="txtUF" readonly="readonly" />
                                    </div>

                                </div>

                                <div class="row">
                                    <div class=" col-md-12 form-group formGroupCss">
                                        <label class="labelbottom" for="exampleFormControlInput1">Tipo Producto</label>
                                        <input type="text" class="form-control formControlCss input-sm" id="txtTipoProducto" readonly="readonly" />
                                    </div>

                                </div>

                                <div class="row">
                                    <div class=" col-md-6 form-group formGroupCss">
                                        <label class="labelbottom" for="exampleFormControlInput1">Estado Venta</label>
                                        <input type="text" class="form-control formControlCss input-sm" id="txtEstadoVenta" readonly="readonly" />
                                    </div>

                                    <div class=" col-md-6 form-group formGroupCss">
                                        <label class="labelbottom" for="exampleFormControlInput1">Canal Venta</label>
                                        <input type="text" class="form-control formControlCss input-sm" id="txtCanalVenta" readonly="readonly" />
                                    </div>

                                </div>

                                <div class="row">
                                    <div class=" col-md-6 form-group formGroupCss">
                                        <label class="labelbottom" for="exampleFormControlInput1">CUI</label>
                                        <input type="text" class="form-control formControlCss input-sm" id="txtCUI" readonly="readonly" />
                                    </div>

                                    <div class=" col-md-6 form-group formGroupCss">
                                        <label class="labelbottom" for="exampleFormControlInput1">Marca Referidos</label>
                                        <input type="text" class="form-control formControlCss input-sm" id="txtMarcaReferidos" readonly="readonly" />
                                    </div>

                                </div>

                                <div class="row">
                                    <div class=" col-md-6 form-group formGroupCss">
                                        <label class="labelbottom" for="exampleFormControlInput1">Mes Contable</label>
                                        <input type="text" class="form-control formControlCss input-sm" id="txtMesContable" readonly="readonly" />
                                    </div>

                                    <div class=" col-md-6 form-group formGroupCss">
                                        <label class="labelbottom" for="exampleFormControlInput1">Año Contable</label>
                                        <input type="text" class="form-control formControlCss input-sm" id="txtAnnoContable" readonly="readonly" />
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-primary mb-1">
                            <div class="panel-heading">
                                <h3 class="panel-title">Datos Reasignación</h3>
                            </div>
                            <div class="panel-body pb-1 mt-1 pt-1">

                                <div class="row">
                                    <div class=" col-md-12 form-group formGroupCss">
                                        <label class="labelbottom" for="exampleFormControlSelect1">Motivo Reasignación</label>
                                        <select class="form-control formControlCss input-sm" id="cbMotivoReasignacion"></select>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class=" col-md-6 form-group formGroupCss pr-0">
                                        <label class="labelbottom" for="exampleFormControlInput1">RUT Nuevo Ejecutivo</label>
                                        <input type="text" class="form-control formControlCss input-sm onlyNumbers" id="txtNuevoRutEjecutivo" onchange="f_BuscarDatosEjecutivoReasignacion();" onkeypress="return soloNumeros(event);" maxlength="10" />
                                    </div>
                                    <div class=" align-self-center pt-1">-
                                        </div>
                                    <div class=" col-md-1 form-group formGroupCss align-self-end pl-0">
                                        <input type="text" class="form-control formControlCss input-sm" id="txtNuevoDVEjecutivo" readonly="readonly" />
                                    </div>
                                    
                                 </div>

                                <div class="row">
                                    <div class=" col-md-12 form-group formGroupCss">
                                        <label class="labelbottom" for="exampleFormControlSelect1">Nombre Ejecutivo</label>
                                        <input type="text" class="form-control formControlCss input-sm" id="txtNombreReasignacion" readonly="readonly" />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class=" col-md-6 form-group formGroupCss">
                                        <label class="labelbottom" for="exampleFormControlSelect1">Canal Venta</label>
                                        <input type="text" class="form-control formControlCss input-sm" id="txtCanalVentaReasignacion" readonly="readonly" />
                                    </div>

                                    <div class=" col-md-6 form-group formGroupCss">
                                        <label class="labelbottom" for="exampleFormControlSelect1">CUI</label>
                                        <input type="number" id="txtCUI_Reasignacion" class="form-control formControlCss input-sm onlyNumbers pasteOnlyNumbers" onchange="f_ValidarCUI();"  maxlength="10" />
                                    </div>

                                </div>
                                <div class="row">
                                    <div class=" col-md-6 form-group formGroupCss">
                                        <label class="labelbottom" for="exampleFormControlInput1">Zona</label>
                                        <input type="text" class="form-control formControlCss input-sm" id="txtZonaEje" readonly="readonly" />
                                    </div>

                                    <div class=" col-md-6 form-group formGroupCss">
                                        <label class="labelbottom" for="exampleFormControlInput1">Sucursal</label>
                                        <input type="text" class="form-control formControlCss input-sm" id="txtSucursalEje" readonly="readonly" />
                                    </div>

                                </div>
                                <div class="row">
                                    <div class=" col-md-12 form-group formGroupCss margenInferior">
                                        <label class="labelbottom" for="exampleFormControlSelect1">Observación</label>
                                        <textarea id="txtObservacion" class="form-control input-sm"  maxlength="200" rows="4" onkeydown="f_RestrictInvalidChar();"></textarea>
                                    </div>
                                </div>

                                <div class="row separacion-rows pt-2 ">
                                    <div class="col-md-1">
                                        <button type="button" id="pbAdjuntar_Doc" title="Adjuntar Información" onclick="AdjuntarDocumentacion();">
                                            <i class="fas fa-paperclip fa-2x"></i><%--<img src="../Img/adjuntar.png" width="30" height="30"/>--%>
                                        </button>

                                    </div>
                                    <div class="col-md-11 text-center">
                                        <input type="button" id="pbGrabar" class="btn btn-primary" value="Ingresar Solicitud" onclick="f_Grabar();" />
                                        <%--<input type="button" id="pbImprimir" class="btn btn-primary" value="Imprimir" onclick="f_Buscar??????();" />--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <h4 class="panel-title">LOS SEGUROS SE PODRÁN REASIGNAR UN DÍA HABIL POSTERIOR A SU INGRESO.</h4>
            </div>
        </div>
    </div>



    <div class="modal modal-header-success" role="dialog" tabindex="-1" id="modal_Upload">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h6 class="modal-title text-white">Documentos Relacionado a la Reasignación</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: white;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row separacion-rows">
                        <div class="col-md-2">
                            <label>Descripción</label>
                        </div>
                        <div class="col-md-10">
                            <asp:TextBox ID="txtComentario" CssClass="form-control formControlCss" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group formGroupCss row">
                        <div class="col-md-12"></div>
                    </div>
                    <div class="row separacion-rows">
                        <div class="col-md-2">
                            <label>Adjunto</label>
                        </div>
                        <div class="col-md-10">
                            <input type="file" id="fileUp-input"  accept=".msg" onchange="ValidarImagen(this);" class="form-control-file" <%--onchange="subir()"--%> multiple/><b>Peso máximo de archivo  18MB</b>
                        </div>
                    </div>
                    <div class="row separacion-rows">
                        <div class="col-md-12"></div>
                    </div>
                    <div class="row separacion-rows">
                        <div class="col-md-12"></div>
                    </div>


                    <div class="row">
                        <div class="col-md-8"></div>
                        <div class="col-md-2">
                            <%--<asp:Button ID="pbUpdateFile" class="btn btn-primary form-control"   runat="server"  Text="Subir" />--%>

                            <input type="button" id="pb_UpdateFile" class="btn btn-primary form-control" value="Subir" onclick="subir();" />


                        </div>
                        <div class="col-md-2">

                            <input type="button" id="pbCancelar" class="btn btn-primary form-control" value="Cancelar" onclick="f_CancelarAdjunto();" />
                        </div>
                    </div>

                    <div class="container">
                        <table id="tbl_Adjuntos"
                            class="table table-hover"
                            data-toolbar="#toolbar"
                            data-checkbox-header="true"
                            data-checkbox="true"
                            data-click-to-select="true"
                            data-search="true"
                            data-show-toggle="false"
                            data-show-header="true"
                            data-show-columns="false"
                            data-show-export="true"
                            data-pagination="false"
                            data-page-list="[10, 25, 50, 100, ALL]"
                            data-show-footer="false">
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="showConfirmacion" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-heade bg-info">
                    <div class="row">
                        <div class="col-8 text-center">
                            <h6 id="showConfirmacionTitle" class="modal-title text-white">Grabar Solicitud</h6>
                        </div>
                        <div class="col-2 text-right align-items-center">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: white;">
                                <span aria-hidden="true">X</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <p id="showConfirmacionBody"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="pbConfirmarSI" class="btn btn-primary" onclick="verPerfil();" data-dismiss="modal">SI</button>
                    <button type="button" id="pbConfirmarNO" class="btn btn-primary" data-dismiss="modal">NO</button>
                </div>
            </div>
        </div>
    </div>


    <div id="showDiferenteFolio" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-heade bg-info">
                    <div class="row">
                        <div class="col-8 text-center">
                            <h6 id="showDiferenteFolioTitle" class="modal-title text-white">Graba Supervisor Internet Folio</h6>
                        </div>
                        <div class="col-2 text-right align-items-center">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: white;">
                                <span aria-hidden="true">X</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <p id="showDiferenteFolioBody"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="pbDiferenteFolioSI" class="btn btn-primary" onclick="f_diferenteFolio('S');" data-dismiss="modal">SI</button>
                    <button type="button" id="pbDiferenteFolioNO" class="btn btn-primary" onclick="f_diferenteFolio('N');" data-dismiss="modal">NO</button>
                </div>
            </div>
        </div>
    </div>

    <div id="showBuscarTitular" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-heade bg-info">
                    <div class="row">
                        <div class="col-10 text-center">
                            <h6 id="showBuscarTitularTitle" class="modal-title text-white mb-0">Buscar Titular</h6>
                        </div>
                        <div class="col-2 align-self-center text-center pr-4">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: white;">
                                <span aria-hidden="true">x</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class=" col-md-3 form-group formGroupCss">
                        </div>
                         <div class=" col-md-4 form-group formGroupCss">
                              <label class="labelbottom" for="exampleFormControlInput1">Rut Titular<span style="color:red">(sin guion ni dígito)</span></label>
                              <input type="text" class="form-control formControlCss" id="txtRutTitular" maxlength="10" />
                         </div>

                      
                        <div class=" col-md-2 align-self-center text-center">
                            <input type="button" id="btBuscarTitular" class="btn btn-primary" value="Buscar" onclick="f_llenaTitularBusca();" />
                        </div>
                        <div class=" col-md-3 form-group formGroupCss">
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <div class="col-md-12"  >
                       <div class="container">
                            <table id="tbl_dataTitular"
                                   class="table table-hover"

                                    data-toolbar="#toolbar"
                                    data-search="false"
                                    data-show-toggle="false"
                                    data-show-columns="false"
                                    data-show-export="false"
                                    data-pagination="true"

                                   data-page-list="[10, 25, 50, 100, ALL]"
                                   data-show-footer="false">
                            </table>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-header-success" role="dialog" tabindex="-1" id="modal_DevolucionesSeguro">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-success">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Devoluciones Para el Folio</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group formGroupCss row">
                        <div class="col-md-2">
                            <label>Nro Seguro</label>
                        </div>
                        <div class="col-md-3">
                            <input id="txtNroFolio" class="form-control formControlCss input-sm" readonly type="text" />
                        </div>
                    </div>

                    <div class="form-group formGroupCss row">
                        <table id="tbl_Data"
                            <%--class="table table-bordered table-hover col-xs-12"--%>
                            class="table table-hover"
                            <%--data-click-to-select="true"--%>
                            data-toolbar="#toolbar"
                            data-search="true"
                            data-show-toggle="false"
                            data-show-columns="true"
                            data-show-export="false"
                            data-pagination="false"
                            <%--data-id-field="Id"--%>
                            data-page-list="[10, 25, 50, 100, ALL]"
                            data-show-footer="false">
                        </table>
                    </div>

                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-5"></div>
                        <div class="col-md-2">
                            <%--                        <asp:Button ID="pbNuevaDevolucion" class="btn btn-primary"  runat="server"  Text="Nueva Dev." />--%>
                            <input type="button" id="pbNuevaDevolucion" class="btn btn-primary" value="Nueva Dev." onclick="f_NuevaDevolucion();" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="modal modal-header-success" role="dialog" tabindex="-1" id="CamposOcualtos">
        <%--<input id="txtIDUsuario" class="form-control input-sm"  type="text" />--%>

        <asp:TextBox ID="txtIDUsuario" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtIDPerfil" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtIP_Actual" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtSessionID_Actual" runat="server"></asp:TextBox>

        <asp:TextBox ID="txtIP_Session" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtSessionID_Session" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtUsuario_Session" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtNroSeguro_Doc" runat="server"></asp:TextBox>

        <asp:TextBox ID="txtExisteEnBVA_SN" runat="server"></asp:TextBox>


        <input id="txtNroRutTitular" class="form-control input-sm" value="0" type="text" />
        <input id="txtClienteEsFuncionario" class="form-control input-sm" value="N" type="text" />
        <input id="txtEjecutivoActivo" class="form-control input-sm" value="N" type="text" />
        <input id="txtSeguirValidando" class="form-control input-sm" value="S" type="text" />
        <input id="fechaDiaHabil" class="form-control input-sm" value="S" type="text" />
        <input id="txtValidaSeguro" class="form-control input-sm" value="S" type="text" />
        <input id="txtRamoAceptado" class="form-control input-sm" value="N" type="text" />
        <input id="txtMesCierreVenta" class="form-control input-sm" value="0" type="text" />
        <input id="txtAnnoCierreVenta" class="form-control input-sm" value="0" type="text" />
        <input id="txtMesCalendario" class="form-control input-sm" value="0" type="text" />
        <input id="txtFechaTerminoMes" class="form-control input-sm" value="" type="text" />
        
        <input id="txtFechaCierreVenta" class="form-control input-sm" value="0" type="text" />
        <input id="txtSupervisor" class="form-control input-sm" value="" type="text" />
        <input id="txtSupervisorInternet" class="form-control input-sm" value="" type="text" />
        <input id="txtEsSupervisorInternet" class="form-control input-sm" value="" type="text" />
        <input id="txtGerente" class="form-control input-sm" value="" type="text" />
        <input id="txtEsInternet" class="form-control input-sm" value="" type="text" />
        <input id="txtDiferenteFolioSN" class="form-control input-sm" value="N" type="text" />
        <input id="txtIDTipoSolicitud" class="form-control input-sm" value="0" type="text" />
     <!--   <input id="txtSucursal" class="form-control input-sm" value="" type="text" />-->
     <!--   <input id="txtZona" class="form-control input-sm" value="" type="text" />-->
        <input id="txtExisteEnBVA" value="N" type="text" />
        <input id="txtDiaActual" value="0" type="text" />
        <input id="txtMesAnterior" value="000000" type="text" />
        <input id="txtIngresoDocumento" value="N" type="text" />



    </div>
    <script>
        //Activo
        document.getElementById('rptBotonera_primero_0').style.backgroundColor = "#128ff2";
        document.getElementById('rptBotonera_primero_0').style.borderColor = "#128ff2";
        //Normal

        document.getElementById('rptBotonera_primero_1').style.backgroundColor = "#00438c";
        document.getElementById('rptBotonera_primero_1').style.borderColor = "#356396";

        document.getElementById('rptBotonera_primero_2').style.backgroundColor = "#00438c";
        document.getElementById('rptBotonera_primero_2').style.borderColor = "#356396";

        document.getElementById('rptBotonera_primero_3').style.backgroundColor = "#00438c";
        document.getElementById('rptBotonera_primero_3').style.borderColor = "#356396";

        document.getElementById('navbarDropdown').style.backgroundColor = "#00438c";
        document.getElementById('navbarDropdown').style.borderColor = "#356396";

    </script>
</asp:Content>

