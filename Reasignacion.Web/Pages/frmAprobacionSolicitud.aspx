﻿<%@ Page Language="vb" AutoEventWireup="false"  MasterPageFile="~/Pages/Sitio.Master" CodeBehind="frmAprobacionSolicitud.aspx.vb" Inherits="Reasignacion.Web.frmAprobacionSolicitud" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

  <script type="text/javascript">




      $(document).ready(function () {
          $(document).ajaxStart($.blockUI({ message: '<h1><img src="../Img/loading.gif" /> Procesando...</h1>' })).ajaxStop($.unblockUI);

          Get_Coordinadores();
          Get_TipoSolicitud();
          Get_Reasignaciones();
          
          $.unblockUI();

      });


      function Get_Reasignaciones() {
          var _IDUsuario = $('#<%=txtIDUsuario.ClientID%>').val();
          var _IDReasignacion = $("#txtIDReasignacion").val();
          var _IDCoordinador = $("#cbCoordinador").val();
          var _FechaDesde = $("#txtFechaSolicitudDesde").val();
          var _FechaHasta = $("#txtFechaSolicitudHasta").val();
          var _IDTipoSolicitud = $("#cbTipoSolicitud").val();
          var _RutEjecutivoReasignacion = $("#txtRutNuevoEjecutivo").val();
          var _NroSeguro = $("#txtNroSeguro").val();
          var _enProceso = $("#cbEnProceso").val();

          if (_IDReasignacion == '') {
              _IDReasignacion = 0;
          }
          if (_RutEjecutivoReasignacion == '') {
              _RutEjecutivoReasignacion = 0;
          }

          $.ajax({
              type: "POST",
              url: "frmAprobacionSolicitud.aspx/Get_Reasignaciones",
              data: '{_IDUsuario: "' + _IDUsuario + '", _IDReasignacion: "' + _IDReasignacion + '", _IDCoordinador: "' + _IDCoordinador + '", _FechaDesde: "' + _FechaDesde + '", _FechaHasta: "' + _FechaHasta + '", _IDTipoSolicitud: "' + _IDTipoSolicitud + '", _RutEjecutivoReasignacion: "' + _RutEjecutivoReasignacion + '", _NroSeguro: "' + _NroSeguro + '", _enProceso: "' + _enProceso + '" }',
              contentType: "application/json; charset=utf-8",
              dataType: "json",
              success: onSuccess_Reasignaciones,
              failure: function (response) {
                  alert(response.d);
              }
          });
      }

      function onSuccess_Reasignaciones(response) {

          lista = JSON.parse(response.d);
          $('#tbl_Data').bootstrapTable('destroy')
          var _IDPerfil = $('#<%=txtIDPerfil.ClientID%>').val();

          if (_IDPerfil == 1) {
              $('#tbl_Data').bootstrapTable({
                  columns: [
                      { align: 'center', title: 'Aprobar', formatter: Col_Aprobar }
                      , { align: 'center', title: 'Rechazar', formatter: Col_Rechazar }
                      , { align: 'center', title: 'Ver', formatter: Col_Ver }
                      , { field: 'IDReasignacion', title: 'Nro', visible: true, sortable: true }
                      , { field: 'FechaSolicitud', type: Date, title: 'Fecha', visible: true, sortable: true, sorter: DateSorter, formatter: DateFormat }
                      , { field: 'TipoSolicitud', title: 'Tipo', visible: true, sortable: true }
                      , { field: 'NroSeguro', title: 'Nro Seguro', visible: true, sortable: true }
                      , { field: 'Coordinador', title: 'Solicitante', visible: true }
                      , { field: 'Supervisor', title: 'Supervisor', visible: true }
                      , { field: 'Gerente', title: 'Gerente', visible: true }
                      , { field: 'RutEjecutivoReasignacion', title: 'Rut Ejecutivo Nuevo', visible: true, sortable: true }
                      , { field: 'EnProceso', title: 'En Proceso', visible: true, sortable: true }
                      , { field: 'descMotivo', title: 'Motivo', switchable: false }
                      , { field: 'mismoFolio', title: 'Folio', switchable: false }
                      , { field: 'NroFirma', title: 'NumFirma', switchable: false }
                  ]
                  , data: lista
              });
          }
          else {
              $('#tbl_Data').bootstrapTable({
                  columns: [
                      { align: 'center', title: 'Aprobar', formatter: Col_Aprobar }
                      , { align: 'center', title: 'Rechazar', formatter: Col_Rechazar }
                      , { align: 'center', title: 'Ver', formatter: Col_Ver }
                      , { field: 'IDReasignacion', title: 'Nro', visible: true, sortable: true }
                      , { field: 'FechaSolicitud', type: Date, title: 'Fecha', visible: true, sortable: true, sorter: DateSorter, formatter: DateFormat }
                      , { field: 'TipoSolicitud', title: 'Tipo', visible: true, sortable: true }
                      , { field: 'NroSeguro', title: 'Nro Seguro', visible: true, sortable: true }
                      , { field: 'Coordinador', title: 'Solicitante', visible: true }
                      , { field: 'RutEjecutivoReasignacion', title: 'Rut Ejecutivo Nuevo', visible: true, sortable: true }
                      , { field: 'EnProceso', title: 'En Proceso', visible: true, sortable: true }
                      , { field: 'descMotivo', title: 'Motivo', switchable: false }
                      , { field: 'mismoFolio', title: 'Folio', switchable: false }
                      , { field: 'NroFirma', title: 'NumFirma', switchable: false }
                  ]
                  , data: lista
              });
          }

          
          $('#tbl_Data').bootstrapTable('hideColumn', 'descMotivo');
          $('#tbl_Data').bootstrapTable('hideColumn', 'mismoFolio');
          $('#tbl_Data').bootstrapTable('hideColumn', 'NroFirma');

          function DateSorter(a, b) {
              var d1 = new Date(a);
              var d2 = new Date(b);

              //var a = new Date(moment(a).format('YYYY/MM/DD'));
              //var b = new Date(moment(b).format('YYYY/MM/DD'));
              if (d1 < d2) return -1;
              if (d1 > d2) return 1;
              return 0;
          }

          function DateFormat(value, row, index) {
              return moment(value).format('DD/MM/YYYY');
          }
      }

      function Col_Rechazar(value, row, index) {
          var color_ = "black";

          if ($("#inicio").val() === "2") {
              color_=document.getElementById('R' + row.IDReasignacion).style.color;
          }

          return [
              '<button type="button" class="btn btn-default" aria-label="Left Align" data-toggle="tooltip" href="#menu1" title="Rechazar Solicitud" onclick="f_Rechazar(\'' + index + '\',\'' + row.IDReasignacion + '\', \'' + row.NroFirma + '\');">',
              '<span id="R' + row.IDReasignacion + '" class="fa fa-times-circle fa-lg" style="color: ' + color_ + '\;" aria-hidden="true"></span>',
              '</button>'
          ].join('');

      }

      function Col_Aprobar(value, row, index) {
          var color_ = "black";

          if ($("#inicio").val() === "2") {
              color_ = document.getElementById('A' + row.IDReasignacion).style.color;
          }
          return [
              '<button  type="button" class="btn btn-default" aria-label="Left Align" data-toggle="tooltip" href="#menu1" title="Aprobar Solicitud" onclick="f_Aprobar(\'' + index + '\',\'' + row.IDReasignacion + '\', \'' + row.NroFirma + '\', \'' + row.EsInternet + '\');">',
              '<span id="A' + row.IDReasignacion + '" class="fa fa-check  fa-lg" style="color:' + color_ + '\;"  aria-hidden="true"></span>',
              '</button>'
          ].join('');
      }

      //Fin

      function Col_Ver(value, row, index) {
          return [
              '<button type="button" class="btn btn-default" aria-label="Left Align" data-toggle="tooltip" href="#menu1" title="Aprobar Solicitud" onclick="f_VerResumenReasignacion(\'' + index + '\',\'' + row.IDReasignacion + '\', \'' + row.NroFirma + '\', \'' + row.EsInternet + '\');">',
              '<span class="fa fa-eye fa-lg" aria-hidden="true"></span>',
              '</button>'
          ].join('');
      }

      function f_Aprobar(_index,_IDReasignacion, _NroFirma, _EsInternet) {
          
          if (_IDReasignacion == 'undefined') {
              showError('No existe Registro a Aprobar');
              return;
          }

          $("#index_").val(_index);

          $("#txtIDReasignacion_Tabla").val(_IDReasignacion);
          $("#txtNroFirma").val(_NroFirma);
          $('#<%=txtNroSeguroOtro.ClientID%>').val('');

          $('#<%=txtNroSeguroOtro.ClientID%>').val('N');
          $("#inicio").val('2');
          if (_EsInternet == 'S') {
              if (document.getElementById('A' + _IDReasignacion).style.color === "black" || document.getElementById('A' + _IDReasignacion).style.color === '') {
                  $('#modal_Aprobar_V2').modal();
              } else {
                    document.getElementById('A' + _IDReasignacion).style.color = "black"
                  }
              
              //// return;
              <%--              if (confirm('¿Aprueba con otro Nro de Seguro?')) {
                  //$('#modal_Aprobar').modal();
                  //return;
                  $('#<%=txtNroSeguroOtro.ClientID%>').val('S');
              }--%>
          } else {

              if (document.getElementById('A' + _IDReasignacion).style.color === "black" || document.getElementById('A' + _IDReasignacion).style.color === "") {
                  document.getElementById('A' + _IDReasignacion).style.color = "green"
                  document.getElementById('R' + _IDReasignacion).style.color = "black"
              } else {
                  document.getElementById('A' + _IDReasignacion).style.color = "black"
              }
          }
         // f_ReasignacionAprobar();
      }

      function f_MarcarMismoFolio() {
          var _MismoFolio = $("input[name='rbMismoFolio']:checked").val();
          var _valorMismoFolio = '';

          if (_MismoFolio == 'S') {
              //$('#<%=txtNroSeguroOtro.ClientID%>').val('S');
              _valorMismoFolio = 'S';
          }
          else {
              $('#<%=txtNroSeguroOtro.ClientID%>').val('N');
              _valorMismoFolio = 'N';
          }


          $('#tbl_Data').bootstrapTable('showColumn', 'descMotivo');
          $('#tbl_Data').bootstrapTable('showColumn', 'mismoFolio');
          $('#tbl_Data').bootstrapTable('showColumn', 'NroFirma');

          $("#tbl_Data").bootstrapTable('updateCell', {
              index: $("#index_").val(),
              field: 'mismoFolio',
              value: _valorMismoFolio
          });


          $('#tbl_Data').bootstrapTable('hideColumn', 'descMotivo');
          $('#tbl_Data').bootstrapTable('hideColumn', 'mismoFolio');
          $('#tbl_Data').bootstrapTable('hideColumn', 'NroFirma');


      }

      function f_Rechazar(_index,_IDReasignacion, _NroFirma) {

          if (_IDReasignacion == 'undefined') {
              showError('No existe Registro a Rechazar');
              return;
          }

          $("#index_").val(_index);
          $("#inicio").val('2');
        //  $("#txtMotivoRechazo").val('');
          $("#txtIDReasignacion_Tabla").val(_IDReasignacion);
          $("#txtNroFirma").val(_NroFirma);

          if (document.getElementById('R' + _IDReasignacion).style.color === "black" || document.getElementById('R' + _IDReasignacion).style.color === '') {
              $('#modal_Rechazar').modal();
          } else {
              document.getElementById('R' + _IDReasignacion).style.color = "black"
          }
            


          
      }

      // ---------------------------------------------------------------------
      // Rechazar Reasignacion
      // ---------------------------------------------------------------------

      function f_ReasignacionRechazar() {
          
          var _IDUsuario = $('#<%=txtIDUsuario.ClientID%>').val();
           var _IDReasignacion = $("#txtIDReasignacion_Tabla").val();
           var _NroFirma = $("#txtNroFirma").val();
           var _EsInternet = $("#txtEsInternet").val();
           var _Observacion = $('#<%=txtMotivoRechazo.ClientID%>').val();

          if (_Observacion == '') {
              showError('Debe Ingresar Motivo del Rechazo');
              return;
          }


          $('#tbl_Data').bootstrapTable('showColumn', 'descMotivo');
          $('#tbl_Data').bootstrapTable('showColumn', 'mismoFolio');
          $('#tbl_Data').bootstrapTable('showColumn', 'NroFirma');

          $("#tbl_Data").bootstrapTable('updateCell', {
              index:  $("#index_").val(),
              field: 'descMotivo',
              value: _Observacion              
          });


          $('#tbl_Data').bootstrapTable('hideColumn', 'descMotivo');
          $('#tbl_Data').bootstrapTable('hideColumn', 'mismoFolio');
          $('#tbl_Data').bootstrapTable('hideColumn', 'NroFirma');


          $("#modal_Rechazar").modal('hide');
          
          if (document.getElementById('R' + _IDReasignacion).style.color === "black" || document.getElementById('R' + _IDReasignacion).style.color === '') {
              document.getElementById('R' + _IDReasignacion).style.color = "red"
              document.getElementById('A' + _IDReasignacion).style.color = "black"

          } else {
              document.getElementById('R' + _IDReasignacion).style.color = "black"
          }

        //  $('#tbl_Data').bootstrapTable('refresh')();
          // Grabar en enviar
          //f_ReasignacionRechazarEjecutar(_IDUsuario, _IDReasignacion, _NroFirma, _Observacion);

          //$.ajax({
          //    type: "POST",
          //    url: "frmAprobacionSolicitud.aspx/Upd_ReasignacionRechazo",
          //    data: '{_IDReasignacion: "' + _IDReasignacion + '", _NroFirma: "' + _NroFirma + '", _Observacion: "' + _Observacion + '", _IDUsuario: "' + _IDUsuario + '" }',
          //    contentType: "application/json; charset=utf-8",
          //    dataType: "json",
          //    success: onSuccess_ReasignacionRechazar,
          //    failure: function (response) {
          //        alert(response.d);
          //    }
          //});
      }

      function f_ReasignacionRechazarEjecutar(_IDUsuario, _IDReasignacion, _NroFirma, _Observacion, _IDCoordinador, _FechaDesde, _FechaHasta, _IDTipoSolicitud, _RutEjecutivoReasignacion, _NroSeguro, _largoArreglo, _arregloActual) {

          $.ajax({
              type: "POST",
              url: "frmAprobacionSolicitud.aspx/Upd_ReasignacionRechazo",
              data: '{_IDReasignacion: "' + _IDReasignacion + '", _NroFirma: "' + _NroFirma + '", _Observacion: "' + _Observacion + '", _IDUsuario: "' + _IDUsuario + '", _IDCoordinador: "' + _IDCoordinador + '", _FechaDesde: "' + _FechaDesde + '", _FechaHasta: "' + _FechaHasta + '", _IDTipoSolicitud: "' + _IDTipoSolicitud + '", _RutEjecutivoReasignacion: "' + _RutEjecutivoReasignacion + '", _NroSeguro: "' + _NroSeguro + '", _largoArreglo: "' + largoArreglo + '", _arregloActual: "' + i + '" }',
              contentType: "application/json; charset=utf-8",
              dataType: "json",
              success: onSuccess_ReasignacionRechazar,
              failure: function (response) {
                  alert(response.d);
              }
          });

      }

      function onSuccess_ReasignacionRechazar(response) {

          datos = JSON.parse(response.d);
          $(datos).each(function () {
              if (this.Respuesta == 'OK') {
                 // showSuccess('Se graba observación de rechazo');
                  //   Get_Reasignaciones();
              }
              else {
                  showError('ERROR al grabar observación de rechazo');
              }
          });
      }

      // ---------------------------------------------------------------------
      // Aprobar Reasignacion
      // ---------------------------------------------------------------------

      function f_ReasignacionAprobar() {

          var _IDUsuario = $('#<%=txtIDUsuario.ClientID%>').val();
           var _IDReasignacion = $("#txtIDReasignacion_Tabla").val();
           var _NroFirma = $("#txtNroFirma").val();
           var _NroSeguroOtro = $('#<%=txtNroSeguroOtro.ClientID%>').val();
          $("#modal_Aprobar_V2").modal('hide');

          f_MarcarMismoFolio(); //Guarda folio
          if (document.getElementById('A' + _IDReasignacion).style.color === "black" || document.getElementById('A' + _IDReasignacion).style.color === '') {
              document.getElementById('A' + _IDReasignacion).style.color = "green"
              document.getElementById('R' + _IDReasignacion).style.color = "black"
          } else {
              document.getElementById('A' + _IDReasignacion).style.color = "black"
          }

          // f_ReasignacionAprobarEjecutar(_IDUsuario, _IDReasignacion, _NroFirma, _NroSeguroOtro); ////Momentanoe grabar después

          //$.ajax({
          //    type: "POST",
          //    url: "frmAprobacionSolicitud.aspx/Upd_ReasignacionAprobar",
          //    data: '{_IDReasignacion: "' + _IDReasignacion + '", _NroFirma: "' + _NroFirma + '", _NroSeguroOtro: "' + _NroSeguroOtro + '", _IDUsuario: "' + _IDUsuario + '" }',
          //    contentType: "application/json; charset=utf-8",
          //    dataType: "json",
          //    success: onSuccess_ReasignacionAprobar,
          //    failure: function (response) {
          //        alert(response.d);
          //    }
          //});Upd_ReasignacionAprobar
      }

      function f_ReasignacionAprobarEjecutar(_IDUsuario, _IDReasignacion, _NroFirma, _NroSeguroOtro,_IDCoordinador, _FechaDesde, _FechaHasta, _IDTipoSolicitud, _RutEjecutivoReasignacion, _NroSeguro) {
          $.ajax({

              type: "POST",
              url: "frmAprobacionSolicitud.aspx/Upd_ReasignacionAprobar",
              data: '{_IDReasignacion: "' + _IDReasignacion + '", _NroFirma: "' + _NroFirma + '", _NroSeguroOtro: "' + _NroSeguroOtro + '", _IDUsuario: "' + _IDUsuario + '", _IDCoordinador: "' + _IDCoordinador + '", _FechaDesde: "' + _FechaDesde + '", _FechaHasta: "' + _FechaHasta + '", _IDTipoSolicitud: "' + _IDTipoSolicitud + '", _RutEjecutivoReasignacion: "' + _RutEjecutivoReasignacion + '", _NroSeguro: "' + _NroSeguro + '", _largoArreglo: "' + largoArreglo + '", _arregloActual: "' + i + '" }',
              contentType: "application/json; charset=utf-8",
              dataType: "json",
              success: onSuccess_ReasignacionAprobar,
              failure: function (response) {
                  alert(response.d);
              }
          });
      }

      function onSuccess_ReasignacionAprobar(response) {

          datos = JSON.parse(response.d);
          $(datos).each(function () {
              if (this.Respuesta == 'OK') {
                  
                //  Get_Reasignaciones();
              }
              else {
                  showError('ERROR al marca cambio ');
              }
          });
      }



      // ---------------------------------------------------------------------
      // Motivo de Reasignacion
      // ---------------------------------------------------------------------

      function Get_TipoSolicitud() {
          $.ajax({
              type: "POST",
              url: "frmAprobacionSolicitud.aspx/Get_TipoSolicitud",
              data: '{}',
              contentType: "application/json; charset=utf-8",
              dataType: "json",
              success: onSuccess_TipoSolicitud,
              failure: function (response) {
                  alert(response.d);

              }
          });
      }

      function onSuccess_TipoSolicitud(response) {
          datos = JSON.parse(response.d);
          $(datos).each(function () {
              var option = $(document.createElement('option'));
              option.text(this.Nombre);
              option.val(this.IDTipoSolicitud);
              $("#cbTipoSolicitud").append(option);
          });
      }


      // ---------------------------------------------------------------------
      // Coordinadores
      // ---------------------------------------------------------------------

      function Get_Coordinadores() {
          $.ajax({
              type: "POST",
              url: "frmAprobacionSolicitud.aspx/Get_Coordinadores",
              data: '{}',
              contentType: "application/json; charset=utf-8",
              dataType: "json",
              success: onSuccess_Coordinadores,
              failure: function (response) {
                  alert(response.d);

              }
          });
      }

      function onSuccess_Coordinadores(response) {
          datos = JSON.parse(response.d);
          $(datos).each(function () {
              var option = $(document.createElement('option'));
              option.text(this.NombreCoordinador);
              option.val(this.IDUsuario);
              $("#cbCoordinador").append(option);
          });
      }


      function f_GrabarRechazo() {
          var _IDReasignacion = $("#txtIDReasignacion_Tabla").val();

          f_ReasignacionRechazar();

          //$.ajax({
          //         type: "POST",
          //         url: "frmAprobacionSolicitud.aspx/Get_Coordinadores",
          //         data: '{}',
          //         contentType: "application/json; charset=utf-8",
          //         dataType: "json",
          //         success: onSuccess_Coordinadores,
          //         failure: function (response) {
          //             alert(response.d);
          //             //alert('Error');
          //         }
          //     });
      }

      function onSuccess_Coordinadores(response) {
          datos = JSON.parse(response.d);
          $(datos).each(function () {
              var option = $(document.createElement('option'));
              option.text(this.NombreCoordinador);
              option.val(this.IDUsuario);
              $("#cbCoordinador").append(option);
          });
      }




      function  f_ProcesarReasignaciones() {
          var _IDUsuario = $('#<%=txtIDUsuario.ClientID%>').val();
          var _IDReasignacion = $("#txtIDReasignacion").val();
          var _IDCoordinador = $("#cbCoordinador").val();
          var _FechaDesde = $("#txtFechaSolicitudDesde").val();
          var _FechaHasta = $("#txtFechaSolicitudHasta").val();
          var _IDTipoSolicitud = $("#cbTipoSolicitud").val();
          var _RutEjecutivoReasignacion = $("#txtRutNuevoEjecutivo").val();
          var _cbEnProceso = $("#cbEnProceso").val();
          
          var _NroSeguro = $("#txtNroSeguro").val();
          var _NroSeguro = $("#txtNroSeguro").val();
          var _IDPerfil = $('#<%=txtIDPerfil.ClientID%>').val();

          var _RowsTabla = document.getElementById("tbl_Data").rows.length;
          if (_RowsTabla == 0) {
              showError("Antes de procesar debe haber trabajado con los datos");
              return false;
          }

          if (_IDReasignacion == '') {
              _IDReasignacion = 0;
          }
          if (_RutEjecutivoReasignacion == '') {
              _RutEjecutivoReasignacion = 0;
          }


          $('#tbl_Data').bootstrapTable('showColumn', 'descMotivo');
          $('#tbl_Data').bootstrapTable('showColumn', 'mismoFolio');
          $('#tbl_Data').bootstrapTable('showColumn', 'NroFirma');
          //var Arreglo = new Array();
          var Arreglo=new Array(100)
          Arreglo[0] = new Array(15);

          Arreglo[0][0] = "Aprobar";
          Arreglo[0][1]="Rechazar"
          Arreglo[0][2]="Numero"
          Arreglo[0][3]="Seguro"
          Arreglo[0][4]="Motivo"
          Arreglo[0][5]="Folio"
          Arreglo[0][6] = "NumeroFirma"
          Arreglo[0][7] = "IDCoordinador" //numFirma = field
          Arreglo[0][8] = "FechaDesde" //numFirma = field
          Arreglo[0][9] = "FechaHasta" //numFirma = field
          Arreglo[0][10] = "IDTipoSolicitud" //numFirma = field
          Arreglo[0][11] = "RutEjecutivoReasignacion" //numFirma = field
          Arreglo[0][12] = "EnProceso" //numFirma = field agrego
          Arreglo[0][13] = "NroSeguro" //numFirma = field 12
          Arreglo[0][14] = "AprovaRechazar" //numFirma = field 13
          Arreglo[1] = new Array(15);  //14
          Arreglo[1][0] = "";
          var numCampo = 0;
          var fila = 1;
          //alert('_IDPerfil: ' + _IDPerfil);
          $('#tbl_Data').find("td").each(function (i) {
              var field = $(this).html();
             //// alert(numCampo + ': ' + field);
              if (_IDPerfil == 1) {

                  if (numCampo == 0) {
                      Arreglo[fila][0] = field;//aprobar = field
                  }
                  if (numCampo == 1) {
                      Arreglo[fila][1] = field; //rechazar = field
                  }
                  //i=2 Ver
                  if (numCampo == 3) {
                      Arreglo[fila][2] = field; //numero = field

                  }
                  //i=4  Fecha
                  //i=5  Tipo
                  if (numCampo == 6) {
                      Arreglo[fila][3] = field; //seguro = field
                  }
                  //i=7 Coordinado Solicitante
                  //i=8 Supervisor
                  //i=9 Gerente
                  //i=10 rut Ejecutivo
                  //i=11 En Proceso
                  if (numCampo == 12) { //11
                      Arreglo[fila][4] = field;  //motivo = field
                  }
                  if (numCampo == 13) { //12
                      Arreglo[fila][5] = field;  //folio = field
                  }

                  if (numCampo == 14) { // 13
                      Arreglo[fila][6] = field; //numFirma = field
                      Arreglo[fila][7] = _IDCoordinador; //numFirma = field
                      Arreglo[fila][8] = _FechaDesde; //numFirma = field
                      Arreglo[fila][9] = _FechaHasta; //numFirma = field
                      Arreglo[fila][10] = _IDTipoSolicitud; //numFirma = field
                      Arreglo[fila][11] = _RutEjecutivoReasignacion; //numFirma = field
                      Arreglo[fila][12] = _cbEnProceso; //numFirma = field
                      Arreglo[fila][13] = _NroSeguro; //numFirma = field 13
                      Arreglo[fila][14] = ""; //numFirma = field 14
                  }

                  numCampo = numCampo + 1;
                  if (numCampo == 15) {

                      numCampo = 0;
                      fila = fila + 1;
                      Arreglo[fila] = new Array(14);

                  }
              } else {

                  if (numCampo == 0) {
                      Arreglo[fila][0] = field;//aprobar = field
                  }
                  if (numCampo == 1) {
                      Arreglo[fila][1] = field; //rechazar = field
                  }
                  //i=2 Ver
                  if (numCampo == 3) {
                      Arreglo[fila][2] = field; //numero = field

                  }
                  //i=4  Fecha
                  //i=5  Tipo
                  if (numCampo == 6) {
                      Arreglo[fila][3] = field; //seguro = field
                  }
                  //i=7 Coordinado solicitante
                  //i=8 Ejecutivo rut
                  //i=9 En proceso

                  if (numCampo == 10) {
                      Arreglo[fila][4] = field;  //motivo = field
                  }
                  if (numCampo == 11) {
                      Arreglo[fila][5] = field;  //folio = field
                  }

                  if (numCampo == 12) {
                      Arreglo[fila][6] = field; //numFirma = field
                      Arreglo[fila][7] = _IDCoordinador; //numFirma = field
                      Arreglo[fila][8] = _FechaDesde; //numFirma = field
                      Arreglo[fila][9] = _FechaHasta; //numFirma = field
                      Arreglo[fila][10] = _IDTipoSolicitud; //numFirma = field
                      Arreglo[fila][11] = _RutEjecutivoReasignacion; //numFirma = field
                      Arreglo[fila][12] = _cbEnProceso; //numFirma = field
                      Arreglo[fila][13] = _NroSeguro; //numFirma = field 13
                      Arreglo[fila][14] = ""; //numFirma = field 14
                    
                  }

                  numCampo = numCampo + 1;
                  if (numCampo == 13) {

                      numCampo = 0;
                      fila = fila + 1;
                      Arreglo[fila] = new Array(15);
                      Arreglo[fila][0] = "";
                  }
              }
          });
          var largoArreglo = 0;
          for (var i = 1; i < fila ; i++) {
              if (Arreglo[i][0] != "") {
                  _IDReasignacion = Arreglo[i][2]
                  _Observacion = Arreglo[i][4]
                  _NroFirma = Arreglo[i][6]
                  _NroSeguroOtro = Arreglo[i][5]

                  largoArreglo = largoArreglo + 1;
            
                  // alert('muestra');
                  // alert(Arreglo[i][2]);
                  // alert(Arreglo[i][4]);
                  //  alert(Arreglo[i][6]);
                  //  alert(Arreglo[i][5]);
                  //  alert('fin');
                  /*
                                $.ajax({
                                    type: "POST",
                                    async: false,
                                    url: "frmAprobacionSolicitud.aspx/Get_buscaReasignacion",
                                    data: '{_IDReasignacion: ' + _IDReasignacion + ' }',
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    success: onSuccess_buscaReasignacion,
                                    failure: function (response) {
                                        alert(response.d);
                                    }
                                });
                  
                                */


                  //Graba Aprobadas
                  if (document.getElementById('A' + _IDReasignacion).style.color === "green") {
                  //    alert('paso aprobar');
                  //    alert('_IDReasignacion: ' + _IDReasignacion);
                  //    alert('_NroFirma: ' + _NroFirma);
                  //    alert('_NroSeguroOtro: ' + _NroSeguroOtro);
                  //    alert('_IDUsuario: ' + _IDUsuario);
                     // for (var ii = 1; ii < 2 ; ii++) {  a = a + 2; }
                      Arreglo[fila][14] = "Aprobar"; //numFirma = field 14
                      // f_ReasignacionAprobarEjecutar(_IDUsuario, _IDReasignacion, _NroFirma, _NroSeguroOtro, _IDCoordinador, _FechaDesde, _FechaHasta, _IDTipoSolicitud, _RutEjecutivoReasignacion, _NroSeguro, _largoArreglo, _arregloActual)
                      $.ajax({

                          type: "POST",
                          url: "frmAprobacionSolicitud.aspx/Upd_ReasignacionAprobar",
                          data: '{_IDReasignacion: "' + _IDReasignacion + '", _NroFirma: "' + _NroFirma + '", _NroSeguroOtro: "' + _NroSeguroOtro + '", _IDUsuario: "' + _IDUsuario + '" }',
                          contentType: "application/json; charset=utf-8",
                          dataType: "json",
                          async: false,
                          success: onSuccess_ReasignacionAprobar,
                          failure: function (response) {
                              alert(response.d);
                          }
                      });
                  }
                  //Graba REchazadas
                  if (document.getElementById('R' + _IDReasignacion).style.color === "red") {
                   //   lert('paso rechazar');
                      //alert('prueba supervisor R')
                   //   for (var ii = 1; ii < 2 ; ii++) {  a = a + 2; }
                      Arreglo[fila][14] = "Rechazar"; //numFirma = field 14
                      //f_ReasignacionRechazarEjecutar(_IDUsuario, _IDReasignacion, _NroFirma, _Observacion,_IDCoordinador, _FechaDesde, _FechaHasta, _IDTipoSolicitud, _RutEjecutivoReasignacion, _NroSeguro,_largoArreglo, _arregloActual)
                      $.ajax({
                          type: "POST",
                          url: "frmAprobacionSolicitud.aspx/Upd_ReasignacionRechazo",
                          data: '{_IDReasignacion: "' + _IDReasignacion + '", _NroFirma: "' + _NroFirma + '", _Observacion: "' + _Observacion + '", _IDUsuario: "' + _IDUsuario + '" }',
                          contentType: "application/json; charset=utf-8",
                          dataType: "json",
                          async: false,
                          success: onSuccess_ReasignacionRechazar,
                          failure: function (response) {
                              alert(response.d);
                          }
                      });
                  }


                  //alert('salio');
                  //f_ReasignacionAprobarEjecutar(_IDUsuario, _IDReasignacion, _NroFirma, _NroSeguroOtro);

                  //Guarda Envíos

              }
          }
          // alert(Arreglo.length);
          var a = 0;
          for (var i = 1; i < fila ; i++) {
              if (Arreglo[i][0] != "") {
                  _IDReasignacion = Arreglo[i][2]
                  _Observacion = Arreglo[i][4]
                  _NroFirma = Arreglo[i][6]
                  _NroSeguroOtro = Arreglo[i][5]
               ////   alert('paso 3');
               ////   alert('_IDUsuario: ' + _IDUsuario);
               ////   alert('_IDReasignacion: ' + _IDReasignacion);
               ////   alert('_IDCoordinador:  ' + _IDCoordinador);
               ////   alert('_FechaDesde: ' + _FechaDesde);
               ////   alert('_FechaHasta: ' + _FechaHasta);
               ////   alert('_IDTipoSolicitud: ' + _IDTipoSolicitud);
               ////   alert('_RutEjecutivoReasignacion: ' + _RutEjecutivoReasignacion);
               ////   alert('_NroSeguro: ' + _NroSeguro);
               ////   alert('_largoArreglo: "' + largoArreglo);
               ////   alert('_arregloActual: ' + i);
                 // for (var ii = 1; ii < 1000 ; ii++) { console.log('1'); a = a + 2; }
                  //Guarda Envíos
                  if (document.getElementById('A' + _IDReasignacion).style.color === "green" || document.getElementById('R' + _IDReasignacion).style.color === "red") {
                      //    if(1==2){
                      // alert('entro envio');
                     
                      $.ajax({
                          type: "POST",
                          url: "frmAprobacionSolicitud.aspx/Get_ReasignacionesActualizar",
                       //   data: JSON.stringify(Arreglo),
                          data: '{_IDUsuario: "' + _IDUsuario + '", _IDReasignacion: "' + _IDReasignacion + '", _IDCoordinador: "' + _IDCoordinador + '", _FechaDesde: "' + _FechaDesde + '", _FechaHasta: "' + _FechaHasta + '", _IDTipoSolicitud: "' + _IDTipoSolicitud + '", _RutEjecutivoReasignacion: "' + _RutEjecutivoReasignacion + '", _NroSeguro: "' + _NroSeguro + '", _largoArreglo: "' + largoArreglo + '", _arregloActual: "' + i + '" }',
                          //data: '{_IDUsuario: "' + _IDUsuario + '", _IDReasignacion: "' + _IDReasignacion + '", _IDCoordinador: "' + _IDCoordinador + '", _FechaDesde: "' + _FechaDesde + '", _FechaHasta: "' + _FechaHasta + '", _IDTipoSolicitud: "' + _IDTipoSolicitud + '", _RutEjecutivoReasignacion: "' + _RutEjecutivoReasignacion + '", _NroSeguro: "' + _NroSeguro + '" }',
                          contentType: "application/json; charset=utf-8",
                          dataType: "json",
                          async: false,
                          success: onSuccess_ReasignacionesActualizar,
                          failure: function (response) {
                                 alert(response.d);
                          }
                      });
                  }
              }
          }

         // for (var ii = 1; ii < 10000 ; ii++) { console.log('2'); a = a + 2; }
          showSuccess('Reasignaciones procesadas de forma Exitosa');
          $("#inicio").val('1');
          $('#tbl_Data').bootstrapTable('hideColumn', 'descMotivo');
          $('#tbl_Data').bootstrapTable('hideColumn', 'mismoFolio');
          $('#tbl_Data').bootstrapTable('hideColumn', 'NroFirma');
          Get_Reasignaciones();

         

      }

      function onSuccess_ReasignacionesActualizar(response) {

          datos = JSON.parse(response.d);
          $(datos).each(function () {
              if (this.Respuesta == 'OK') {
                 // alert(response.d);
              } else {
                  showError('ERROR al procesar las Reasignaciones');
              }
          });
      }

      function onSuccess_buscaReasignacion(response) {

          datos = JSON.parse(response.d);
          $(datos).each(function () {
              if (this.Respuesta == 'OK') {
                  showSuccess('Reasignaciones procesadas de forma Exitosa');
                  Get_Reasignaciones();
              }
              else {
                  showError('ERROR al procesar las Reasignaciones');
              }
          });
      }

      // ---------------------------------------------------------------------
      // Ver Resumen de Reasignación
      // ---------------------------------------------------------------------
      function f_VerResumenReasignacion(_index,_IDReasignacion, _NroFirma, _EsInternet) {

          if (_IDReasignacion == 'undefined') {
              showError('No existe Registro a visualizar');
              return;
          }

          $("#index_").val(_index);

          $("#txtIDReasignacion_Tabla").val(_IDReasignacion);
          $("#txtNroFirma").val(_NroFirma);
          $("#txtEsInternet").val(_EsInternet);

          $('#modal_Resumen').modal();


          var _IDUsuario = $('#<%=txtIDUsuario.ClientID%>').val();
          //           var _IDReasignacion = $("#txtIDReasignacion_Tabla").val();


          $.ajax({
              type: "POST",
              url: "frmAprobacionSolicitud.aspx/Get_ReasignacionResumen",
              data: '{_IDReasignacion: "' + _IDReasignacion + '", _IDUsuario: "' + _IDUsuario + '" }',
              contentType: "application/json; charset=utf-8",
              dataType: "json",
              success: onSuccess_ReasignacionResumen,
              failure: function (response) {
                  alert(response.d);
              }
          });
      }

      function onSuccess_ReasignacionResumen(response) {

           datos = JSON.parse(response.d);
           $(datos).each(function () {

               var _IDPerfil = $('#<%=txtIDPerfil.ClientID%>').val();

               $("#txtFechaAprobacion").val(this.FechaAprobacion);
               //Solicitud pendiente
               if (this.IDEstadoAprobacion == 1) {
                   $("#txtFechaAprobacion").val('');
               }
              // $("#txtFechaAprobacion").val(this.FechaAprobacion);
               $("#txtFechaGestion").val(this.FechaGestion);
               $("#txtRutEjecutivoActual").val(this.RutEjecutivoActual);
               $("#txtMesAnnoContable").val(this.MesAnnoContable);
               $("#txtZona").val(this.Zona);
               $("#txtSucursal").val(this.Sucursal);
               //$("#txtMotivoRechazo").val(this.MotivoRechazo);
               $("#cbTipoSolicitudResumen").val(this.TipoSolicitud);
               $("#cbEstadoAprobacionResumen").val(this.EstadoAprobacion);
               $("#cbSupervisor").val(this.Supervisor);
               $("#txtIDSupervisorOriginal").val(this.IDSupervisor);
               $("#cbGerente").val(this.Gerente);
               $("#txtIDGerenteOriginal").val(this.IDGerente);
               $("#txtNroSeguroOriginal").val(this.NroSeguro);
               $("#txtNroSeguroReasignado").val(this.NroSeguroOtro);
               $("#txtObservacion").val(this.Observacion);

               $("#txtMarcaReferidos").val(this.MarcaReferido);
               $("#txtMotivoReasignacion").val(this.MotivoReasignacion);
               $("#txtRutNuevoEjecutivo_Resumen").val(this.RutEjecutivoReasignacion);
               $("#txtNombreNuevoEjecutivo").val(this.NombreEjecutivoReasignacion);
               $("#txtNroSolicitudDetalle").val(this.IDReasignacion);
               $("#txtFechaSolicitudDetalle").val(this.FechaSolicitud);
               $("#txtCoordinadorDetalle").val(this.Coordinador);
               
               $("#txtMotivoRechazo_Resumen").val(this.MotivoRechazo);

               if (this.EnProceso === 'Si') {
                   $("#chkEnProceso").prop("checked", true);
               } else {
                    $("#chkEnProceso").prop("checked", false);
               }
          });
      }


      function f_GrabaEnProceso(_enProceso) {
          if ($("#txtIDReasignacion_Tabla").val() == 'undefined') {
              showError('No existe Registro a visualizar');
              return;
          }

          var _IDReasignacion = $("#txtIDReasignacion_Tabla").val()

          var _IDUsuario = $('#<%=txtIDUsuario.ClientID%>').val();
          //           var _IDReasignacion = $("#txtIDReasignacion_Tabla").val();

          $.ajax({
              type: "POST",
              url: "frmAprobacionSolicitud.aspx/Put_grabaEnProceso",
              data: '{_IDReasignacion: "' + _IDReasignacion + '", _IDUsuario: "' + _IDUsuario + '", _enProceso: "' + _enProceso + '" }',
              contentType: "application/json; charset=utf-8",
              dataType: "json",
              success: onSuccess_grabaEnProceso,
              failure: function (response) {
                  alert(response.d);
              }
          });
      }

      function onSuccess_grabaEnProceso(response) {


          datos = JSON.parse(response.d);
          if (datos === null) {
              showError('ERROR al grabar la Estado en Solicitud de Reasignación');
          }

          $(datos).each(function () {
              if (this.IDReasignacion <= 0) {
                  showError('ERROR al grabar la Solicitud de Reasignación');
              }
          });
      }

      function f_RechazarResumen() {

          var _IDUsuario = $('#<%=txtIDUsuario.ClientID%>').val();
          var _IDReasignacion = $("#txtIDReasignacion_Tabla").val();
          var _NroFirma = $("#txtNroFirma").val();
          var _EsInternet = $("#txtEsInternet").val();
          var _Observacion = $('#txtMotivoRechazo_Resumen').val();
          var _NroSeguroOtro = $('#txtNroSeguroOtro_Resumen').val();

        //  if (_Observacion == '') {
           //   showError('Debe Ingresar Motivo del Rechazo');
          //    $("#txtMotivoRechazo_Resumen").focus();
          //    return;
          //}
     //     alert('f_rechazarresumen');
         
          $("#modal_Resumen").modal('hide');
         
          f_Rechazar($("#index_").val(), _IDReasignacion, _NroFirma);
         // $('#tbl_Data').bootstrapTable('refresh')();
         
       /*   if (document.getElementById('R' + _IDReasignacion).style.color === "black") {
              document.getElementById('R' + _IDReasignacion).style.color = "red"
              document.getElementById('A' + _IDReasignacion).style.color = "black"
          } else {
              document.getElementById('R' + _IDReasignacion).style.color = "black"
          }
          */
          //Esta función se debe implementar cuando se envie
          //f_ReasignacionRechazarEjecutar(_IDUsuario, _IDReasignacion, _NroFirma, _Observacion);

      }

      function f_AprobarResumen() {

          var _IDUsuario = $('#<%=txtIDUsuario.ClientID%>').val();
          var _IDReasignacion = $("#txtIDReasignacion_Tabla").val();
          var _NroFirma = $("#txtNroFirma").val();
          var _Observacion = $('#txtMotivoRechazo_Resumen').val();
          var _NroSeguroOtro = $('#txtNroSeguroOtro_Resumen').val();
          var _EsInternet = $('#txtEsInternet').val();

          $("#modal_Resumen").modal('hide');

          f_Aprobar($("#index_").val(), _IDReasignacion, _NroFirma, _EsInternet);
         //// if (_EsInternet == 'S') {
         ////     $('#<%=txtNroSeguroOtro.ClientID%>').val('N');
         ////     if (confirm('¿Aprueba con otro Nro de Seguro?')) {
         ////          $('#<%=txtNroSeguroOtro.ClientID%>').val('S');
         ///     }
         //// }

          //// f_ReasignacionAprobarEjecutar(_IDUsuario, _IDReasignacion, _NroFirma, _NroSeguroOtro); ////Momentanoe grabar después

      }


       // ---------------------------------------------------------------------
       // Bajar archivo enviado al Banco
       // ---------------------------------------------------------------------
       function f_VerAdjunto() {

           
           var _IDReasignacion = $("#txtIDReasignacion_Tabla").val();
           var _NroSeguro = '';


           $.ajax({
               type: "POST",
               url: "frmEstadoSolicitud.aspx/Get_Download",
               data: '{_IDReasignacion: "' + _IDReasignacion + '", _NroSeguro: "' + _NroSeguro + '" }',
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               success: onSuccess_Download,
               failure: function (response) {
                   alert(response.d);
               }
           });
       }

       function onSuccess_Download(response) {

           datos = JSON.parse(response.d);

           $(datos).each(function () {
               if (this.Archivo == 'DB_Error') {
                   showError('No tiene documentos adjunto');
                   return;
               }
               var loc = window.location;
               var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
               var _URL_Sitio = loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - (pathName.length - 7)));
               //alert(loc);
               //alert(pathName);
               //alert(_URL_Sitio);
               var _Archivo = _URL_Sitio + '/Download/' + this.Archivo;
    
               window.open(_Archivo, '_blank');
           });
       }

      function f_OcultarAprobar() {
          $("#modal_Aprobar_V2").modal('hide');
      }

      function f_enProceso(cb) {
          f_GrabaEnProceso(cb.checked);
          //alert('Clicked, new value = ' + cb.checked);
      }

  </script>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="content content--full">
    <div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Gestión Solicitudes</h3>

    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-primary">
                    <div class="panel-body">

                        <div class="panel-group">
                          <div class="panel panel-default">
                            <div class="panel-heading">
                              <h4 class="panel-title">
                                <a data-toggle="collapse" class="collapsed" href="#collapse1">Filtros <i class="fa"></i></a>
                              </h4>
                            </div>
                            <div id="collapse1" class="panel-body collapse">


                        <div class="row">
                            <div class="col-md-12">

                                <div class="row">
                                    <div class=" col-md-6 form-group">
                                        <label for="exampleFormControlSelect1">Nro Solicitud</label>
                                        <input id="txtIDReasignacion" class="form-control input-sm"  type="text" tabindex='1'   maxlength="15"/>
                                    </div>

                                    <div class=" col-md-6 form-group">
                                        <label for="exampleFormControlSelect1">Solicitante</label>
                                        <select class="form-control input-sm" id="cbCoordinador" tabindex="4"></select>
                                    </div>
                                </div>
                                 
                                <div class="row">
                                    <div class=" col-md-6 form-group">
                                        <label for="exampleFormControlSelect1">Tipo Solicitud</label>
                                        <select class="form-control input-sm" id="cbTipoSolicitud" tabindex='2'></select>
                                    </div>

                                    <div class=" col-md-6 form-group">
                                        <label for="exampleFormControlSelect1">Rut Nuevo Ejecutivo</label>
                                        <input id="txtRutNuevoEjecutivo" class="form-control input-sm"  type="text"   maxlength="15" tabindex="5"/>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class=" col-md-6 form-group">
                                        <label for="exampleFormControlSelect1">Nro Seguro</label>
                                        <input id="txtNroSeguro" class="form-control input-sm"  type="text"  tabindex="3" maxlength="15"/>
                                    </div>

                                    <div class="col-md-1 form-group align-self-center">
                                            <label>Fecha Solicitud</label>
                                     </div>
                                    <div class=" col-md-2 form-group">
                                        <label for="exampleFormControlSelect1">Desde</label>
                                        <input id="txtFechaSolicitudDesde" class="form-control input-sm"  type="date"  tabindex="6"/>
                                    </div>

                                    <div class=" col-md-2 form-group">
                                        <label for="exampleFormControlSelect1">Hasta</label>
                                        <input id="txtFechaSolicitudHasta" class="form-control input-sm"  type="date" tabindex="7" />
                                    </div>
                                     <div class="col-md-1 form-group">
                                     </div>  
                                </div>

                                <div class="row">
                                    <div class=" col-md-6 form-group">
                                            <label for="exampleFormControlSelect1">En proceso</label>
                                            <select class="form-control input-sm" id="cbEnProceso" tabindex="5">
                                                <option value="T">Todos</option> 
                                                <option value="True">Si</option> 
                                                <option value="False">No</option>
                                            </select>
                                        </div>

                                    <div class="col-md-6"></div>
                                </div>
                                <div class="row">
                                     <div class="col-md-4 offset-md-4">
                                          <button class="btn btn-primary form-control" onclick="Get_Reasignaciones();" type="button" id="btn_Buscar" tabindex="8">Filtrar</button>
                                     </div>
                                 </div>

                                </div>      

                            </div>
                        </div>
                            </div>
                          </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12"  >

                                <div class="container">
                                    <table id="tbl_Data"

                                    class="table table-hover"

                                    data-toolbar="#toolbar"
                                    data-search="true"
                                    data-show-toggle="false"
                                    data-show-columns="true"
                                    data-show-export="true"
                                    data-pagination="true"

                                    data-page-list="[10, 25, 50, 100, ALL]"
                                    data-show-footer="false">
                                    </table>
                                </div>

                                <div class="row separacion-rows">

                                        <div class="col-md-4 offset-md-4">
                                            <button class="btn btn-primary form-control" onclick="f_ProcesarReasignaciones();" type="button" id="btn_Procesar">Enviar Respuestas</button>
                                        </div>
                                    </div>
                </div>
</div>








                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
<%--</div>--%>

<!--
    MODAL
-->

<div class="modal modal-header-success" role="dialog" tabindex="-1" id="modal_Resumen">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h6 class="modal-title text-white">Resumen Solicitud</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: white;">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">


                <div class="row separacion-rows">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4">
                               <label>Rut Ej. Actual</label>
                            </div>
                            <div class="col-md-4">
                                <input id="txtRutEjecutivoActual" class="form-control input-sm" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                               <label>Marca Referidos</label>
                            </div>
                            <div class="col-md-8">
                                <input id="txtMarcaReferidos" class="form-control input-sm" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                               <label>Motivo Reasignación</label>
                            </div>
                            <div class="col-md-8">
                                <input id="txtMotivoReasignacion" class="form-control input-sm" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                               <label>Nuevo RUT Ejecutivo </label>
                            </div>
                            <div class="col-md-4">
                                <input id="txtRutNuevoEjecutivo_Resumen" class="form-control input-sm" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                               <label>Nombre Ejecutivo</label>
                            </div>
                            <div class="col-md-8">
                                <input id="txtNombreNuevoEjecutivo" class="form-control input-sm" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Zona</label>
                            </div>
                            <div class="col-md-8">
                                <input id="txtZona" class="form-control input-sm" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Nro Solicitud</label>
                            </div>
                            <div class="col-md-4">
                                <input id="txtNroSolicitudDetalle" class="form-control input-sm" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Fecha Solicitud</label>
                            </div>
                            <div class="col-md-4">
                                <input id="txtFechaSolicitudDetalle" class="form-control input-sm" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Tipo Solicitud</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group ">
                                <%--<select id="cbTipoSolicitudResumen" class="input-sm form-control"  disabled="disabled" ></select>--%>
                                    <input id="cbTipoSolicitudResumen" class="form-control input-sm" disabled="disabled" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Supervisor</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group ">
                                    <input id="cbSupervisor" class="form-control input-sm" disabled="disabled" />
                                <%--<select id="cbSupervisor" class="input-sm form-control"  disabled="disabled" ></select>--%>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Observación</label>
                            </div>
                            <div class="col-md-8">
                                <textarea id="txtObservacion"  disabled="disabled"  class="form-control input-sm"  maxlength="200" rows="4" onkeydown="f_RestrictInvalidChar();"></textarea>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Fecha Gestión</label>
                            </div>
                            <div class="col-md-4">
                                <input id="txtFechaGestion" class="form-control input-sm" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Mes-Año contable</label>
                            </div>
                            <div class="col-md-4">
                                <input id="txtMesAnnoContable" class="form-control input-sm" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Sucursal</label>
                            </div>
                            <div class="col-md-8">
                                <input id="txtSucursal" class="form-control input-sm" disabled="disabled" />
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Solicitante</label>
                            </div>
                            <div class="col-md-8">
                                <input id="txtCoordinadorDetalle" class="form-control input-sm" disabled="disabled" />
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label>Adjunto</label>
                            </div>
                            <div class="col-md-4">
                                        <button type="button"  title="Información Adjunta" onclick="f_VerAdjunto();">
                                        <i class="fas fa-paperclip fa-2x"></i>
                                        </button>
                    
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Motivo Rechazo</label>
                            </div>
                            <div class="col-md-8">
                                <input id="txtMotivoRechazo_Resumen" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Fecha Aprobación</label>
                            </div>
                            <div class="col-md-4">
                                <input id="txtFechaAprobacion" class="form-control input-sm" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Estado Aprobación</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group ">
                                    <input id="cbEstadoAprobacionResumen" class="form-control input-sm" disabled="disabled" />
                                <%--<select id="cbEstadoAprobacionResumen" class="input-sm form-control"  disabled="disabled" ></select>--%>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Gerente</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group ">
                                    <input id="cbGerente" class="form-control input-sm" disabled="disabled"  />
                                <%--<select id="cbGerente" class="input-sm form-control"  disabled="disabled" ></select>--%>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Nro Seguro Reasignado</label>
                            </div>
                            <div class="col-md-4">
                                <input id="txtNroSeguroReasignado"  disabled="disabled"  class="form-control input-sm"  type="text" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Nro Seguro</label>
                            </div>
                            <div class="col-md-4">
                                <input id="txtNroSeguroOriginal"  disabled="disabled"  class="form-control input-sm"  type="text" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>En proceso</label>
                            </div>
                            <div class="col-md-4">
                                <input type="checkbox" id="chkEnProceso" value="1" class="form-control input-sm" onclick='f_enProceso(this);'/>
                            </div>
                        </div>
                    </div>

                </div>



                <div class="row separacion-rows">
                    <div class="col-md-12"></div>
                </div>
                <div class="row separacion-rows">
                    <div class="col-md-12"></div>
                </div>
                <div class="row separacion-rows">
                    <div class="col-md-12"></div>
                </div>

                
                <div class="row">
                    <div class="col-md-8"></div>
                    <div class="col-md-2">
                        <button id="pbRechazarResumen" onclick="f_RechazarResumen();" class="btn btn-danger" type="button">Rechazar</button>
                        <%--<asp:Button ID="Button1" class="btn btn-primary form-control"   runat="server"  Text="Subir" />--%>
                        <%--<button class="btn btn-primary form-control" onclick="f_Grabar();" type="button" id="btn_Buscar">Grabar</button>--%>
                    </div>
                    <div class="col-md-2">
                        <%----<asp:Button ID="Button2" class="btn btn-primary form-control"  runat="server"  Text="Cancelar" />--%>
                        <button id="pbAprobar_Resumen" onclick="f_AprobarResumen();" class="btn btn-success" type="button">Aprobar</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



<div class="modal modal-header-success" role="dialog" tabindex="-1" id="CamposOcualtos">
    <asp:TextBox ID="txtNroSolicitudBCO" runat="server" ></asp:TextBox>
    <asp:TextBox ID="txtUsuario_Session" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtIDPerfil" runat="server"></asp:TextBox>

     <asp:TextBox ID="txtIDUsuario" runat="server" ></asp:TextBox>


    <input id="txtIDReasignacion_Tabla" class="form-control input-sm" value="0" type="text" />
    <input id="txtNroFirma"  value="0" type="text" />
    <input id="txtEsInternet"  value="N" type="text" />

    <input id="index_" class="form-control input-sm" disabled="disabled" />
    <input id="inicio" class="form-control input-sm" value="1" ="disabled" />

</div>

<div class="modal modal-header-success" role="dialog" tabindex="-1" id="modal_Rechazar">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white">Rechazar Solicitud de Reasignación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"  style="color: white;">×</span></button>
            </div>
            <div class="modal-body">

                <div class="row separacion-rows">
                   
                    <div class="col-md-2">
                        <label>Motivo Rechazo</label>
                    </div>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtMotivoRechazo" class="input-sm form-control"  Rows="4"  TextMode="MultiLine" MaxLength ="1000" runat="server"></asp:TextBox>
                        <%--<textarea id="txtMotivoRechazo" class="form-control input-sm"  type="text"  rows="4"  maxlength="1000"/>--%>
                    </div>
                </div>
                
                <div class="row separacion-rows">
                   
                    <div class="col-md-9"></div>
                    <div class="col-md-3">
                        
                      <%--  <asp:Button ID="pbRechazar"  class="btn btn-primary" runat="server" Text="Rechazar" />--%>
                        <button class="btn btn-primary form-control" onclick="f_GrabarRechazo();" type="button" id="pbRechazar">Rechazar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal modal-header-success" role="dialog" tabindex="-1" id="modal_Aprobar">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title  text-white">Aprobar con Cambio de Folio</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color: white;">×</span></button>
            </div>
            <div class="modal-body">

                <div class="row separacion-rows">
                   
                    <div class="col-md-3">
                        <label>Otro Folio</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtNroSeguroOtro" class="input-sm form-control"  MaxLength ="15" runat="server"></asp:TextBox>
                    </div>
                </div>
                
                <div class="row separacion-rows">
                   
                    <div class="col-md-9"></div>
                    <div class="col-md-3">
                        <button class="btn btn-primary form-control" onclick="f_ReasignacionAprobar();" type="button" id="pbAprobar">Aceptar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal modal-header-success" role="dialog" tabindex="-1" id="modal_Aprobar_V2">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white">Aprobar con Cambio de Folio</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color: white;">×</span></button>
                
            </div>
            <div class="modal-body">

                <div class="row separacion-rows">
                   
                    <div class="col-md-3">
                        <label>Mismo Folio</label>
                    </div>
                    <div class="col-md-6">
                        <input type="radio" name="rbMismoFolio"  value ="S" /> <!--onchange="f_MarcarMismoFolio();"-->
                    </div>
                </div>
                <div class="row separacion-rows">
                   
                    <div class="col-md-3">
                        <label>Diferente Folio</label>
                    </div>
                    <div class="col-md-6">
                        <input type="radio" name="rbMismoFolio"  value ="N" /> <!--onchange="f_MarcarMismoFolio();"-->
                    </div>
                </div>
                
                <div class="row separacion-rows">
                   
                    <div class="col-md-6"></div>
                    <div class="col-md-3">
                        <button class="btn btn-primary form-control" onclick="f_ReasignacionAprobar();" type="button" id="pb_FolioAprobar">Aceptar</button>
                    </div>
                    <div class="col-md-3">
                        <button class="btn btn-primary form-control" onclick="f_OcultarAprobar();" type="button" id="pb_FolioCancelar">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal modal-header-success" role="dialog" tabindex="-1" id="modal_Upload">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title bg-primary">Subir Respuesta Banco</h4></div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3">
                        <label>Archivo .CSV</label>
                    </div>
                    <div class="col-md-9">
                        <asp:FileUpload ID="fuSubirArchivo" runat="server" Width="90%" />
                    </div>
                </div>
                
                <div class="row">
                   
                    <div class="col-md-10"></div>
                    <div class="col-md-2">
                        <%--<button class="btn btn-primary" type="button">Subir</button>--%>
                        <asp:Button ID="pbUpdateFile" class="btn btn-primary form-control"  runat="server"  Text="Subir" />
                        <asp:Button ID="pbCancelar" class="btn btn-primary form-control"  runat="server"  Text="Cancelar" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-header-success" role="dialog" tabindex="-1" id="modal_Download">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title bg-primary">Descargar Archivo enviado al Banco</h4></div>
            <div class="modal-body">

                
                <div class="row">
                   
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <%--<button class="btn btn-primary" type="button">Subir</button>--%>
                        <%--<asp:Button ID="pbDescargar_" class="btn btn-primary"  runat="server"  Text="Subir" />--%>
                        <asp:Button ID="pbDescargar"  class="btn btn-primary" runat="server" Text="Descargar" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<%--<div class="modal modal-header-success" role="dialog" tabindex="-1" id="CamposOcualtos">




</div>--%>


<%--    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">        
    </asp:ToolkitScriptManager>--%>

<script>
    if ($('#<%=txtIDPerfil.ClientID%>').val() == 4) {
        //Activo
        document.getElementById('rptBotonera_primero_1').style.backgroundColor = "#128ff2";
        document.getElementById('rptBotonera_primero_1').style.borderColor = "#128ff2";
        //Normal

        document.getElementById('rptBotonera_primero_0').style.backgroundColor = "#00438c";
        document.getElementById('rptBotonera_primero_0').style.borderColor = "#356396";

        document.getElementById('rptBotonera_primero_2').style.backgroundColor = "#00438c";
        document.getElementById('rptBotonera_primero_2').style.borderColor = "#356396";


    } else {
        //Activo
        document.getElementById('rptBotonera_primero_2').style.backgroundColor = "#128ff2";
        document.getElementById('rptBotonera_primero_2').style.borderColor = "#128ff2";
        //Normal

        document.getElementById('rptBotonera_primero_0').style.backgroundColor = "#00438c";
        document.getElementById('rptBotonera_primero_0').style.borderColor = "#356396";

        document.getElementById('rptBotonera_primero_3').style.backgroundColor = "#00438c";
        document.getElementById('rptBotonera_primero_3').style.borderColor = "#356396";

        document.getElementById('rptBotonera_primero_4').style.backgroundColor = "#00438c";
        document.getElementById('rptBotonera_primero_4').style.borderColor = "#356396";
    }


    </script>
</asp:Content>