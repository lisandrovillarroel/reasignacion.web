﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="~/Pages/frmLogin.aspx.vb" Inherits="Reasignacion.Web.frmLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Reasignaciones</title>


    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../Content/bootstrap-table.min.css" rel="stylesheet" />
    <link href="../Content/Sitio.css" rel="stylesheet" />
    <link href="../Content/all.min.css" rel="stylesheet" />
    <link href="../Content/fontawesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans|Oswald|PT+Sans" rel="stylesheet" />

    <link rel="shortcut icon" href="../Img/BANCHILE.ico" />


    <%--JS--%>
    <script src="../Scripts/jquery-3.4.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.15.4/dist/bootstrap-table.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.15.4/dist/locale/bootstrap-table-es-CL.min.js"></script>
    <script src="https://unpkg.com/tableexport.jquery.plugin/tableExport.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.js"></script>
    <script src="../Scripts/funciones.js"></script>

    <%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.2/css/bootstrap.min.css"/>--%>
    <%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>--%>
    
   <%-- <link rel="stylesheet" href="assets/css/styles.min.css">--%>
    <%--<link rel="stylesheet" href="../Content/Sitio.css"/>--%>

    <script>

        
      $(document).ready(function () {

       <%--   $("#aForgot").click(function () {
              $("#mdlForgot").modal();
          });
        --%>
          $("#txtPassword").keypress(function (e) {
              if (e.which == 13) {
                  ValidarUsuario();
              }
          });


      });
   
        function ShowPopUps() {

            $('#showError').modal('show');
            $('#showInfo').modal('show');
            //showError("Valide los datos ingresados para Usuario y Clave");
        }

//             function showError(mensaje) {
//    $('#showErrorTitle').text('[Reasignación] -Operación con error');
//    $('#showErrorBody').empty();
//    $('#showErrorBody').html(mensaje);

//    $('#showError').modal('show');
//}

        function ValidarUsuario() {
            var Usuario = $("#txtUsuario").val();
            var Password = $("#txtPassword").val();

            if (Usuario == "") {
                showError('Debe ingresar Usuario');
                return false;
            }
            if (Password == "") {
                showError('Debe ingresar la Clave');
                return false;
            }
            f_ValidarUsuario();
        }

        function olvidoClave() {
            var Usuario = $("#txtUsuario").val();

            if (Usuario == "") {
                showError('Debe ingresar Usuario');
                return false;
            }

            f_enviaClave();
        }

        // ---------------------------------------------------------------------
        // Envia Correo recupera Clave
        // ---------------------------------------------------------------------
        function f_enviaClave() {
            var _Usuario = $("#txtUsuario").val();
        
            $.ajax({
                type: "POST",
                async: false,
                url: "frmLogin.aspx/f_enviaClave",
                data: '{_Usuario: "' + _Usuario + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccess_enviaClave,
                failure: function (response) {
                    alert(response.d);
                },error: function (xhr, status, error) {
                var exception = JSON.parse(xhr.responseText);
                ErrorMessageModel('Error', exception.Message);
            }
            });
        }

        function onSuccess_enviaClave(response) {
            datos = JSON.parse(response.d);
            $(datos).each(function () {
                //$('#txtRutCliente').val(this.RutTitular);
                if (this.Respuesta == 'ER') {
                    showError(this.Mensaje);
                }
                else {
                    showInfo(this.Mensaje);
                }
            });
        }

        
        // ---------------------------------------------------------------------
        // Validar Usuario
        // ---------------------------------------------------------------------
        function f_ValidarUsuario() {

            var _Usuario = $("#txtUsuario").val();
            var _Password = $("#txtPassword").val();
            $.ajax({
                type: "POST",
                async: false,
                url: "frmLogin.aspx/f_ValidaUsuario",
                data: '{_Usuario: "' + _Usuario + '" , _Password : "' + _Password + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onSuccess_ValidarUsuario,
                failure: function (response) {
                    alert(response.d);
                    //alert('Error');
                }
            });
        }

        function onSuccess_ValidarUsuario(response) {
            datos = JSON.parse(response.d);
            $(datos).each(function () {
                //$('#txtRutCliente').val(this.RutTitular);
                if (this.Respuesta == 'ER') {
                    showError(this.Mensaje);
                }
                else {


                    var loc = window.location;
                    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
                    var _URL_Sitio = loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
                    //OJO   var _Pagina = _URL_Sitio + 'frmIngresoSolicitud.aspx'; 
                    var _Pagina = _URL_Sitio + 'frmDefault.aspx'; 
                    

                    //alert(_Pagina);

<%--                     '<%Session["NombreEjecutivo"] = "' + this.NombreEjecutivo + '"; %>';
                    alert('<%=Session["NombreEjecutivo"] %>');--%>


                    //Session["NombreEjecutivo"] = this.NombreEjecutivo;
                    //Session["Usuario"] = this.Usuario;
                    //Session["Email"] = this.Email;
                    //Session["IDGrupo"] = this.IDGrupo;
                    //Session["IDUsuario"] = this.IDUsuario;
                    //alert('15');
                    //var url = "http://jquery4u.com";    
                    $(location).attr('href',_Pagina);

                    //window.location.href("~/Pages/frmIngresoSolicitud.aspx");
                    //alert('wwwwww');
                    //window.location='<   %= ResolveUrl("~/Account/Login.aspx") %>';
                }
            });
        }


    </script>

</head>
<body>
    <div class="login-dark">
        <form runat="server" method="post">
                <%--<div style="margin-left: 6%;margin-top:-60%;"> <img src="img/BanchileV1.png" class="img-responsive" width="200"/></div>--%>
               <div style="margin-left: 8%;"> <img src="../img/BanchileV1.png" class="img-responsive" width="200"/></div>
            <h2 class="sr-only">Solicitudes de Reasignaciones</h2>
            <%--<div class="illustration"><i class="icon ion-ios-locked-outline"></i></div>--%>
            <div class="illustration"></div>
            <div class="form-group">
                <%--<input class="form-control" name="txtUsuario" id="txtUsuario" placeholder="Usuario"/>--%>
                 <asp:TextBox ID="txtUsuario" runat="server" class="form-control" name="txtUsuario" placeholder="Usuario" MaxLength="20" ></asp:TextBox>
            </div>
            <div class="form-group">
                <%--<input class="form-control" type="password" name="txtPassword" id="txtPassword" placeholder="Contraseña"/>--%>
                <asp:TextBox ID="txtPassword" class="form-control" runat="server" TextMode="Password" MaxLength="50" placeholder="Contraseña"></asp:TextBox>
            </div>
            <div class="form-group">
                <input type="button" id="pbGrabar" class="btn btn-primary form-control" value="Ingresar" onclick="ValidarUsuario();" />
                <%--<asp:Button class="btn btn-primary btn-block" ID="btnIngresar" runat="server" Text="Ingresar" OnClick="btnIngresar_Click" OnClientClick="return ValidarUsuario();" />--%>
               <%-- <asp:Button runat="server" class="btn btn-primary btn-block"  id="btnIngresar" Text="Ingresar" OnClientClick="return ValidarUsuario();" OnClick="btnIngresar_Click"/>--%>
            </div>
            <%--<button class="forgot" id="btnForgot">Olvido su Contraseña?</button>--%>
            <a class="forgot"  onclick="olvidoClave()">Olvido su Contraseña?</a>
        </form>
    </div>
    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.2/js/bootstrap.bundle.min.js"></script>
    --%><%--<script src="assets/js/script.min.js"></script>--%>

    
        <!-- showError -->
        <div id="showError" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-danger">

                        <h6 id="showErrorTitle" class="modal-title text-white">Operación con error</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: white;">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p id="showErrorBody"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                    </div>
                </div>
            </div>
        </div>

            <!-- showInfo -->
        <div id="showInfo" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-info">

                        <h6 id="showInfoTitle" class="modal-title text-white">Operación Informativa</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: white;">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p id="showInfoBody"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                    </div>
                </div>
            </div>
        </div>

</body>
</html>

<!--
<div class="modal modal-header-success" role="dialog" tabindex="-1" id="mdlForgot">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title bg-primary">Solicitud de cambio de contraseña</h4></div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3">
                        <label>Ingrese Correo</label>
                    </div>
                    <div class="col-md-9">
                       <input type="text" id="txtMail"/>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
-->
