﻿Imports Newtonsoft.Json

Public Class frmAprobacionSolicitud
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtIDUsuario.Text = Session("IDUsuario")
            txtIDPerfil.Text = Session("IDGrupo")
            If txtIDUsuario.Text = "" Then
                Response.Redirect("frmLogin.aspx")
            End If
        End If
    End Sub

    Public Shared Function ToJSON(pTable As DataTable) As String
        Dim vCadenaJSON As String = ""
        vCadenaJSON = JsonConvert.SerializeObject(pTable)
        If vCadenaJSON.Length <= 5 Then
            vCadenaJSON = "[{""NRO_SEGURO"":""-""}]"
        End If
        Return vCadenaJSON
    End Function


    <System.Web.Services.WebMethod()>
    Public Shared Function Get_TipoSolicitud() As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec.Get_TipoSolicitudTodas(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    <System.Web.Services.WebMethod()>
    Public Shared Function Get_Coordinadores() As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec.Get_CoordinadoresTodos(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function Put_grabaEnProceso(_IDReasignacion As String, _IDUsuario As String, _enProceso As String) As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._IDReasignacion = _IDReasignacion
            Exec._IDUsuario = _IDUsuario
            Exec._enProceso = _enProceso

            Exec.Put_grabaEnProceso(rp)

            dtJSON = Nothing
            If rp.TieneDatos Then
                'MsgBox(rp.Ds.Tables(0).Rows(0).Item(0))
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function Get_Reasignaciones(_IDUsuario As String, _IDReasignacion As String, _IDCoordinador As String, _FechaDesde As String, _FechaHasta As String, _IDTipoSolicitud As String, _RutEjecutivoReasignacion As String, _NroSeguro As String, _enProceso As String) As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec.IDUsuario = _IDUsuario
            Exec._IDReasignacion = _IDReasignacion
            Exec._IDCoordinador = IIf(_IDCoordinador = "null", 0, _IDCoordinador)
            Exec._FechaDesde = _FechaDesde
            Exec._FechaHasta = _FechaHasta
            Exec._IDTipoSolicitud = IIf(_IDTipoSolicitud = "null", 0, _IDTipoSolicitud)
            Exec._RutEjecutivoReasignacion = _RutEjecutivoReasignacion
            Exec._NroSeguro = _NroSeguro
            Exec._enProceso = IIf(_enProceso = "T", "", _enProceso)

            Exec.Get_ReasignacionesGestionar(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function



    <System.Web.Services.WebMethod()>
    Public Shared Function Get_ReasignacionesActualizar(_IDUsuario As String, _IDReasignacion As String, _IDCoordinador As String, _FechaDesde As String, _FechaHasta As String, _IDTipoSolicitud As String, _RutEjecutivoReasignacion As String, _NroSeguro As String, _largoArreglo As String, _arregloActual As String) As String

        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec.IDUsuario = _IDUsuario
            Exec._IDReasignacion = _IDReasignacion
            Exec._IDCoordinador = _IDCoordinador
            Exec._FechaDesde = _FechaDesde
            Exec._FechaHasta = _FechaHasta
            Exec._IDTipoSolicitud = _IDTipoSolicitud
            Exec._RutEjecutivoReasignacion = _RutEjecutivoReasignacion
            Exec._NroSeguro = _NroSeguro

            Exec.Get_ReasignacionesGestionarActualizar(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            ' Return ToJSON(table)
            ' Else
            Return ToJSON(dtJSON)
            ' End If

        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    <System.Web.Services.WebMethod()>
    Public Shared Function Upd_ReasignacionRechazo(_IDReasignacion As String, _NroFirma As String, _Observacion As String, _IDUsuario As String) As String
        Try


            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            '        Dim table As New DataTable("variable")
            '        table.Columns.Add("Respuesta", GetType(String))
            '       table.Columns.Add("IDReasignacion", GetType(String))
            '      table.Columns.Add("NroFirma", GetType(String))
            '     table.Columns.Add("IDUsuario", GetType(String))
            '    table.Columns.Add("IDCoordinador", GetType(String))
            '   table.Columns.Add("FechaDesde", GetType(String))
            '  table.Columns.Add("FechaHasta", GetType(String))
            ' table.Columns.Add("IDTipoSolicitud", GetType(String))
            '           table.Columns.Add("RutEjecutivoReasignacion", GetType(String))
            '          table.Columns.Add("_NroSeguro", GetType(String))
            '         table.Columns.Add("_largoArreglo", GetType(String))
            '        table.Columns.Add("_arregloActual", GetType(String))

            '       Dim newRow As DataRow = table.NewRow()
            '      newRow("Respuesta") = "Ok1"
            '     newRow("IDReasignacion") = _IDReasignacion
            '    newRow("NroFirma") = _NroFirma
            '   newRow("IDUsuario") = _IDUsuario
            '  newRow("IDCoordinador") = _IDCoordinador
            '     newRow("FechaDesde") = _FechaDesde
            '    newRow("FechaHasta") = _FechaHasta
            '   newRow("IDTipoSolicitud") = _IDTipoSolicitud
            '  newRow("RutEjecutivoReasignacion") = _RutEjecutivoReasignacion
            '     newRow("NroSeguro") = _NroSeguro
            '     newRow("largoArreglo") = _largoArreglo
            '     newRow("arregloActual") = _arregloActual

            'table.Rows.Add(newRow)

            Exec._IDReasignacion = _IDReasignacion
            Exec._NroFirma = _NroFirma
            Exec._Observacion = _Observacion
            Exec._IDUsuario = _IDUsuario

            Exec.Upd_AutorizacionRechazo(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    <System.Web.Services.WebMethod()>
    Public Shared Function Upd_ReasignacionAprobar(_IDReasignacion As String, _NroFirma As String, _NroSeguroOtro As String, _IDUsuario As String) As String

        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._IDReasignacion = _IDReasignacion
            Exec._NroFirma = _NroFirma
            Exec._NroSeguroOtro = _NroSeguroOtro
            Exec._IDUsuario = _IDUsuario

            Exec.Upd_AutorizacionAprobar(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function paso() As String

        Try
            Return ""
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    <System.Web.Services.WebMethod()>
    Public Shared Function Get_ReasignacionResumen(_IDReasignacion As String, _IDUsuario As String) As String
        Try

            Dim Exec As New Business.BANCHDB13
            Dim rp As New DotNetResponse.SQLPersistence

            Dim dtJSON As DataTable

            Exec._IDReasignacion = _IDReasignacion
            Exec._IDUsuario = _IDUsuario

            Exec.Get_ReasignacionResumen(rp)
            dtJSON = Nothing
            If rp.TieneDatos Then
                dtJSON = rp.Ds.Tables(0)
            End If
            If rp.Errores Then
            End If
            rp.Dispose()
            Exec.Dispose()

            Return ToJSON(dtJSON)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


End Class